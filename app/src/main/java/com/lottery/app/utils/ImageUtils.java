package com.lottery.app.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.lottery.app.api.Api;
import com.luck.picture.lib.basic.PictureSelector;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.SelectMimeType;

/**
 * 图片加载工具类
 */
public class ImageUtils {

    static public void load(Context context, String imageUrl, ImageView launcherImageView) {

        if (imageUrl == null || imageUrl.length() == 0) {
            return;
        }
        if (!imageUrl.contains("http://") && !imageUrl.contains("https://")) {
            imageUrl = Api.IMAGE_URL + imageUrl;
        }
        Glide.with(context).load(imageUrl).into(launcherImageView);
    }

    /**
     * Base64转Bitmap
     *
     * @param base64String base64数据流
     * @return Bitmap 图片
     */
    public static Bitmap base642Bitmap(String base64String) {
        if (null == base64String) throw new NullPointerException();
        byte[] decode = Base64.decode(base64String.split(",")[1], Base64.DEFAULT);
        Bitmap mBitmap = BitmapFactory.decodeByteArray(decode, 0, decode.length);
        return mBitmap;
    }

    public static void openAlbum(Activity activity){
        PictureSelector.create(activity)
                .openGallery(SelectMimeType.ofImage())
                .setImageEngine(GlideEngine.createGlideEngine())
                .setMaxSelectNum(1)// 最大图片选择数量
                .setMinSelectNum(1)// 最小选择数量
                .isPreviewZoomEffect(false)//预览图片时 是否增强左右滑动图片体验(图片滑动一半即可看到上一张是否选中)
                .forResult(PictureConfig.CHOOSE_REQUEST);//结果回调onActivityResult code
    }

}
