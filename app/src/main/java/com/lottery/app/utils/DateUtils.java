package com.lottery.app.utils;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 时间工具类
 */
public class DateUtils {

    private final static long minute = 60 * 1000;// 1分钟
    private final static long hour = 60 * minute;// 1小时
    private final static long day = 24 * hour;// 1天
    private final static long month = 31 * day;// 月
    private final static long year = 12 * month;// 年

    /** 年-月-日 时:分:秒 显示格式 */
    // 备注:如果使用大写HH标识使用24小时显示格式,如果使用小写hh就表示使用12小时制格式。
    public static String DATE_TO_STRING_DETAIAL_PATTERN = "yyyy-MM-dd HH:mm:ss";

    /** 年-月-日 显示格式 */
    public static String DATE_TO_STRING_SHORT_PATTERN = "yyyy-MM-dd";


    private static SimpleDateFormat simpleDateFormat;


    public static String dateToString(Date source, String pattern) {
        simpleDateFormat = new SimpleDateFormat(pattern);
        return simpleDateFormat.format(source);
    }
    public static String dateToString(Date source){
        return dateToString(source,DATE_TO_STRING_DETAIAL_PATTERN);
    }

    public static Date stringToDate(String strDate){
        if (strDate==null){
            return null;
        }
        SimpleDateFormat formatter = new SimpleDateFormat(DATE_TO_STRING_DETAIAL_PATTERN);
        ParsePosition pos = new ParsePosition(0);
        Date date = formatter.parse(strDate, pos);
        return date;
    }

    public static Date stringToDate(String strDate,String pattern){
        if (strDate==null){
            return null;
        }
        SimpleDateFormat formatter = new SimpleDateFormat(pattern);
        ParsePosition pos = new ParsePosition(0);
        Date date = formatter.parse(strDate, pos);
        return date;
    }

    public static Date stringToDate2(String strDate){
        if (strDate==null){
            return null;
        }
        SimpleDateFormat formatter = new SimpleDateFormat(DATE_TO_STRING_SHORT_PATTERN);
        ParsePosition pos = new ParsePosition(0);
        Date date = formatter.parse(strDate, pos);
        return date;
    }

    static public String formattedDateDescription(String dateString) {
        Date date = stringToDate(dateString);
        return formattedDateDescription(date);
    }

    static public String formattedDateDescription(Date date){
        if (date == null) {
            return null;
        }

        long diff = new Date().getTime() - date.getTime();
        long r = 0;
        if (diff > year) {
//            r = (diff / year);
//            return r + "个年前";
            return dateToString(date);
        }
        if (diff > month) {
//            r = (diff / month);
//            return r + "个月前";
            return dateToString(date);
        }
        Date currDate2 = DateUtils.stringToDate2(DateUtils.dateToString(new Date(),DATE_TO_STRING_SHORT_PATTERN));
        long diff2= currDate2.getTime() - date.getTime();
        if (diff2 > day) {
            r = (diff2 / day);
            if (r >= 1 && r <= 6){
                int r2 = (int)Math.floor(r);
                return r2+"天前 "+ DateUtils.dateToString(date,"HH:mm");
            }
            return dateToString(date);
        }
        if (diff >= hour) {
            r = (diff / hour);
            return r + "小时前";
        }
        if (diff >= minute) {
            r = (diff / minute);
            return r + "分钟前";
        }
        return "刚刚";
    }
    public static int getYear(String date){
        return getYear(stringToDate(date,DATE_TO_STRING_SHORT_PATTERN));
    }
    public static int getYear(Date date){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
        String year = sdf.format(date);
        return Integer.parseInt(year);
    }

    public static int getMonth(String date){
        return getMonth(stringToDate(date,DATE_TO_STRING_SHORT_PATTERN));
    }

    public static int getMonth(Date date){
        SimpleDateFormat sdf = new SimpleDateFormat("MM");
        String month = sdf.format(date);
        return Integer.parseInt(month);
    }

    public static int getDay(String date){
        return getDay(stringToDate(date,DATE_TO_STRING_SHORT_PATTERN));
    }

    public static int getDay(Date date){
        SimpleDateFormat sdf = new SimpleDateFormat("dd");
        String day = sdf.format(date);
        return Integer.parseInt(day);
    }

}
