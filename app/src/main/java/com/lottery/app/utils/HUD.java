package com.lottery.app.utils;

import android.content.Context;
import android.widget.Toast;

import com.lottery.app.core.App;
import com.maning.mndialoglibrary.MProgressDialog;


/**
 * HUD
 */
public class HUD {

    public static void show(Context context) {
        show(context,null);
    }
    public static void show(Context context,String title) {
        if (title==null){
            title = "加载中...";
        }
        MProgressDialog.showProgress(context,title);
    }

    public static void dismiss() {
        MProgressDialog.dismissProgress();
    }


    /**
     * Toast 提示 短时间
     * @param title
     */
    public static void Toast(String title) {
        if (!AppUtils.isAppInForeground(App.getContext())){
            return;
        }
        Toast.makeText(App.getContext(), title, Toast
                .LENGTH_SHORT).show();
    }

}
