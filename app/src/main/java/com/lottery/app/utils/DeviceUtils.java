package com.lottery.app.utils;

import android.graphics.drawable.Drawable;

import com.lottery.app.R;
import com.lottery.app.core.App;

public class DeviceUtils {


    /**
     * 根据设备状态返回背景
     * @param status 设备状态
     */
    public static Drawable getDeviceStatusDrawable(String status) {
        if (status.equals("在线")||status.equals("1")) {
            return App.getContext().getDrawable(R.drawable.device_status_online);
        }
        return App.getContext().getDrawable(R.drawable.device_status_offline);
    }


}
