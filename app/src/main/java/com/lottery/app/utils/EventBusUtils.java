package com.lottery.app.utils;

import com.lottery.app.entity.EventMessage;
import com.lottery.app.entity.TbWxUser;
import com.lottery.app.entity.TbWxUserRedeem;
import com.lottery.app.mqtt.PaySuccessVo;

import org.greenrobot.eventbus.EventBus;

import java.util.List;


public class EventBusUtils {

    public static void post(EventMessage message) {
        EventBus.getDefault().post(message);
    }

    /**
     * 退出登录
     */
    public static void postLogout() {
        EventMessage message = new EventMessage();
        message.setEvent(EventMessage.EVENT_LOGOUT);
        post(message);
    }

    public static void postMQTTStatus(int status){
        EventMessage message = new EventMessage();
        message.setEvent(EventMessage.EVENT_MQTT_STATUS);
        message.setData(Integer.valueOf(status));
        post(message);
    }

    public static void postChooseDevice(){
        EventMessage message = new EventMessage();
        message.setEvent(EventMessage.EVENT_CHOOSE_DEVICE);
        post(message);
    }

    /**
     * 支付成功
     * @param paySuccessVo 支付成功信息
     */
    public static void postPaySuccess(PaySuccessVo paySuccessVo){
        EventMessage message = new EventMessage();
        message.setData(paySuccessVo);
        message.setEvent(EventMessage.EVENT_PAY_SUCCESS);
        post(message);
    }

    /**
     * 添加设备兑奖记录
     */
    public static void postAddDeviceWxUserRedeems(Object data){
        EventMessage message = new EventMessage();
        message.setData(data);
        message.setEvent(EventMessage.EVENT_ADD_DEVICE_WX_USER_REDEEMS);
        post(message);
    }


    /**
     * 获取微信用户
     * @param tbWxUser 微信用户
     */
    public static void postGetWxUser(TbWxUser tbWxUser){
        EventMessage message = new EventMessage();
        message.setData(tbWxUser);
        message.setEvent(EventMessage.EVENT_GET_WX_USER);
        post(message);
    }

    public static void postReloadRedeems(List<TbWxUserRedeem> redeems){
        EventMessage message = new EventMessage();
        message.setData(redeems);
        message.setEvent(EventMessage.EVENT_RELOAD_REDEEMS);
        post(message);
    }
}
