package com.lottery.app.utils;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;

import com.lottery.app.R;
import com.lottery.app.core.App;
import com.lottery.app.ui.activity.CustomizeScanActivity;
import com.lottery.app.ui.activity.CustomizeScanPadActivity;
import com.yxing.ScanCodeConfig;
import com.yxing.bean.ScanRect;
import com.yxing.def.ScanStyle;

public class ScanUtils {

    public static void pushScan(Activity activity){
        if (AppUtils.isPad()){
            ScanCodeConfig.create(activity)
                    //设置扫码页样式 ScanStyle.NONE：无  ScanStyle.QQ ：仿QQ样式   ScanStyle.WECHAT ：仿微信样式  ScanStyle.CUSTOMIZE ： 自定义样式
                    .setStyle(ScanStyle.NONE)
                    //扫码成功是否播放音效  true ： 播放   false ： 不播放
                    .setPlayAudio(true)
                    //////////////////////////////////////////////
                    .buidler()
                    //跳转扫码页   扫码页可自定义样式
                    .start(CustomizeScanPadActivity.class);
            return;
        }
//        int W = App.screenHeight;
//        int H = App.screenWidth;
//
//        if (W > H){
//            H = App.screenWidth;
//            W = App.screenHeight;
//        }
//        int left = (W - 360)/2;
//        int top = (H - 160)/2;
//
//        left = ViewUtils.dp2px(activity,left);
//        top = ViewUtils.dp2px(activity,top);

        ScanCodeConfig.create(activity)
                //设置扫码页样式 ScanStyle.NONE：无  ScanStyle.QQ ：仿QQ样式   ScanStyle.WECHAT ：仿微信样式  ScanStyle.CUSTOMIZE ： 自定义样式
                .setStyle(ScanStyle.NONE)
                //扫码成功是否播放音效  true ： 播放   false ： 不播放
                .setPlayAudio(true)
                //////////////////////////////////////////////
                .buidler()
                //跳转扫码页   扫码页可自定义样式
                .start(CustomizeScanActivity.class);
    }

    public static Bitmap createQR(String text){
        int size = 500;
        Context context = App.getContext();
        Resources res = context.getResources();
        Bitmap    bmp = BitmapFactory.decodeResource(res, R.mipmap.logo);
        Bitmap qrCode = ScanCodeConfig.createQRCodeWithStrokeLogo(text,size,context.getResources().getColor(R.color.main_color),context.getResources().getColor(R.color.clear_color),bmp,50,50,8,8,4,context.getResources().getColor(R.color.white));
        return qrCode;
    }

    public static String scanQRImage(Activity activity, Uri uri){
        String code = ScanCodeConfig.scanningImage(activity,uri);
        return code;
    }

    /**
     * J084023173009286710952584129276080308362
     * 验证二维码是否正确
     * 位数在40或者41位 并是G或J开头的二维码
     */
    public static boolean checkQRCode(String code) {
        if (code.length() == 40 || code.length() == 41) {
            if (code.startsWith("G") || code.startsWith("J")) {
                return true;
            }
        }
        return false;
    }

}
