package com.lottery.app.utils;

import com.alibaba.fastjson.JSON;
import com.lottery.app.api.HttpAction;
import com.lottery.app.core.UserCore;
import com.lottery.app.entity.ResponseBody;
import com.lottery.app.entity.TbEventLog;

import java.util.Map;

import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;

/**
 * EventLogUtils
 */
public class EventLogUtils {

    public static void addEventLog(String event, Map map) {
        String ct = JSON.toJSONString(map);
        addEventLog(event,ct);
    }

    public static void addEventLog(String event, String content) {
        TbEventLog eventLog = new TbEventLog();
        if (UserCore.isLogin() && UserCore.sharedCore().getUserInfo()!=null){
            eventLog.setUserId(UserCore.sharedCore().getUserInfo().getUser().getUserId().toString());
        }
        eventLog.setDeviceSn(UserCore.sharedCore().getDeviceSn());
        eventLog.setContent(content);
        eventLog.setEvent(event);
        eventLog.setType(0l);
        eventLog.setSystemVersion(AppUtils.getSystemVersion());
        eventLog.setDeviceBrand(AppUtils.getDeviceBrand());
        eventLog.setDeviceModel(AppUtils.getDeviceModel());
        eventLog.setVersionCode(AppUtils.getAppVersionCode()+"");
        eventLog.setVersionName(AppUtils.getAppVersionName());
        HttpAction.addEventLog(eventLog, new Observer<ResponseBody>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull ResponseBody responseBody) {

            }

            @Override
            public void onError(@NonNull Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });

    }

}
