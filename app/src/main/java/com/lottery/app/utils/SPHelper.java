package com.lottery.app.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

import com.lottery.app.core.App;


/**
 * Sp工具  缓存一些基本的东西
 * @author xllyll
 */
public class SPHelper {

    private static volatile SPHelper instance;

    /**
     * 文件名字
     */
    public static final String FILE_NAME = "lottery_data.xml";

    private final SharedPreferences sharedPreferences;

    public static SPHelper getInstance() {
        if (instance == null) {
            synchronized (SPHelper.class) {
                if (instance == null) {
                    instance = new SPHelper();
                }
            }
        }
        return instance;
    }


    /*
     * 保存手机里面的名字
     */
    private final SharedPreferences.Editor editor;

    @SuppressLint("CommitPrefEdits")
    public SPHelper() {

        sharedPreferences = App.sharedApplication().getSharedPreferences(FILE_NAME,
                Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();

    }

    /**
     * 存储
     */
    public void put(String key, Object object) {
        if (object == null)
            return;
        if (object instanceof String) {
            editor.putString(key, (String) object);
        } else if (object instanceof Integer) {
            editor.putInt(key, (Integer) object);
        } else if (object instanceof Boolean) {
            editor.putBoolean(key, (Boolean) object);
        } else if (object instanceof Float) {
            editor.putFloat(key, (Float) object);
        } else if (object instanceof Long) {
            editor.putLong(key, (Long) object);
        } else {
            editor.putString(key, object.toString());
        }
        editor.apply();
    }

    public void putString(String key, String value) {
        put(key,value);
    }

    public String getString(String key) {
        return sharedPreferences.getString(key, "");
    }


    public boolean getBoolean(String key,boolean defaultValue) {
        return sharedPreferences.getBoolean(key, defaultValue);
    }

    public long getLong(String key) {
        return sharedPreferences.getLong(key, 0);
    }

    public int getInt(String key) {
        return sharedPreferences.getInt(key, 0);
    }


    public boolean getTrueBoolean(String key) {
        return sharedPreferences.getBoolean(key, true);
    }

    public boolean getFalseBoolean(String key) {
        return sharedPreferences.getBoolean(key, false);
    }


    /**
     * 移除某个key值已经对应的值
     */
    public void remove(String key) {
        editor.remove(key);
        editor.apply();
    }

    /**
     * 清除所有数据
     */
    public void clear() {
        editor.clear();
        editor.apply();
    }

    /**
     * 查询某个key是否存在
     */
    public Boolean contain(String key) {
        return sharedPreferences.contains(key);
    }

}
