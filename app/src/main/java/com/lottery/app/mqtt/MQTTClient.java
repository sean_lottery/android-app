package com.lottery.app.mqtt;

import android.app.Activity;
import android.util.Log;

import com.lottery.app.api.Api;
import com.lottery.app.core.App;
import com.lottery.app.core.UserCore;
import com.lottery.app.ui.activity.BaseActivity;
import com.lottery.app.utils.EventBusUtils;
import com.lottery.app.utils.EventLogUtils;
import com.umeng.analytics.MobclickAgent;

import net.dreamlu.iot.mqtt.codec.MqttProperties;
import net.dreamlu.iot.mqtt.codec.MqttPublishMessage;
import net.dreamlu.iot.mqtt.codec.MqttVersion;
import net.dreamlu.iot.mqtt.core.client.IMqttClientConnectListener;
import net.dreamlu.iot.mqtt.core.client.IMqttClientMessageListener;
import net.dreamlu.iot.mqtt.core.client.MqttClient;

import org.tio.core.ChannelContext;
import org.tio.utils.buffer.ByteBufferAllocator;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;


/**
 * MQTT 客户端
 */
public class MQTTClient implements IMqttClientConnectListener{

    private static final String TAG = "MQTTClient";

    /**
     * mqtt 客户端
     */
    private MqttClient client;

    private boolean isBuild = false;

    private boolean isOnline = false;

    /**
     * 单列
     */
    private static MQTTClient mInstance;

    /*
     * 获取RetrofitServiceManager
     **/
    public static MQTTClient getInstance() {
        if (mInstance == null) {
            synchronized (MQTTClient.class) {
                if (mInstance == null) {
                    mInstance = new MQTTClient();
                }
            }
        }
        return mInstance;
    }

    /**
     * 构造函数
     */
    private MQTTClient(){

    }
    public void buildMqtt(){
        isBuild = true;
        buildMqtt(false);
    }

    public boolean isBuild() {
        return isBuild;
    }

    /**
     * 构建 mqtt 客户端
     */
    public void buildMqtt(boolean isReBuild){

        Log.i(TAG, "buildMqtt: 开始构建 mqtt 客户端...");
        String deviceId = UserCore.sharedCore().getDeviceSn();
        Log.i(TAG, "buildMqtt: host:"+Api.MQTT_HOST+" port:"+Api.MQTT_PORT + " deviceId:"+deviceId);

        client = MqttClient.create()
                .name("Mica-Mqtt-Client")       // 客户端名称，默认：Mica-Mqtt-Client
                .ip(Api.MQTT_HOST)              // mqtt 服务端 ip 地址
                .port(Api.MQTT_PORT)            // 默认：1883
                .version(MqttVersion.MQTT_5)    // 默认：3_1_1
                .clientId(deviceId)             // 非常重要务必手动设置，一般设备 sn 号，默认：MICA-MQTT- 前缀和 36进制的纳秒数
                .bufferAllocator(ByteBufferAllocator.HEAP)       // 堆内存和堆外内存，默认：堆内存
                .readBufferSize(800 * 1024)            // 消息一起解析的长度，默认：为 8 * 1024 （mqtt 消息最大长度）
                .maxBytesInMessage(10 * 1024  *1024)   // 最大包体长度,如果包体过大需要设置此参数，默认为： 10M (10*1024*1024)
                .keepAliveSecs(60)              // 心跳时间，默认：60s
                .timeout(5)                     // 超时时间，t-io 配置，可为 null，为 null 时，t-io 默认为 5
                .reconnect(true)                // 是否重连，默认：true
                .reInterval(2000)               // 重连重试时间，reconnect 为 true 时有效，t-io 默认为：5000
                .connectListener(this)          // 连接监听
                .properties(new MqttProperties()) // mqtt5 properties
                .connect();                   // 同步连接，也可以使用 connect()，可以避免 broker 没启动照成启动卡住。

        Log.i(TAG, "buildMqtt: 构建 mqtt 客户端完成");
        Map<String,Object> map = new HashMap<>();
        map.put("host",Api.MQTT_HOST);
        map.put("port",Api.MQTT_PORT);
        map.put("isReBuild",isReBuild);
        EventLogUtils.addEventLog("mqtt_build",map);
        // 消息订阅
        subscribe();
    }

    public void connect(){
        if (client == null){
            Log.e(TAG, "connect: client is null");
            buildMqtt(true);
            return;
        }
        if (client.isDisconnected()) {
            client.reconnect();
            subscribe();
        }
    }

    public void subscribe(){
        client.subQos0("device/get", new IMqttClientMessageListener() {
            @Override
            public void onMessage(ChannelContext context, String topic, MqttPublishMessage message, byte[] payload) {
                String msg = new String(payload);
                Log.i(TAG, "onMessage: 收到消息 topic:"+topic+" message:"+message+" payload:"+msg);
                MQTTClientHandler.handlerMessage(msg);
            }
        });
    }

    public boolean isOnline() {
        return isOnline;
    }

    /**
     * 发布消息
     * @param topic
     * @param message
     */
    public void publish(String topic, String message){
        client.publish(topic, message.getBytes(StandardCharsets.UTF_8));
    }

    @Override
    public void onConnected(ChannelContext context, boolean isReconnect) {
        Log.i(TAG, "buildMqtt onConnected: 链接服务器成功 isReconnect:"+isReconnect);
        try {
            isOnline = true;
            EventBusUtils.postMQTTStatus(1);
            Map<String,Object> objc = new HashMap<>();
            objc.put("status","1");
            objc.put("isReconnect",isReconnect);
            MobclickAgent.onEventObject(App.getContext(),"mqtt_status",objc);
            EventLogUtils.addEventLog("mqtt_status",objc);
        }catch (Exception e){
            EventLogUtils.addEventLog("mqtt_status_error",e.getMessage());
        }

        Activity activity = App.getCurrentActivity();
        if (activity!=null&&(activity instanceof BaseActivity)){
            BaseActivity baseActivity = (BaseActivity) activity;
            baseActivity.updateMQTTUI(1);
        }
    }

    @Override
    public void onDisconnect(ChannelContext context, Throwable throwable, String remark, boolean isRemove) {
        String error = throwable==null?"NULL":throwable.getMessage();
        if (remark == null){
            remark = "NULL";
        }
        Log.i(TAG, "buildMqtt onDisconnect: 与链接服务器断开连接 throwable:"+error+" remark:"+remark+" isRemove:"+isRemove);
        try {
            isOnline = false;
            EventBusUtils.postMQTTStatus(0);
            Map<String,Object> objc = new HashMap<>();
            objc.put("status","0");
            objc.put("error",error);
            objc.put("remark",remark);
            objc.put("isRemove",isRemove);
            MobclickAgent.onEventObject(App.getContext(),"mqtt_status",objc);
            EventLogUtils.addEventLog("mqtt_status",objc);
        }catch (Exception e){
            EventLogUtils.addEventLog("mqtt_status_error",e.getMessage());
        }
        Activity activity = App.getCurrentActivity();
        if (activity!=null&&(activity instanceof BaseActivity)){
            BaseActivity baseActivity = (BaseActivity) activity;
            baseActivity.updateMQTTUI(0);
        }
    }

}
