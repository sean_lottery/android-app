package com.lottery.app.mqtt;

/**
 * MQTT消息体
 */
public class MqttVo {
    /**
     * 事件
     */
    private String event;
    /**
     * 数据
     */
    private Object data;
    /**
     * 客户端ID
     */
    private String clientId;

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }
}
