package com.lottery.app.mqtt;


/**
 * EventBus 事件
 */
public enum MqttEvent {
    syncGood,//同步商品
    syncGoodOne,//同步单个商品
    deviceError,//排错指令，单个货到
    reset,//重置设备
    reboot,//重启设备
    update,//通知升级
    unlock,//设备开锁
    settingCarousel,//设置轮播图
    testDevice,//设备测试
    testDeviceGood,//电机测试
    voiceTip,//语音提示
    paySuccess,//订单支付成功
    paySuccess_REPLY,//订单支付成功,机器的回复
    ticketingEnd,//上行，出票完成
    ticketingEnd_REPLY,// 出票完成之后，回执给客户端
    wxUserInfo,// 获取微信用户信息
    redeem,//兑奖
    redeemEndWinning,//兑奖结束[中奖]
    redeemEndNOWinning,//兑奖结束[未中奖]
    redeemEndError,//兑奖结束【异常】
    upChannelsStatus,//定时上报机头状态
    onLine,//定时检查机器是否在线
    orderTicking,//往服务器同步出票指令
}
