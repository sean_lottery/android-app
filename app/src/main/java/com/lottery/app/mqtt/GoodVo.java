package com.lottery.app.mqtt;

import java.math.BigDecimal;


/**
 * 商品
 */
public class GoodVo {

    private Long id;
    private String goodName;
    /** 商品ID */
    private Long goodId;
    /** 货道号 */
    private String cargoNo;
    private Long cargoId;
    /** 商品数量 */
    private Long goodNum;
    private BigDecimal goodPrice;



    private String channelState;//": "NORMAL", //出货后通道状态 参见 上面 channelState 取值
    private String channelStateMsg;//": "",//状态说明
    private Long deliveryState;//": 2, // 出货状态 出货状态 0 初始状态 1.出货中 2.全部成功, 3.部分成功, 4.全部失败 5.未知失败
    private Long successNum;//": 1,// 出货成功数量
    private String failReason;//":""//失败原因备注.





    public void setCargoNo(String cargoNo) {
        this.cargoNo = cargoNo;
    }
    public Long getCargoId() {
        return cargoId;
    }
    public void setCargoId(Long cargoId) {
        this.cargoId = cargoId;
    }
    public String getChannelState() {
        return channelState;
    }
    public void setChannelState(String channelState) {
        this.channelState = channelState;
    }
    public String getChannelStateMsg() {
        return channelStateMsg;
    }
    public void setChannelStateMsg(String channelStateMsg) {
        this.channelStateMsg = channelStateMsg;
    }

    public String getCargoNo() {
        return cargoNo;
    }
    public Long getDeliveryState() {
        return deliveryState;
    }
    public void setDeliveryState(Long deliveryState) {
        this.deliveryState = deliveryState;
    }
    public Long getSuccessNum() {
        return successNum;
    }
    public void setSuccessNum(Long successNum) {
        this.successNum = successNum;
    }
    public String getFailReason() {
        return failReason;
    }
    public void setFailReason(String failReason) {
        this.failReason = failReason;
    }
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getGoodName() {
        return goodName;
    }
    public void setGoodName(String goodName) {
        this.goodName = goodName;
    }
    public Long getGoodId() {
        return goodId;
    }
    public void setGoodId(Long goodId) {
        this.goodId = goodId;
    }

    public Long getGoodNum() {
        return goodNum;
    }
    public void setGoodNum(Long goodNum) {
        this.goodNum = goodNum;
    }
    public BigDecimal getGoodPrice() {
        return goodPrice;
    }
    public void setGoodPrice(BigDecimal goodPrice) {
        this.goodPrice = goodPrice;
    }


}
