//package com.lottery.app.mqtt;
//
//import android.util.Log;
//
//import com.lottery.app.api.Api;
//import com.lottery.app.core.App;
//import com.lottery.app.core.UserCore;
//import com.lottery.app.utils.EventBusUtils;
//import com.lottery.app.utils.EventLogUtils;
//import com.umeng.analytics.MobclickAgent;
//
//import org.eclipse.paho.client.mqttv3.IMqttActionListener;
//import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
//import org.eclipse.paho.client.mqttv3.IMqttToken;
//import org.eclipse.paho.client.mqttv3.MqttCallback;
//import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
//import org.eclipse.paho.client.mqttv3.MqttException;
//import org.eclipse.paho.client.mqttv3.MqttMessage;
//
//import java.util.HashMap;
//import java.util.Map;
//
//import info.mqtt.android.service.Ack;
//import info.mqtt.android.service.MqttAndroidClient;
//
///**
// * MQTT核心类
// */
//public class MQTTCore implements IMqttActionListener, MqttCallback {
//
//    private static final String TAG = "MQTTCore";
//    /**
//     * 单列
//     */
//    private static MQTTCore mInstance;
//
//    // 创建MQTT客户端
//    private MqttAndroidClient mqttAndroidClient;
//
//    private boolean isOnline = false;
//
//    public static MQTTCore getInstance() {
//        if (mInstance == null) {
//            synchronized (MQTTCore.class) {
//                if (mInstance == null) {
//                    mInstance = new MQTTCore();
//                }
//            }
//        }
//        return mInstance;
//    }
//
//    private MQTTCore(){
//        String url = "tcp://"+ Api.MQTT_HOST + ":" + Api.MQTT_PORT;
//        String deviceSn = UserCore.sharedCore().getDeviceSn();
//        mqttAndroidClient = new MqttAndroidClient(App.getContext(),url,deviceSn, Ack.AUTO_ACK);
//        mqttAndroidClient.setCallback(this);
//    }
//
//    public boolean isOnline() {
//        return isOnline;
//    }
//
//    public void init(){
//        connect();
//    }
//
//    public void connect(){
//        try {
//            // 连接到MQTT代理服务器
//            MqttConnectOptions options = new MqttConnectOptions();
//            options.setCleanSession(true);
//            options.setAutomaticReconnect(true);
//            options.setConnectionTimeout(10);
//            options.setKeepAliveInterval(60);
//            mqttAndroidClient.connect(options, null, this);
//
//        } catch (Exception e) {
//            throw new RuntimeException(e);
//        }
//    }
//
//    public void subscribe(){
//        try {
//            mqttAndroidClient.subscribe("device/get", 0);
//        } catch (Exception e) {
//            throw new RuntimeException(e);
//        }
//    }
//
//    public void disconnect(){
//        try {
//            mqttAndroidClient.disconnect();
//        } catch (Exception e) {
//            throw new RuntimeException(e);
//        }
//    }
//
//
//    @Override
//    public void onSuccess(IMqttToken asyncActionToken) {
//        Log.d(TAG, "onSuccess: MQTT连接成功 "+ UserCore.sharedCore().getDeviceSn());
//        try {
//            isOnline = true;
//            EventBusUtils.postMQTTStatus(1);
//            Map<String,Object> objc = new HashMap<>();
//            objc.put("status","1");
//            objc.put("deviceSn",UserCore.sharedCore().getDeviceSn());
//            MobclickAgent.onEventObject(App.getContext(),"mqtt_status",objc);
//            EventLogUtils.addEventLog("mqtt_status",objc);
//
//            subscribe();
//        }catch (Exception e){
//            EventLogUtils.addEventLog("mqtt_status_error",e.getMessage());
//        }
//    }
//
//    @Override
//    public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
//        Log.e(TAG, "onFailure: MQTT连接失败!");
//        try {
//            isOnline = false;
//            EventBusUtils.postMQTTStatus(0);
//            Map<String,Object> objc = new HashMap<>();
//            objc.put("status","0");
//            objc.put("deviceSn",UserCore.sharedCore().getDeviceSn());
//            objc.put("error",exception==null?"NULL":exception.getMessage());
//            MobclickAgent.onEventObject(App.getContext(),"mqtt_status",objc);
//            EventLogUtils.addEventLog("mqtt_status",objc);
//        }catch (Exception e){
//            EventLogUtils.addEventLog("mqtt_status_error",e.getMessage());
//        }
//    }
//
//    @Override
//    public void connectionLost(Throwable cause) {
//        Log.e(TAG, "connectionLost: MQTT连接断开!");
//        try {
//            isOnline = false;
//            EventBusUtils.postMQTTStatus(0);
//            Map<String,Object> objc = new HashMap<>();
//            objc.put("status","0");
//            objc.put("deviceSn",UserCore.sharedCore().getDeviceSn());
//            objc.put("error",cause==null?"NULL":cause.getMessage());
//            MobclickAgent.onEventObject(App.getContext(),"mqtt_status",objc);
//            EventLogUtils.addEventLog("mqtt_status",objc);
//        }catch (Exception e){
//            EventLogUtils.addEventLog("mqtt_status_error",e.getMessage());
//        }
//    }
//
//    @Override
//    public void messageArrived(String topic, MqttMessage message) throws Exception {
//        Log.d(TAG, "messageArrived: 收到消息");
//        Log.d(TAG, "messageArrived: topic="+topic);
//        Log.d(TAG, "messageArrived: message="+message.toString());
//        MQTTClientHandler.handlerMessage(message.toString());
//    }
//
//    @Override
//    public void deliveryComplete(IMqttDeliveryToken token) {
//        Log.d(TAG, "deliveryComplete: 消息发送成功");
//    }
//}
