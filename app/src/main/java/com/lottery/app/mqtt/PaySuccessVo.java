package com.lottery.app.mqtt;

import com.lottery.app.entity.TbWxUserRedeem;

import java.util.List;


/**
 * EventBus 事件
 */
public class PaySuccessVo extends MqttVo{

    public long orderId;
    public String orderNo;
    public int payState;
    public int totalFee;
    public Double balance;
    public String handpieceNum;
    public List<GoodVo> goods;
    public List<TbWxUserRedeem> redeems;

    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public int getPayState() {
        return payState;
    }

    public void setPayState(int payState) {
        this.payState = payState;
    }

    public int getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(int totalFee) {
        this.totalFee = totalFee;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public String getHandpieceNum() {
        return handpieceNum;
    }

    public void setHandpieceNum(String handpieceNum) {
        this.handpieceNum = handpieceNum;
    }

    public List<GoodVo> getGoods() {
        return goods;
    }

    public void setGoods(List<GoodVo> goods) {
        this.goods = goods;
    }

    public List<TbWxUserRedeem> getRedeems() {
        return redeems;
    }

    public void setRedeems(List<TbWxUserRedeem> redeems) {
        this.redeems = redeems;
    }
}
