package com.lottery.app.mqtt;

import android.util.Log;

import com.alibaba.fastjson.JSON;
import com.google.gson.Gson;
import com.lottery.app.core.App;
import com.lottery.app.core.UserCore;
import com.lottery.app.entity.TbWxUser;
import com.lottery.app.entity.TbWxUserRedeem;
import com.lottery.app.utils.EventBusUtils;
import com.lottery.app.utils.EventLogUtils;
import com.lottery.app.utils.HUD;
import com.umeng.analytics.MobclickAgent;

import java.util.HashMap;
import java.util.Map;


/**
 * MQTT 客户端 处理类
 */
public class MQTTClientHandler {

    private static final String TAG = "MQTTClientHandler";

    /**
     * 处理MQTT消息
     * @param msg
     */
    public static void handlerMessage(String msg){
        Log.e("MQTTClientHandler", "handlerMessage: "+msg );
        MqttVo mqttVo = new Gson().fromJson(msg, MqttVo.class);
        try {
            Map map = new HashMap();
            if (UserCore.isLogin()){
                map.put("userId",UserCore.sharedCore().getUserName());
                map.put("deviceSn",UserCore.sharedCore().getDeviceSn());
            }
            map.put("event",mqttVo.getEvent());
            map.put("data",msg);

            MobclickAgent.onEventObject(App.getContext(), "mqtt_event", map);
            EventLogUtils.addEventLog("mqtt_event",map);
        }catch (Exception e){
            Log.d(TAG, "handlerMessage() returned: " + e.getMessage());
            EventLogUtils.addEventLog("mqtt_event_error",e.getMessage());
        }

        if (mqttVo.getEvent().equals(MqttEvent.redeemEndWinning.name())){
            //兑奖结束[中奖]
            redeemEnd(mqttVo,true);
        } else if (mqttVo.getEvent().equals(MqttEvent.redeemEndNOWinning.name())){
            //兑奖结束[未中奖]
            redeemEnd(mqttVo,false);
        } else if (mqttVo.getEvent().equals(MqttEvent.redeemEndError.name())){
            //兑奖结束[一场2]
            HUD.Toast("兑奖异常");
        } else if (mqttVo.getEvent().equals(MqttEvent.ticketingEnd.name())){
            //上行，出票完成
        } else if (mqttVo.getEvent().equals(MqttEvent.ticketingEnd_REPLY.name())){
            // 出票完成之后，回执给客户端
        } else if (mqttVo.getEvent().equals(MqttEvent.paySuccess.name())){
            //订单支付成功
            PaySuccessVo paySuccessVo = new Gson().fromJson(msg, PaySuccessVo.class);
            paySuccess(paySuccessVo);
        } else if (mqttVo.getEvent().equals(MqttEvent.paySuccess_REPLY.name())){
            //订单支付成功,机器的回复
        } else if (mqttVo.getEvent().equals(MqttEvent.wxUserInfo.name())){
            // 获取微信用户信息
            try {
                TbWxUser tbWxUser = JSON.parseObject(JSON.toJSONString(mqttVo.getData()), TbWxUser.class);
                EventBusUtils.postGetWxUser(tbWxUser);
            }catch (Exception e){
                Log.d(TAG, "handlerMessage() returned: " + e.getMessage());
                HUD.Toast("推送微信用户信息异常");
            }
        }
    }

    private static void redeemEnd(MqttVo mqttVo,boolean isWinning) {
        // TODO Auto-generated method stub
        try {
            TbWxUserRedeem tbWxUserRedeem = JSON.parseObject(JSON.toJSONString(mqttVo.getData()), TbWxUserRedeem.class);
            if (tbWxUserRedeem!=null){
                EventBusUtils.postAddDeviceWxUserRedeems(tbWxUserRedeem);
            }else{
                HUD.Toast("兑奖异常");
            }
        }catch (Exception e){
            Log.d(TAG, "redeemEnd() returned: " + e.getMessage());
            HUD.Toast("兑奖解析数据异常");
        }
    }

    private static void paySuccess(PaySuccessVo paySuccessVo) {
        // TODO Auto-generated method stub
        EventBusUtils.postPaySuccess(paySuccessVo);
    }

}
