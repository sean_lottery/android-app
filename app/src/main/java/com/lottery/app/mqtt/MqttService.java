package com.lottery.app.mqtt;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import com.lottery.app.R;
import com.lottery.app.core.UserCore;
import com.lottery.app.ui.activity.HomeActivity;
import com.lottery.app.utils.EventLogUtils;

import static androidx.core.app.NotificationCompat.PRIORITY_MIN;

import java.util.HashMap;
import java.util.Map;

/**
 * MQTT服务 保活
 */
public class MqttService extends Service {

    private static final String TAG = "MqttService";

    /**
     * 添加计时器
     */
    private long runTime = 0;

    private boolean isRun = false;


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {

        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        startForeground();

        startTimer();

        MQTTClient.getInstance().buildMqtt();

    }

    private void startTimer(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                isRun = true;
                while (isRun){
                    try {
                        Thread.sleep(1000);
                        runTime++;
                        Log.d(TAG, "run:" + runTime + "s");
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }
    @Override
    public void onDestroy() {
        Map<String, String> map = new HashMap<>();
        map.put("body", "mqtt服务销毁");
        map.put("runTime", runTime + "");
        map.put("isOnline", MQTTClient.getInstance().isOnline() + "");
        EventLogUtils.addEventLog("mqtt_server", map);
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    /**
     * 创建通知通道
     * @param channelId
     * @param channelName
     * @return
     */
    @RequiresApi(Build.VERSION_CODES.O)
    private String createNotificationChannel(String channelId, String channelName){
        NotificationChannel chan = new NotificationChannel(channelId,
                channelName, NotificationManager.IMPORTANCE_NONE);
        chan.setLightColor(Color.BLUE);
        chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        NotificationManager service = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        service.createNotificationChannel(chan);
        return channelId;
    }

    private void startForeground() {
        String channelId = null;
        // 8.0 以上需要特殊处理
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            channelId = createNotificationChannel("com.lottery.app.service", "Mqtt服务");
        } else {
            channelId = "";
        }
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, channelId);
        Notification notification = builder.setOngoing(true)
                .setSmallIcon(R.mipmap.logo)
                .setPriority(PRIORITY_MIN)
                .setCategory(Notification.CATEGORY_SERVICE)
                .setContentTitle("App正在运行")
                .build();
        startForeground(1, notification);
        //定时更新通知
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true){
                    try {
                        Thread.sleep(1000 * 5);
                        Notification notification = builder.setOngoing(true)
                                .setSmallIcon(R.mipmap.logo)
                                .setPriority(PRIORITY_MIN)
                                .setCategory(Notification.CATEGORY_SERVICE)
                                .setContentTitle("App正在运行:"+runTime+"s")
                                .build();
                        startForeground(1, notification);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }




}
