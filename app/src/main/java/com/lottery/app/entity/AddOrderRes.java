package com.lottery.app.entity;

import java.util.List;

public class AddOrderRes {

    public  TbOrder order;
    public  Object wxOrderData;

    public TbOrder getOrder() {
        return order;
    }

    public void setOrder(TbOrder order) {
        this.order = order;
    }

    public Object getWxOrderData() {
        return wxOrderData;
    }

    public void setWxOrderData(Object wxOrderData) {
        this.wxOrderData = wxOrderData;
    }
}