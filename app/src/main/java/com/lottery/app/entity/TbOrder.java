package com.lottery.app.entity;

import java.math.BigDecimal;
import java.util.List;

/**
 * 订单对象 tb_order
 * 
 * @author xllyll
 * @date 2022-09-21
 */
public class TbOrder extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 订单编号 */
    private String orderNo;

    private String transactionId;

    /** 1微信支付 2账户余额支付 */
    private String payType;

    /** 0 待支付  2 支付完成  3未支付 */
    private String payStatus;

    /** 0交易中  1交易完成 */
    private String orderStatus;

    /** 支付时间 */
    private String payTime;

    /** 优惠金额 */
    private BigDecimal discountMoney;

    /** 退款金额 */
    private BigDecimal refundMoney;

    /** 出库时间 */
    private String outTime;

    /** 需出货数量 */
    private Long outNum;

    /** 交易总金额 */
    private BigDecimal totalMoney;

    /** 买家实付 */
    private BigDecimal paidMoney;

    /** 用户id */
    private Long userId;

    /** 设备编号 */
    private String equipmentNum;

    /** 出货状态  1待出货   2已出货 3全部成功  4 部分成功  5出货失败 6出货异常*/
    private String outStatus;

    private Long outSuccessNum;
    private Long outErrorNum;
    private Long outExpNum;

    /**硬件机头出票的数量 */
    private String handpieceNum;

    /** 用户电话号码 */
    private String phone;

    private String remark;

    /**
     *  部门ID
     */
    private int deptId;
    /**
     * 部门名称
     */
    private String deptName;
    private int ePId;
    private String ePName;

    private Long shopId;
    private String shopName;


    private List<TbOrderGood> goods;
   
    private List<TbOrderRefund> refunds;


    public String getHandpieceNum() {
        return handpieceNum;
    }

    public void setHandpieceNum(String handpieceNum) {
        this.handpieceNum = handpieceNum;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Long getOutSuccessNum() {
        return outSuccessNum;
    }

    public void setOutSuccessNum(Long outSuccessNum) {
        this.outSuccessNum = outSuccessNum;
    }

    public Long getOutErrorNum() {
        return outErrorNum;
    }

    public void setOutErrorNum(Long outErrorNum) {
        this.outErrorNum = outErrorNum;
    }

    public Long getOutExpNum() {
        return outExpNum;
    }

    public void setOutExpNum(Long outExpNum) {
        this.outExpNum = outExpNum;
    }

    public int getDeptId() {
        return deptId;
    }

    public void setDeptId(int deptId) {
        this.deptId = deptId;
    }

    public String getePName() {
        return ePName;
    }

    public void setePName(String ePName) {
        this.ePName = ePName;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public void setOrderNo(String orderNo) 
    {
        this.orderNo = orderNo;
    }

    public String getOrderNo() 
    {
        return orderNo;
    }
    public void setPayType(String payType) 
    {
        this.payType = payType;
    }

    public String getPayType() 
    {
        return payType;
    }
    public void setPayStatus(String payStatus) 
    {
        this.payStatus = payStatus;
    }

    public String getPayStatus() 
    {
        return payStatus;
    }
    public void setOrderStatus(String orderStatus) 
    {
        this.orderStatus = orderStatus;
    }

    public String getOrderStatus() 
    {
        return orderStatus;
    }
    public void setPayTime(String payTime)
    {
        this.payTime = payTime;
    }

    public String getPayTime()
    {
        return payTime;
    }
    public void setDiscountMoney(BigDecimal discountMoney) 
    {
        this.discountMoney = discountMoney;
    }

    public BigDecimal getDiscountMoney() 
    {
        return discountMoney;
    }
    public void setRefundMoney(BigDecimal refundMoney) 
    {
        this.refundMoney = refundMoney;
    }

    public BigDecimal getRefundMoney() 
    {
        return refundMoney;
    }
    public void setOutTime(String outTime)
    {
        this.outTime = outTime;
    }

    public String getOutTime()
    {
        return outTime;
    }
    public void setOutNum(Long outNum) 
    {
        this.outNum = outNum;
    }

    public Long getOutNum() 
    {
        return outNum;
    }
    public void setTotalMoney(BigDecimal totalMoney) 
    {
        this.totalMoney = totalMoney;
    }

    public BigDecimal getTotalMoney() 
    {
        return totalMoney;
    }
    public void setPaidMoney(BigDecimal paidMoney) 
    {
        this.paidMoney = paidMoney;
    }

    public BigDecimal getPaidMoney() 
    {
        return paidMoney;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setEquipmentNum(String equipmentNum) 
    {
        this.equipmentNum = equipmentNum;
    }

    public String getEquipmentNum() 
    {
        return equipmentNum;
    }


    public int getePId() {
        return ePId;
    }

    public void setePId(int ePId) {
        this.ePId = ePId;
    }

    public String getOutStatus() {
        return outStatus;
    }

    public void setOutStatus(String outStatus) {
        this.outStatus = outStatus;
    }

    public List<TbOrderGood> getGoods() {
        return goods;
    }

    public void setGoods(List<TbOrderGood> goods) {
        this.goods = goods;
    }

    public List<TbOrderRefund> getRefunds() {
        return refunds;
    }

    public void setRefunds(List<TbOrderRefund> refunds) {
        this.refunds = refunds;
    }

    
}
