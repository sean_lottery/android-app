package com.lottery.app.entity;



/**
 * 微信用户对象
 */
public class TbWxUser extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 用户电话号码 */
    private String phone;

    /** 微信openID */
    private String openid;

    /** 支付宝openID */
    private String zfbopenid;

    /** 昵称 */
    private String nickname;

    /** 性别 */
    private String sex;

    /** 城市 */
    private String city;

    /** 省 */
    private String province;

    /** 国家 */
    private String country;

    /** 头像 */
    private String headimgurl;

    /** uuid */
    private String unionid;

    /** 余额 */
    private Double balance;

    private String loginTime;

    private String createTime;



    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    private String flag;


    public String getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(String loginTime) {
        this.loginTime = loginTime;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getZfbopenid() {
        return zfbopenid;
    }

    public void setZfbopenid(String zfbopenid) {
        this.zfbopenid = zfbopenid;
    }

    public void setNickname(String nickname)
    {
        this.nickname = nickname;
    }

    public String getNickname()
    {
        return nickname;
    }
    public void setSex(String sex)
    {
        this.sex = sex;
    }

    public String getSex()
    {
        return sex;
    }
    public void setCity(String city)
    {
        this.city = city;
    }

    public String getCity()
    {
        return city;
    }
    public void setProvince(String province)
    {
        this.province = province;
    }

    public String getProvince()
    {
        return province;
    }
    public void setCountry(String country)
    {
        this.country = country;
    }

    public String getCountry()
    {
        return country;
    }
    public void setHeadimgurl(String headimgurl)
    {
        this.headimgurl = headimgurl;
    }

    public String getHeadimgurl()
    {
        return headimgurl;
    }
    public void setUnionid(String unionid)
    {
        this.unionid = unionid;
    }

    public String getUnionid()
    {
        return unionid;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }


}
