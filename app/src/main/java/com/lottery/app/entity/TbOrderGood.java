package com.lottery.app.entity;

import java.math.BigDecimal;

/**
 * 订单商品对象 tb_order_good
 * 
 * @author xllyll
 * @date 2022-09-21
 */
public class TbOrderGood extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 订单号 */
    private Long orderId;

    /** 商品ID */
    private Long goodId;

    /** 商品图片 */
    private String goodImg;

    /** 商品名称 */
    private String goodName;

    /** 货道号 */
    private String cargoNo;

    private Long cargoId;

    /** 商品价格 */
    private BigDecimal goodPrice;

    /** 商品数量 */
    private Integer goodNum;

    /** 总价 */
    private BigDecimal totalPrice;

    /** 出货总量 */
    private Long outNum;

    /** 0 未支付  1全部成功   2部分成功  3失败 */
    private String outStatus;
    
    public String getCargoNo() {
        return cargoNo;
    }

    public void setCargoNo(String cargoNo) {
        this.cargoNo = cargoNo;
    }

    public Long getCargoId() {
        return cargoId;
    }

    public void setCargoId(Long cargoId) {
        this.cargoId = cargoId;
    }

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setOrderId(Long orderId) 
    {
        this.orderId = orderId;
    }

    public Long getOrderId() 
    {
        return orderId;
    }
    public void setGoodId(Long goodId) 
    {
        this.goodId = goodId;
    }

    public Long getGoodId() 
    {
        return goodId;
    }
    public void setGoodImg(String goodImg) 
    {
        this.goodImg = goodImg;
    }

    public String getGoodImg() 
    {
        return goodImg;
    }
    public void setGoodName(String goodName) 
    {
        this.goodName = goodName;
    }

    public String getGoodName() 
    {
        return goodName;
    }
    
    public void setGoodPrice(BigDecimal goodPrice) 
    {
        this.goodPrice = goodPrice;
    }

    public BigDecimal getGoodPrice() 
    {
        return goodPrice;
    }
    public void setGoodNum(Integer goodNum)
    {
        this.goodNum = goodNum;
    }

    public Integer getGoodNum()
    {
        return goodNum;
    }
    public void setTotalPrice(BigDecimal totalPrice) 
    {
        this.totalPrice = totalPrice;
    }

    public BigDecimal getTotalPrice() 
    {
        return totalPrice;
    }
    public void setOutNum(Long outNum) 
    {
        this.outNum = outNum;
    }

    public Long getOutNum() 
    {
        return outNum;
    }
    public void setOutStatus(String outStatus) 
    {
        this.outStatus = outStatus;
    }

    public String getOutStatus() 
    {
        return outStatus;
    }


}
