package com.lottery.app.entity;


import com.lottery.app.utils.DateUtils;
import com.lottery.app.utils.EventLogUtils;

import java.util.Date;

/**
 * 微信兑奖对象 tb_wx_user_redeem
 * 
 * @author xllyll
 * @date 2022-08-04
 */
public class TbWxUserRedeem
{
    private static final long serialVersionUID = 1L;

    private String rescode;

    /** id */
    private Long id;

    /** 订单编号 */
    private String orderId;

    /** 1入账  2提现 */
    private String type;

    /** 金额 */
    private Double money;

    /** 方案代码 */
    private String plancode;

    /** 方案名称 */
    private String planname;

    /** 生产批次 */
    private String produceorder;

    /** 票流水号 */
    private String ticksn;

    /** 奖等信息 */
    private String winlevelstring;

    /** 中奖金额 */
    private String winprize;

    /** 关联微信用户ID */
    private Long wxUserId;


    /**
     * 序列号
     */
    private String sn;

    private String deviceSn;


    private String wxNickName;
    private String headImgUrl;

    private Long stationId;

    private String stationName;


    /** 创建时间 */
    private String createTime;

    /** 更新时间 */
    private String updateTime;

    public String getDeviceSn() {
        return deviceSn;
    }

    public void setDeviceSn(String deviceSn) {
        this.deviceSn = deviceSn;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public Long getStationId() {
        return stationId;
    }

    public void setStationId(Long stationId) {
        this.stationId = stationId;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getWxNickName() {
        return wxNickName;
    }

    public void setWxNickName(String wxNickName) {
        this.wxNickName = wxNickName;
    }

    public String getHeadImgUrl() {
        return headImgUrl;
    }

    public void setHeadImgUrl(String headImgUrl) {
        this.headImgUrl = headImgUrl;
    }

    public String getRescode() {
        return rescode;
    }

    public void setRescode(String rescode) {
        this.rescode = rescode;
    }


    public TbWxUserRedeem() {

    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getType() 
    {
        return type;
    }

    public Double getMoney() {
        return money;
    }

    public void setMoney(Double money) {
        this.money = money;
    }

    public void setPlancode(String plancode)
    {
        this.plancode = plancode;
    }

    public String getPlancode() 
    {
        return plancode;
    }
    public void setPlanname(String planname) 
    {
        this.planname = planname;
    }

    public String getPlanname() 
    {
        return planname;
    }
    public void setProduceorder(String produceorder) 
    {
        this.produceorder = produceorder;
    }

    public String getProduceorder() 
    {
        return produceorder;
    }
    public void setTicksn(String ticksn) 
    {
        this.ticksn = ticksn;
    }

    public String getTicksn() 
    {
        return ticksn;
    }
    public void setWinlevelstring(String winlevelstring) 
    {
        this.winlevelstring = winlevelstring;
    }

    public String getWinlevelstring() 
    {
        return winlevelstring;
    }
    public void setWinprize(String winprize) 
    {
        this.winprize = winprize;
    }

    public String getWinprize() 
    {
        return winprize;
    }
    public void setWxUserId(Long wxUserId) 
    {
        this.wxUserId = wxUserId;
    }

    public Long getWxUserId() 
    {
        return wxUserId;
    }


    public String getCreateTime() {

        // 判断createTime 包含‘-’
        if (createTime.contains("-")) {
            return createTime;
        }
        Long time = null;
        try {
            time = Long.parseLong(createTime);
            if (time != null) {
                Date date = new Date(time);
                createTime = DateUtils.dateToString(date);
            }
        }catch (Exception e) {
            e.printStackTrace();
            EventLogUtils.addEventLog("time_error",createTime + ">>" +e.getMessage());
        }
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }
}
