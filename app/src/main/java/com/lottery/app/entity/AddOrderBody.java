package com.lottery.app.entity;

import java.util.List;

/**
 * 订单请求体
 */
public class AddOrderBody {
    /**
     * app设备编号
     */
    private String appDeviceSn;
    private String equipmentNum; // 设备编号
    private List<OrderGoods> orderGoods; // 订单商品列表
    private String orderStatus; // 订单状态
    private String payStatus; // 支付状态
    private String phone; // 手机号
    private String payType; // 支付类型


    // getters and setters


    public String getAppDeviceSn() {
        return appDeviceSn;
    }

    public void setAppDeviceSn(String appDeviceSn) {
        this.appDeviceSn = appDeviceSn;
    }

    public String getEquipmentNum() {
        return equipmentNum;
    }

    public void setEquipmentNum(String equipmentNum) {
        this.equipmentNum = equipmentNum;
    }

    public List<OrderGoods> getOrderGoods() {
        return orderGoods;
    }

    public void setOrderGoods(List<OrderGoods> orderGoods) {
        this.orderGoods = orderGoods;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(String payStatus) {
        this.payStatus = payStatus;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }
}