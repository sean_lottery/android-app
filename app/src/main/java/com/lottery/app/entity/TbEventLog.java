package com.lottery.app.entity;


/**
 * 事件统计对象
 */
public class TbEventLog extends BaseEntity
{

    /** 主键ID */
    private Long id;

    private String userId;

    private String deviceSn;

    /** 类型[0默认类型] */
    private Long type;

    /** 事件 */
    private String event;

    /** 内容 */
    private String content;


    private String systemVersion;

    /** 设备品牌 */
    private String deviceBrand;

    /** 设备型号 */
    private String deviceModel;
    private String versionCode;
    private String versionName;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDeviceSn() {
        return deviceSn;
    }

    public void setDeviceSn(String deviceSn) {
        this.deviceSn = deviceSn;
    }

    public void setType(Long type)
    {
        this.type = type;
    }

    public Long getType() 
    {
        return type;
    }
    public void setEvent(String event) 
    {
        this.event = event;
    }

    public String getEvent() 
    {
        return event;
    }
    public void setContent(String content) 
    {
        this.content = content;
    }

    public String getContent() 
    {
        return content;
    }

    public String getSystemVersion() {
        return systemVersion;
    }

    public void setSystemVersion(String systemVersion) {
        this.systemVersion = systemVersion;
    }

    public String getDeviceBrand() {
        return deviceBrand;
    }

    public void setDeviceBrand(String deviceBrand) {
        this.deviceBrand = deviceBrand;
    }

    public String getDeviceModel() {
        return deviceModel;
    }

    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }

    public String getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(String versionCode) {
        this.versionCode = versionCode;
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }
}
