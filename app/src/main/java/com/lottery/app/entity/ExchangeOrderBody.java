package com.lottery.app.entity;

import java.math.BigDecimal;
import java.util.List;

public class ExchangeOrderBody {
    /**
     * app设备编号
     */
    private String appDeviceSn;
    /** 用户id */
    private Long userId;

    /** 设备编号 */
    private String equipmentNum;

    /** 用户号码 */
    private String phone;

    /** 1微信支付 2 账户余额支付 3支付宝 4 银联支付 5 聚合支付*/
    private String payType = "1";
    /// 购买商品的列表
    private List<TbOrderGood> orderGoods;
    //要换购的彩票IDS
    private List<Long> redmmIds;
    //兑换的金额
    private BigDecimal redmmMoney;

    public String getAppDeviceSn() {
        return appDeviceSn;
    }

    public void setAppDeviceSn(String appDeviceSn) {
        this.appDeviceSn = appDeviceSn;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getEquipmentNum() {
        return equipmentNum;
    }

    public void setEquipmentNum(String equipmentNum) {
        this.equipmentNum = equipmentNum;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public List<TbOrderGood> getOrderGoods() {
        return orderGoods;
    }

    public void setOrderGoods(List<TbOrderGood> orderGoods) {
        this.orderGoods = orderGoods;
    }

    public List<Long> getRedmmIds() {
        return redmmIds;
    }

    public void setRedmmIds(List<Long> redmmIds) {
        this.redmmIds = redmmIds;
    }

    public BigDecimal getRedmmMoney() {
        return redmmMoney;
    }

    public void setRedmmMoney(BigDecimal redmmMoney) {
        this.redmmMoney = redmmMoney;
    }
}
