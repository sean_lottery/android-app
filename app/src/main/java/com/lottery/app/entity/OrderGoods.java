package com.lottery.app.entity;

import java.math.BigDecimal;
import java.util.Map;

/**
 * OrderGood类用于表示订单中的商品信息
 */
public class OrderGoods {
    private Long cargoId; // 货物ID
    private String cargoNo; // 货物编号
    private Long goodId; // 商品ID
    private String goodImg; // 商品图片
    private String goodName; // 商品名称
    private Integer goodNum; // 商品数量
    private BigDecimal goodPrice; // 商品价格
    private Map<String, Object> params; // 商品参数

    // getters and setters
    public Long getCargoId() { // 获取货物ID
        return cargoId;
    }

    public void setCargoId(Long cargoId) { // 设置货物ID
        this.cargoId = cargoId;
    }

    public String getCargoNo() { // 获取货物编号
        return cargoNo;
    }

    public void setCargoNo(String cargoNo) { // 设置货物编号
        this.cargoNo = cargoNo;
    }

    public Long getGoodId() { // 获取商品ID
        return goodId;
    }

    public void setGoodId(Long goodId) { // 设置商品ID
        this.goodId = goodId;
    }

    public String getGoodImg() { // 获取商品图片
        return goodImg;
    }

    public void setGoodImg(String goodImg) { // 设置商品图片
        this.goodImg = goodImg;
    }

    public String getGoodName() { // 获取商品名称
        return goodName;
    }

    public void setGoodName(String goodName) { // 设置商品名称
        this.goodName = goodName;
    }

    public Integer getGoodNum() { // 获取商品数量
        return goodNum;
    }

    public void setGoodNum(Integer goodNum) { // 设置商品数量
        this.goodNum = goodNum;
    }

    public BigDecimal getGoodPrice() { // 获取商品价格
        return goodPrice;
    }

    public void setGoodPrice(BigDecimal goodPrice) { // 设置商品价格
        this.goodPrice = goodPrice;
    }

    public Map<String, Object> getParams() { // 获取商品参数
        return params;
    }

    public void setParams(Map<String, Object> params) { // 设置商品参数
        this.params = params;
    }
}