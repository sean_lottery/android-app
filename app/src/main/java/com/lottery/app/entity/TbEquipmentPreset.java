package com.lottery.app.entity;

public class TbEquipmentPreset {

    /** ID */
    private Long ePId;

    /** 设备编号 */
    private String ePNum;

    /** 设备名称 */
    private String ePName;

    /** 所属组织id */
    private Long deptId;

    /** 设备类型 */
    private Long eTypeId;

    /** 是否启用 */
    private String isYn;

    /** 设备状态  1在线  2 离线  */
    private String ePState;

    /** 流量卡ID */
    private Long flowCardId;

    /** 兑奖显示 */
    private String showRedeem;

    /** 待机广告 */
    private String showAd;

    /** 收银功能 */
    private String showIncome;

    /** 自动退款 */
    private String showRefund;

    /** 微信刷脸支付 */
    private String showWeiXin;

    /** 扫码支付 */
    private String showScodePay;

    /** 授权书显示 */
    private String showAuthorize;

    /** 设备兑奖 */
    private String showEquipmentRedeem;

    /** 商品的ID */
    private Long shopId;

    /** 支付商户数组JSON，每个元素是tb_merchant记录ID */
    private String merchants;

    /** 代销证图片地址 */
    private String consignmentCertificate;

    /** 二维码地址 */
    private String qrcodeUrl;


    private String deptName;
    private String typeName;
    private String flowName;

    private String etImg;
    private String shopName;
    private String shopAddress;
    private String cardCid;
    private String cardNum;

    private String serviceStart;

    private String serviceEnd;



    /// 电机测试的时候，货道ID
    private long cargoLaneId;

    public Long getePId() {
        return ePId;
    }

    public void setePId(Long ePId) {
        this.ePId = ePId;
    }

    public String getePNum() {
        return ePNum;
    }

    public void setePNum(String ePNum) {
        this.ePNum = ePNum;
    }

    public String getePName() {
        return ePName;
    }

    public void setePName(String ePName) {
        this.ePName = ePName;
    }

    public Long getDeptId() {
        return deptId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    public Long geteTypeId() {
        return eTypeId;
    }

    public void seteTypeId(Long eTypeId) {
        this.eTypeId = eTypeId;
    }

    public String getIsYn() {
        return isYn;
    }

    public void setIsYn(String isYn) {
        this.isYn = isYn;
    }

    public String getePState() {
        return ePState;
    }

    public void setePState(String ePState) {
        this.ePState = ePState;
    }

    public Long getFlowCardId() {
        return flowCardId;
    }

    public void setFlowCardId(Long flowCardId) {
        this.flowCardId = flowCardId;
    }

    public String getShowRedeem() {
        return showRedeem;
    }

    public void setShowRedeem(String showRedeem) {
        this.showRedeem = showRedeem;
    }

    public String getShowAd() {
        return showAd;
    }

    public void setShowAd(String showAd) {
        this.showAd = showAd;
    }

    public String getShowIncome() {
        return showIncome;
    }

    public void setShowIncome(String showIncome) {
        this.showIncome = showIncome;
    }

    public String getShowRefund() {
        return showRefund;
    }

    public void setShowRefund(String showRefund) {
        this.showRefund = showRefund;
    }

    public String getShowWeiXin() {
        return showWeiXin;
    }

    public void setShowWeiXin(String showWeiXin) {
        this.showWeiXin = showWeiXin;
    }

    public String getShowScodePay() {
        return showScodePay;
    }

    public void setShowScodePay(String showScodePay) {
        this.showScodePay = showScodePay;
    }

    public String getShowAuthorize() {
        return showAuthorize;
    }

    public void setShowAuthorize(String showAuthorize) {
        this.showAuthorize = showAuthorize;
    }

    public String getShowEquipmentRedeem() {
        return showEquipmentRedeem;
    }

    public void setShowEquipmentRedeem(String showEquipmentRedeem) {
        this.showEquipmentRedeem = showEquipmentRedeem;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public String getMerchants() {
        return merchants;
    }

    public void setMerchants(String merchants) {
        this.merchants = merchants;
    }

    public String getConsignmentCertificate() {
        return consignmentCertificate;
    }

    public void setConsignmentCertificate(String consignmentCertificate) {
        this.consignmentCertificate = consignmentCertificate;
    }

    public String getQrcodeUrl() {
        return qrcodeUrl;
    }

    public void setQrcodeUrl(String qrcodeUrl) {
        this.qrcodeUrl = qrcodeUrl;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getFlowName() {
        return flowName;
    }

    public void setFlowName(String flowName) {
        this.flowName = flowName;
    }

    public String getEtImg() {
        return etImg;
    }

    public void setEtImg(String etImg) {
        this.etImg = etImg;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getShopAddress() {
        return shopAddress;
    }

    public void setShopAddress(String shopAddress) {
        this.shopAddress = shopAddress;
    }

    public String getCardCid() {
        return cardCid;
    }

    public void setCardCid(String cardCid) {
        this.cardCid = cardCid;
    }

    public String getCardNum() {
        return cardNum;
    }

    public void setCardNum(String cardNum) {
        this.cardNum = cardNum;
    }

    public String getServiceStart() {
        return serviceStart;
    }

    public void setServiceStart(String serviceStart) {
        this.serviceStart = serviceStart;
    }

    public String getServiceEnd() {
        return serviceEnd;
    }

    public void setServiceEnd(String serviceEnd) {
        this.serviceEnd = serviceEnd;
    }

    public long getCargoLaneId() {
        return cargoLaneId;
    }

    public void setCargoLaneId(long cargoLaneId) {
        this.cargoLaneId = cargoLaneId;
    }
}
