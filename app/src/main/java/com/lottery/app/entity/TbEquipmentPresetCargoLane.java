package com.lottery.app.entity;

import java.math.BigDecimal;

public class TbEquipmentPresetCargoLane {

    /** ID */
    private Long id;

    /** 设备类型，货道类型关联的ID */
    private Long ePCId;

    /** 商品id */
    private Long goodId;

    /** 设备ID */
    private Long equipmentId;

    /** 是否启用 */
    private String isYn;

    /** 当前库存 */
    private Integer inStock;

    /** 库存告警 */
    private Integer inStockWaring;

    /** 货道状态 货道状态0 正常    1缺货 2 故障*/
    private String state;

    /** 状态说明  */
    private String stateDesc;

    /** 货道号 */
    private String ePCNum;


    //货道ID
    private Long cLtId;
    /** 彩票名称 */
    private String goodName;
    /** 彩票图片 */
    private String goodImg;
    /** 彩票面值 */
    private BigDecimal goodPrice;
    private String ctTitle;
    private String ctNum;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getePCId() {
        return ePCId;
    }

    public void setePCId(Long ePCId) {
        this.ePCId = ePCId;
    }

    public Long getGoodId() {
        return goodId;
    }

    public void setGoodId(Long goodId) {
        this.goodId = goodId;
    }

    public Long getEquipmentId() {
        return equipmentId;
    }

    public void setEquipmentId(Long equipmentId) {
        this.equipmentId = equipmentId;
    }

    public String getIsYn() {
        return isYn;
    }

    public void setIsYn(String isYn) {
        this.isYn = isYn;
    }

    public Integer getInStock() {
        return inStock;
    }

    public void setInStock(Integer inStock) {
        this.inStock = inStock;
    }

    public Integer getInStockWaring() {
        return inStockWaring;
    }

    public void setInStockWaring(Integer inStockWaring) {
        this.inStockWaring = inStockWaring;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getStateDesc() {
        return stateDesc;
    }

    public void setStateDesc(String stateDesc) {
        this.stateDesc = stateDesc;
    }

    public String getePCNum() {
        return ePCNum;
    }

    public void setePCNum(String ePCNum) {
        this.ePCNum = ePCNum;
    }

    public Long getcLtId() {
        return cLtId;
    }

    public void setcLtId(Long cLtId) {
        this.cLtId = cLtId;
    }

    public String getGoodName() {
        return goodName;
    }

    public void setGoodName(String goodName) {
        this.goodName = goodName;
    }

    public String getGoodImg() {
        return goodImg;
    }

    public void setGoodImg(String goodImg) {
        this.goodImg = goodImg;
    }

    public BigDecimal getGoodPrice() {
        return goodPrice;
    }

    public void setGoodPrice(BigDecimal goodPrice) {
        this.goodPrice = goodPrice;
    }

    public String getCtTitle() {
        return ctTitle;
    }

    public void setCtTitle(String ctTitle) {
        this.ctTitle = ctTitle;
    }

    public String getCtNum() {
        return ctNum;
    }

    public void setCtNum(String ctNum) {
        this.ctNum = ctNum;
    }
}
