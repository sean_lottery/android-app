package com.lottery.app.entity;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 退款订单对象 tb_order_refund
 *
 * @author ruoyi
 * @date 2022-09-21
 */
public class TbOrderRefund extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * $column.columnComment
     */
    private Long id;

    /**
     * 退款编号
     */
    private String refundNo;

    private String transactionId;

    /**
     * 退款备注
     */
    private String refundRemark;

    /**
     * 1退款成功  2退款失败
     */
    private String refundStatus;

    /**
     * 退款创建时间
     */
    private Date refundCreateTime;

    /**
     * 退款更新时间
     */
    private String refundUpdateTime;

    /**
     * 订单号
     */
    private Long orderId;

    /**
     * 退款金额
     */
    private BigDecimal refundMoney;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRefundNo() {
        return refundNo;
    }

    public void setRefundNo(String refundNo) {
        this.refundNo = refundNo;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getRefundRemark() {
        return refundRemark;
    }

    public void setRefundRemark(String refundRemark) {
        this.refundRemark = refundRemark;
    }

    public String getRefundStatus() {
        return refundStatus;
    }

    public void setRefundStatus(String refundStatus) {
        this.refundStatus = refundStatus;
    }

    public Date getRefundCreateTime() {
        return refundCreateTime;
    }

    public void setRefundCreateTime(Date refundCreateTime) {
        this.refundCreateTime = refundCreateTime;
    }

    public String getRefundUpdateTime() {
        return refundUpdateTime;
    }

    public void setRefundUpdateTime(String refundUpdateTime) {
        this.refundUpdateTime = refundUpdateTime;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public BigDecimal getRefundMoney() {
        return refundMoney;
    }

    public void setRefundMoney(BigDecimal refundMoney) {
        this.refundMoney = refundMoney;
    }
}
