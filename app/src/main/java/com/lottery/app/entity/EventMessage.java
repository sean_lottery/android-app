package com.lottery.app.entity;

public class EventMessage {

    public static final String EVENT_LOGOUT = "event_logout";

    public static final String EVENT_MQTT_STATUS = "event_mqtt_status";
    public static final String EVENT_CHOOSE_DEVICE = "event_choose_device";
    public static final String EVENT_PAY_SUCCESS = "event_pay_success";
    public static final String EVENT_ADD_DEVICE_WX_USER_REDEEMS = "event_add_device_wx_user_redeems";
    public static final String EVENT_GET_WX_USER = "event_get_wx_user";

    public static final String EVENT_RELOAD_REDEEMS = "event_reload_redeems";

    private String event;

    private Object data;

    public EventMessage() {
    }

    public EventMessage(String event) {
        this.event = event;
    }

    public EventMessage(String event, Object data) {
        this.event = event;
        this.data = data;
    }

    public String getEvent() {
        return event;
    }

    public EventMessage setEvent(String event) {
        this.event = event;
        return this;
    }

    public Object getData() {
        return data;
    }

    public EventMessage setData(Object data) {
        this.data = data;
        return this;
    }


}
