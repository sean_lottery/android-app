package com.lottery.app.entity;

public class RedeemBody {

    private String safeAreaCode;

    private String deviceSn; // 设备序列号


    public String getSafeAreaCode() {
        return safeAreaCode;
    }

    public void setSafeAreaCode(String safeAreaCode) {
        this.safeAreaCode = safeAreaCode;
    }

    public String getDeviceSn() {
        return deviceSn;
    }

    public void setDeviceSn(String deviceSn) {
        this.deviceSn = deviceSn;
    }
}
