package com.lottery.app.ui.activity;

import android.content.Context;
import android.os.Bundle;
import android.service.autofill.UserData;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewbinding.ViewBinding;

import com.alibaba.fastjson.JSON;
import com.lottery.app.BuildConfig;
import com.lottery.app.adapter.HomeDeviceAdapter;
import com.lottery.app.api.Api;
import com.lottery.app.api.HttpAction;
import com.lottery.app.core.DataCore;
import com.lottery.app.core.UserCore;
import com.lottery.app.databinding.ActivityExchangeBinding;
import com.lottery.app.entity.EndPayParms;
import com.lottery.app.entity.EventMessage;
import com.lottery.app.entity.ExchangeOrderBody;
import com.lottery.app.entity.PageResponse;
import com.lottery.app.entity.ResponseBody;
import com.lottery.app.entity.TbEquipmentPreset;
import com.lottery.app.entity.TbEquipmentPresetCargoLane;
import com.lottery.app.entity.TbOrderGood;
import com.lottery.app.entity.TbWxUser;
import com.lottery.app.entity.TbWxUserRedeem;
import com.lottery.app.ui.dialog.ShowQrCodeDialogFragment;
import com.lottery.app.utils.DeviceUtils;
import com.lottery.app.utils.EventBusUtils;
import com.lottery.app.utils.EventLogUtils;
import com.lottery.app.utils.HUD;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;

public class ExchangeActivity extends BaseActivity {

    private static final String TAG = "ExchangeActivity";
    private ActivityExchangeBinding binding;

    private HomeDeviceAdapter homeAdapter;

    private TbEquipmentPreset tbEquipmentPreset;

    private List<HomeDeviceAdapter.Data> tbEquipmentPresetCargoLanes;
    /**
     * 总金额
     */
    private BigDecimal totalAmount;

    /**
     * 已兑换金额
     */
    private BigDecimal redeemedAmount;

    @Override
    protected ViewBinding setViewBinding(Bundle savedInstanceState) {
        binding = ActivityExchangeBinding.inflate(getLayoutInflater());
        return binding;
    }
    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart: ");
        if (!EventBus.getDefault().isRegistered(this)) {//加上判断
            EventBus.getDefault().register(this);
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
    }

    /**
     * 页面暂停
     */
    @Override
    protected void onPause() {
        super.onPause();
        if (EventBus.getDefault().isRegistered(this)) {//加上判断
            EventBus.getDefault().unregister(this);
        }
    }

    @Override
    protected void setup() {
        super.setup();
        setTitle("换购");

        totalAmount = DataCore.sharedCore().getTotalAmount();

        redeemedAmount = BigDecimal.ZERO;

        updateUI();

        loadData();

        tbEquipmentPresetCargoLanes = new ArrayList<>();
        homeAdapter = new HomeDeviceAdapter(this,tbEquipmentPresetCargoLanes);
        homeAdapter.setOnItemNumberChangeListener(new HomeDeviceAdapter.OnItemNumberChangeListener() {
            @Override
            public void onItemNumberChange(int position, int number) {
                updateNumber();
            }
        });
        binding.recyclerView.setAdapter(homeAdapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        binding.recyclerView.setLayoutManager(layoutManager);
        binding.refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadData();
            }
        });
        binding.buyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addExchangeOrder();
            }
        });

        binding.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@androidx.annotation.NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                // 如果滑动了足够的距离，关闭软键盘
                if (shouldCloseKeyboard(recyclerView)) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (imm != null && getCurrentFocus() != null) {
                        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                    }
                }
            }
            private boolean shouldCloseKeyboard(RecyclerView recyclerView) {
                // 根据需要判断是否滑动了足够的距离
                int firstVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
                if (firstVisibleItemPosition != RecyclerView.NO_POSITION) {
                    // 这里假设滑动超过一个Item的高度就关闭键盘
                    View firstVisibleItem = recyclerView.getLayoutManager().findViewByPosition(firstVisibleItemPosition);
                    return firstVisibleItem.getTop() - recyclerView.getPaddingTop() < firstVisibleItem.getHeight();
                }
                return false;
            }
        });
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMessage(EventMessage message) {
        Log.w(TAG, "onEventMessage: "+ JSON.toJSONString(message));
        switch (message.getEvent()){
            case EventMessage.EVENT_GET_WX_USER:
                withdrawCommit(message);
                break;
            case EventMessage.EVENT_MQTT_STATUS:
                updateMQTTUI((Integer)message.getData());
                break;
        }
    }
    private void updateUI(){
        BigDecimal all = totalAmount;
        if (all==null){
            all = BigDecimal.ZERO;
        }
        BigDecimal use = redeemedAmount;
        if (use==null){
            use = BigDecimal.ZERO;
        }
        binding.totalMoneyTv.setText(all.subtract(use).toString());
    }

    /**
     * 加载数据
     */
    private void loadData() {
        binding.emptyLayout.setVisibility(View.VISIBLE);
        binding.refreshLayout.setVisibility(View.GONE);
        TbEquipmentPreset equipmentPreset = UserCore.sharedCore().getChooseDevice();
        if (equipmentPreset==null){
            return;
        }
        binding.emptyLayout.setVisibility(View.GONE);
        binding.refreshLayout.setVisibility(View.VISIBLE);
        Long deviceId = equipmentPreset.getePId();
        HttpAction.getEquipmentPresetInfo(deviceId, new Observer<ResponseBody<TbEquipmentPreset>>() {
            @Override
            public void onSubscribe(@io.reactivex.rxjava3.annotations.NonNull Disposable d) {
                Log.d(TAG, "onSubscribe: ");
            }

            @Override
            public void onNext(@io.reactivex.rxjava3.annotations.NonNull ResponseBody<TbEquipmentPreset> responseBody) {
                Log.d(TAG, "onNext: ");
                if (responseBody.getData()!=null){
                    binding.deviceNumTv.setText(responseBody.getData().getePNum());
                    tbEquipmentPreset = responseBody.getData();
                    binding.statusTv.setBackground(DeviceUtils.getDeviceStatusDrawable(tbEquipmentPreset.getePState()));
                }
            }

            @Override
            public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                Log.d(TAG, "onError: ");
            }

            @Override
            public void onComplete() {
                Log.d(TAG, "onComplete: ");
            }
        });

        HttpAction.getEquipmentPresetCargoLaneList(deviceId, 1, new Observer<ResponseBody<List<TbEquipmentPresetCargoLane>>>() {
            @Override
            public void onSubscribe(@io.reactivex.rxjava3.annotations.NonNull Disposable d) {

            }

            @Override
            public void onNext(@io.reactivex.rxjava3.annotations.NonNull ResponseBody<List<TbEquipmentPresetCargoLane>> listResponseBody) {
                binding.refreshLayout.setRefreshing(false);
                if (listResponseBody.getData()!=null){
                    List<HomeDeviceAdapter.Data> list = new ArrayList<>();
                    for (TbEquipmentPresetCargoLane tbEquipmentPresetCargoLane : listResponseBody.getData()) {
                        HomeDeviceAdapter.Data data = new HomeDeviceAdapter.Data();
                        data.setTbEquipmentPresetCargoLane(tbEquipmentPresetCargoLane);
                        list.add(data);
                    }
                    tbEquipmentPresetCargoLanes = list;
                    homeAdapter.setTbEquipmentPresetCargoLanes(tbEquipmentPresetCargoLanes);
                }
            }

            @Override
            public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                binding.refreshLayout.setRefreshing(false);
            }

            @Override
            public void onComplete() {

            }
        });
    }
    private int totalCount = 0;
    private int addCount = 0;

    /**
     * 重新构建换购列表
     */
    private void reBuildRedeems(){
        //重新构建换购列表
        List<TbWxUserRedeem> tbWxUserRedeems = DataCore.sharedCore().getDeviceWxUserRedeems();
        if (tbWxUserRedeems==null){
            return;
        }
        totalCount = tbWxUserRedeems.size();
        addCount = 0;
        List<TbWxUserRedeem> newRedeems = new ArrayList<>();
        Observer<ResponseBody<TbWxUserRedeem>> observer = new Observer<ResponseBody<TbWxUserRedeem>>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull ResponseBody<TbWxUserRedeem> res) {
                addCount++;
                if (res.getCode()==200 && res.getData()!=null){
                    newRedeems.add(res.getData());
                }else{
                    Map<String,Object> map = new HashMap<>();
                    map.put("res",res);
                    map.put("addCount",addCount);
                    EventLogUtils.addEventLog("getRedeem_err",map);
                }
                if (addCount==totalCount){
                    HUD.dismiss();
                    EventBusUtils.postReloadRedeems(newRedeems);
                    finish();
                }
            }

            @Override
            public void onError(@NonNull Throwable e) {
                addCount++;
                Map<String,Object> map = new HashMap<>();
                map.put("err",e.getMessage());
                map.put("addCount",addCount);
                EventLogUtils.addEventLog("getRedeem_err",map);
                if (addCount==totalCount){
                    HUD.dismiss();
                    HUD.Toast("加载兑奖彩票信息失败");
                    DataCore.sharedCore().getDeviceWxUserRedeems().clear();
                    finish();
                }
            }

            @Override
            public void onComplete() {

            }
        };
        HUD.show(this);
        for (TbWxUserRedeem tbWxUserRedeem : tbWxUserRedeems) {
            HttpAction.getRedeemInfo(tbWxUserRedeem.getId(), observer);
        }
    }

    /**
     * 添加换购订单
     */
    private void addExchangeOrder(){
        //判读是debug
        if (BuildConfig.DEBUG){
            reBuildRedeems();
            return;
        }
        //获取选中的货道
        List<HomeDeviceAdapter.Data> selectData = new ArrayList<>();
        for (HomeDeviceAdapter.Data data : tbEquipmentPresetCargoLanes) {
            if (data.getNumber()>0){
                selectData.add(data);
            }
        }
        BigDecimal orderMoney = BigDecimal.ZERO;
        //获取订单商品
        List<TbOrderGood> orderGoods = new ArrayList<>();
        for (HomeDeviceAdapter.Data data : selectData) {
            TbOrderGood orderGood = new TbOrderGood();
            orderGood.setCargoId(data.getTbEquipmentPresetCargoLane().getId());
            orderGood.setCargoNo(data.getTbEquipmentPresetCargoLane().getePCNum());
            orderGood.setGoodId(data.getTbEquipmentPresetCargoLane().getGoodId());
            orderGood.setGoodImg(data.getTbEquipmentPresetCargoLane().getGoodImg());
            orderGood.setGoodName(data.getTbEquipmentPresetCargoLane().getGoodName());
            orderGood.setGoodNum(data.getNumber());
            orderGood.setGoodPrice(data.getTbEquipmentPresetCargoLane().getGoodPrice());
            orderGoods.add(orderGood);
            orderMoney=orderMoney.add(data.getTbEquipmentPresetCargoLane().getGoodPrice().multiply(BigDecimal.valueOf(data.getNumber())));
        }
        //构建换购订单参数
        ExchangeOrderBody exchangeOrderBody = new ExchangeOrderBody();
        exchangeOrderBody.setAppDeviceSn(UserCore.sharedCore().getDeviceSn());
        exchangeOrderBody.setEquipmentNum(tbEquipmentPreset.getePNum());
        exchangeOrderBody.setOrderGoods(orderGoods);
        exchangeOrderBody.setPayType("2");

        List<TbWxUserRedeem> redeems = DataCore.sharedCore().getDeviceWxUserRedeems();
        if (redeems!=null){
            BigDecimal totalMoney = BigDecimal.ZERO;
            List<Long> ids = new ArrayList<>();
            for (TbWxUserRedeem tbWxUserRedeem : redeems) {
                if (tbWxUserRedeem.getType().equals("3")){
                    BigDecimal win = BigDecimal.valueOf(Double.valueOf(tbWxUserRedeem.getWinprize()));
                    BigDecimal rate = BigDecimal.valueOf(tbWxUserRedeem.getMoney()==null?0.0:tbWxUserRedeem.getMoney());
                    totalMoney = totalMoney.add(win.subtract(rate));
                    ids.add(tbWxUserRedeem.getId());
                }
            }
            exchangeOrderBody.setRedmmIds(ids);
            exchangeOrderBody.setRedmmMoney(totalMoney);
        }

        if (orderMoney.doubleValue() > totalAmount.doubleValue()){
            HUD.Toast("金额不足");
            return;
        }
        BigDecimal finalOrderMoney = orderMoney;

        HUD.show(this);
        HttpAction.addOrderBalanceticai(exchangeOrderBody, new Observer<ResponseBody>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull ResponseBody responseBody) {
                HUD.dismiss();
                if (responseBody.getCode()==200){
                    redeemedAmount = finalOrderMoney;
                    updateUI();
                    if (totalAmount.subtract(redeemedAmount).doubleValue()>0){
                        HUD.Toast("兑换成功，剩余金额"+totalAmount.subtract(redeemedAmount));
                        totalAmount = totalAmount.subtract(redeemedAmount);
                        redeemedAmount = BigDecimal.ZERO;
                        updateUI();
                        loadData();
                        reBuildRedeems();
                        //withdraw();
                    }else{
                        DataCore.sharedCore().getDeviceWxUserRedeems().clear();
                        HUD.Toast("兑换成功");
                        finish();
                    }
                }else{
                    Map<String,Object> map = new HashMap<>();
                    map.put("res",responseBody);
                    map.put("req",exchangeOrderBody);
                    EventLogUtils.addEventLog("exchange_error",map);
                    HUD.Toast(responseBody.getMsg());
                }
            }

            @Override
            public void onError(@NonNull Throwable e) {
                HUD.dismiss();
                Map<String,Object> map = new HashMap<>();
                map.put("error",e.getMessage());
                map.put("req",exchangeOrderBody);
                EventLogUtils.addEventLog("exchange_error",map);
                HUD.Toast(e.getMessage());
            }

            @Override
            public void onComplete() {

            }
        });
    }

    private void updateNumber(){
        BigDecimal number = BigDecimal.valueOf(0.0);
        for (HomeDeviceAdapter.Data data : tbEquipmentPresetCargoLanes) {
            BigDecimal money = data.getTbEquipmentPresetCargoLane().getGoodPrice().multiply(BigDecimal.valueOf(data.getNumber()));
            number = number.add(money);
        }
        binding.allMoneyTv.setText(number+"");
    }


    private void withdraw(){
        BigDecimal totalMoney = BigDecimal.ZERO;
        List<TbWxUserRedeem> tbWxUserRedeems = DataCore.sharedCore().getDeviceWxUserRedeems();
        for (TbWxUserRedeem tbWxUserRedeem : tbWxUserRedeems) {
            totalMoney = totalMoney.add(BigDecimal.valueOf(Double.valueOf(tbWxUserRedeem.getWinprize())));
        }
        if (totalMoney.doubleValue()<=0){
            HUD.Toast("暂无可提现金额");
            return;
        }
        //redirect配的是回调地址，这个地址是你自己的服务器地址，微信会把code传给这个地址，然后你自己的服务器再去请求微信的接口，获取用户的openid等信息;
        String redirect = Api.WX_REDIRECT_URL;
        String STATE = UserCore.sharedCore().getDeviceSn();
        //微信授权页面
        String url = "https://open.weixin.qq.com/connect/oauth2/authorize?appid="+Api.WX_APP_ID+"&redirect_uri="+redirect+"&response_type=code&scope=snsapi_userinfo&state="+STATE+"#wechat_redirect";
        //String url = "https://open.weixin.qg.com/connect/oauth2/authorize?appid="+Api.WX_APP_ID+"&redirect_uri="+redirect;

        ShowQrCodeDialogFragment dialogFragment = new ShowQrCodeDialogFragment(url);
        dialogFragment.show(getSupportFragmentManager(),"ShowQrCodeDialogFragment");
    }
    //是否提现中[防止重复提交提现]
    private boolean isWithdrawing = false;
    /**
     * 提现确认
     * @param eventMessage
     */
    private void withdrawCommit(EventMessage eventMessage){
        try {
            if (isWithdrawing){
                HUD.Toast("提现中,请稍后...");
                return;
            }
            TbWxUser tbWxUser = (TbWxUser) eventMessage.getData();
            if (tbWxUser==null){
                HUD.Toast("获取用户信息失败");
                return;
            }
            String openId = tbWxUser.getOpenid();
            List<Long> ids = new ArrayList<>();
            BigDecimal totalMoney = totalAmount;
            List<TbWxUserRedeem> tbWxUserRedeems = DataCore.sharedCore().getDeviceWxUserRedeems();
            for (TbWxUserRedeem tbWxUserRedeem : tbWxUserRedeems) {
                if (tbWxUserRedeem.getType().equals("3")){
                    ids.add(tbWxUserRedeem.getId());
                }
            }

            EndPayParms endPayParms = new EndPayParms();
            endPayParms.setOpenid(openId);
            endPayParms.setAmount(totalMoney.intValue());
            endPayParms.setIds(ids);

            HUD.show(this);
            isWithdrawing = true;
            HttpAction.entPay3(endPayParms, new Observer<ResponseBody>() {
                @Override
                public void onSubscribe(@NonNull Disposable d) {

                }

                @Override
                public void onNext(@NonNull ResponseBody responseBody) {
                    HUD.dismiss();
                    EventLogUtils.addEventLog("withdraw",JSON.toJSONString(responseBody));
                    if (responseBody.getCode()==200){
                        HUD.Toast("提现成功");
                        DataCore.sharedCore().getDeviceWxUserRedeems().clear();
                        finish();
                    }else{
                        HUD.Toast(responseBody.getMsg());
                        Map<String,Object> map = new HashMap<>();
                        map.put("req",endPayParms);
                        map.put("res",responseBody);
                        EventLogUtils.addEventLog("withdraw_error",map);
                    }
                    isWithdrawing = false;
                }

                @Override
                public void onError(@NonNull Throwable e) {
                    HUD.dismiss();
                    HUD.Toast(e.getMessage());
                    EventLogUtils.addEventLog("withdraw_error",e.getMessage());
                    isWithdrawing = false;
                }

                @Override
                public void onComplete() {

                }
            });
        }catch (Exception e){
            HUD.Toast(e.getMessage());
            EventLogUtils.addEventLog("withdraw_exc_error",e.getMessage());
        }
    }
}