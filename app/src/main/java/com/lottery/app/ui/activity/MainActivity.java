package com.lottery.app.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationBarView;
import com.lottery.app.R;
import com.lottery.app.adapter.MainTabBarAdapter;
import com.lottery.app.api.HttpAction;
import com.lottery.app.core.UserCore;
import com.lottery.app.databinding.ActivityMainTabBinding;
import com.lottery.app.entity.EventMessage;
import com.lottery.app.entity.UserInfoResponse;
import com.lottery.app.ui.fragment.home.HomeFragment;
import com.lottery.app.ui.fragment.mine.MineFragment;
import com.lottery.app.ui.fragment.prize.CashPrizeFragment;
import com.lottery.app.ui.fragment.withdrawal.WithdrawalFragment;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    private ActivityMainTabBinding binding;

    private List<Fragment> fragments = new ArrayList<>();

    private HomeFragment homeFragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMainTabBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());


        setup();

    }
    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart: ");
        if (!EventBus.getDefault().isRegistered(this)) {//加上判断
            EventBus.getDefault().register(this);
        }
    }

    private void setup(){
        loadData();

        homeFragment = new HomeFragment();

        fragments.add(homeFragment);
        fragments.add(new CashPrizeFragment());
        fragments.add(new WithdrawalFragment());
        fragments.add(new MineFragment());

        MainTabBarAdapter adapter = new MainTabBarAdapter(getSupportFragmentManager(), getLifecycle());
        adapter.setFragments(fragments);
        ViewPager2 viewPager = binding.viewPager;
        viewPager.setAdapter(adapter);
        viewPager.setUserInputEnabled(false); //true:滑动，false：禁止滑动

        BottomNavigationView navView = findViewById(R.id.nav_view);
        navView.setOnItemSelectedListener(new NavigationBarView.OnItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.navigation_home:
                        viewPager.setCurrentItem(0);
                        break;
                    case R.id.navigation_dashboard:
                        viewPager.setCurrentItem(1);
                        break;
                    case R.id.navigation_notifications:
                        viewPager.setCurrentItem(2);
                        break;
                    case R.id.navigation_mine:
                        viewPager.setCurrentItem(3);
                        break;
                }
                return true;
            }
        });
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void deviceChanges(EventMessage message) {
        switch (message.getEvent()){
            case EventMessage.EVENT_CHOOSE_DEVICE:
                homeFragment.reload();
                break;
        }
    }
    /**
     * 加载用户信息
     */
    private void loadData(){
        HttpAction.getInfo(new Observer<UserInfoResponse>() {
            @Override
            public void onSubscribe(@io.reactivex.rxjava3.annotations.NonNull Disposable d) {

            }

            @Override
            public void onNext(@io.reactivex.rxjava3.annotations.NonNull UserInfoResponse userInfoResponse) {
                if (userInfoResponse.getCode() == 200) {
                    UserCore.sharedCore().setUserInfo(userInfoResponse);
                }else {
                    UserCore.sharedCore().setUserInfo(null);
                    startActivity(new Intent(MainActivity.this, LoginActivity.class));
                    finish();
                }
            }

            @Override
            public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                Log.e(TAG, "onError: ", e);
            }

            @Override
            public void onComplete() {
                Log.w(TAG, "onComplete: ");
            }
        });
    }

}