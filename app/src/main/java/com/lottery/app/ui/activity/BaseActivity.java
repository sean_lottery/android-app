package com.lottery.app.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewbinding.ViewBinding;

import com.lottery.app.databinding.ActivityBaseBinding;
import com.lottery.app.mqtt.MQTTClient;
import com.lottery.app.utils.HUD;

public abstract class BaseActivity extends AppCompatActivity {

    private ActivityBaseBinding baseBinding;

    /**
     * 启动Activity
     */
    public void startActivity(Class<?> cls) {
        startActivity(new Intent(this, cls));
    }

    protected abstract ViewBinding setViewBinding(Bundle savedInstanceState);

    protected boolean showActionBar() {
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!MQTTClient.getInstance().isOnline()) {
            updateMQTTUI(0);
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        baseBinding = ActivityBaseBinding.inflate(getLayoutInflater());

        //判断是否显示返回按钮
        if (getSupportActionBar() != null) {
            baseBinding.topLayout.setVisibility(View.GONE);
            getSupportActionBar().setDisplayHomeAsUpEnabled(showActionBar());
        }else{
            baseBinding.topLayout.setVisibility(showActionBar()?View.VISIBLE:View.GONE);
        }
        baseBinding.topLeftBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        View root = baseBinding.getRoot();
        setContentView(root);

        ViewBinding viewBinding = setViewBinding(savedInstanceState);
        if (viewBinding!=null){
            View subView = viewBinding.getRoot();
            ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            subView.setLayoutParams(params);
            baseBinding.centerLayout.addView(subView);
        }

        setup();
    }
    private String title;
    public void setTitle(String title){
        this.title = title;
        baseBinding.topTitleTv.setText(title);
    }
    /**
     * 是否显示返回按钮
     */
    public boolean showBack() {
        return true;
    }

    protected void setup(){

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:   //返回键的id
                this.finish();
                return false;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    /**
     * topright 添加view
     */
    public void addTopRightView(View view) {
        baseBinding.topRightLayout.addView(view);
    }


    public void updateMQTTUI(int status){

        //主线程执行
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (status==1) {
                    baseBinding.topTitleTv.setText(title);
                }else{
                    HUD.Toast("MQTT离线");
                    baseBinding.topTitleTv.setText(title+"(离线)");
                }
            }
        });
    }


}
