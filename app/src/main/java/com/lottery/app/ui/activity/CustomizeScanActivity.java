package com.lottery.app.ui.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;

import com.lottery.app.R;
import com.lottery.app.core.App;
import com.lottery.app.ui.view.ScanBoxView;
import com.lottery.app.utils.HUD;
import com.lottery.app.utils.ImageUtils;
import com.lottery.app.utils.ScanUtils;
import com.lottery.app.utils.StatusBarUtil;
import com.luck.picture.lib.basic.PictureSelector;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.entity.LocalMedia;
import com.yxing.ScanCodeActivity;
import com.yxing.ScanCodeConfig;

import java.io.File;
import java.util.List;

public class CustomizeScanActivity extends ScanCodeActivity {

    private static final String TAG = "CustomizeScanActivity";

    private ScanBoxView scanBoxView;
    @Override
    public int getLayoutId() {
        return R.layout.activity_customize_scan;
    }

    @Override
    public void initData() {
        super.initData();

        StatusBarUtil.setColor(this,getResources().getColor(R.color.main_color),0);

        findViewById(R.id.top_left_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        findViewById(R.id.top_album).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openAlbum();
            }
        });
    }

    private void openAlbum(){
        ImageUtils.openAlbum(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case PictureConfig.CHOOSE_REQUEST:
                    // 图片选择结果回调
                    List<LocalMedia> selectList = PictureSelector.obtainSelectorList(data);
                    // 例如 LocalMedia 里面返回三种path
                    // 1.media.getPath(); 为原图path
                    // 2.media.getCutPath();为裁剪后path，需判断media.isCut();是否为true
                    // 3.media.getCompressPath();为压缩后path，需判断media.isCompressed();是否为true
                    // 如果裁剪并压缩了，已取压缩路径为准，因为是先裁剪后压缩的
                    if (selectList != null && selectList.size() > 0) {
//                        String imagePath = selectList.get(0).getPath();
                        Bitmap bitmap = BitmapFactory.decodeFile(selectList.get(0).getRealPath());
                        String code =  ScanCodeConfig.scanningImageByBitmap(bitmap);
                        Log.i(TAG, "onActivityResult: "+code);
                        // 识别成功后，跳转到上一个页面
                        if (code != null) {
                            Intent intent = new Intent();
                            intent.putExtra(ScanCodeConfig.CODE_KEY, code);
                            intent.putExtra(ScanCodeConfig.CODE_TYPE, 0);
                            setResult(RESULT_OK, intent);
                            finish();
                        }else {
                            HUD.Toast("二维码识别失败");
                        }
                    }
                    break;
            }
        }
    }

    @Override
    public void onActivityReenter(int resultCode, Intent data) {
        super.onActivityReenter(resultCode, data);
    }
}