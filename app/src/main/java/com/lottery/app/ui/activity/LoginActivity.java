package com.lottery.app.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.lottery.app.R;
import com.lottery.app.api.HttpAction;
import com.lottery.app.core.UserCore;
import com.lottery.app.databinding.ActivityLoginBinding;
import com.lottery.app.entity.CaptchaImage;
import com.lottery.app.entity.LoginBody;
import com.lottery.app.entity.LoginResponse;
import com.lottery.app.utils.HUD;
import com.lottery.app.utils.ImageUtils;
import com.lottery.app.utils.StatusBarUtil;

import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{

    private ActivityLoginBinding binding;

    private CaptchaImage captchaImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityLoginBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        StatusBarUtil.setColor(this,getResources().getColor(R.color.login_bg_color),0);
        setup();
    }

    private void setup(){
        loadCaptchaImage();
        binding.loginLayout.setOnClickListener(this);
        binding.captchaImage.setOnClickListener(this);
        binding.userNameEt.setText(UserCore.sharedCore().getUserName());
        binding.passwordEt.setText(UserCore.sharedCore().getPassword());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.login_layout:
                login();
                break;
            case R.id.captcha_image:
                loadCaptchaImage();
                break;
        }
    }

    private void login() {
        if (captchaImage==null){
            HUD.Toast("未获取到验证码");
            return;
        }
        String username = binding.userNameEt.getText().toString();
        String password = binding.passwordEt.getText().toString();
        String code = binding.captchaEt.getText().toString();
        String uuid = captchaImage.getUuid();

        LoginBody loginBody = new LoginBody();
        loginBody.setUsername(username);
        loginBody.setPassword(password);
        loginBody.setCode(code);
        loginBody.setUuid(uuid);

        HUD.show(this);
        HttpAction.login(loginBody , new Observer<LoginResponse>() {

            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull LoginResponse res) {
                HUD.dismiss();
                if (res.getCode() == 200) {
                    // 登陆成功
                    UserCore.sharedCore().setUserName(username);
                    UserCore.sharedCore().setPassword(password);
                    UserCore.sharedCore().setToken(res.getToken());
                    startActivity(new Intent(LoginActivity.this, HomeActivity.class));
                    finish();
                }else{
                    HUD.Toast(res.getMsg());
                }
            }

            @Override
            public void onError(@NonNull Throwable e) {
                HUD.dismiss();
                HUD.Toast(e.getMessage());
            }

            @Override
            public void onComplete() {
                HUD.dismiss();
            }
        });
    }
    private void loadCaptchaImage(){
        HttpAction.getCaptchaImage(new Observer<CaptchaImage>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull CaptchaImage res) {
                captchaImage = res;
                if (captchaImage != null){
                    String base64 = "data:image/png;base64,"+captchaImage.getImg();
                    binding.captchaImage.setImageBitmap(ImageUtils.base642Bitmap(base64));
                }
            }

            @Override
            public void onError(@NonNull Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

}