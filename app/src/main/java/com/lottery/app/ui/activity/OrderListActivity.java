package com.lottery.app.ui.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewbinding.ViewBinding;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.View;

import com.lottery.app.R;
import com.lottery.app.adapter.CashPrizeResultAdapter;
import com.lottery.app.adapter.OrderListAdapter;
import com.lottery.app.api.HttpAction;
import com.lottery.app.core.UserCore;
import com.lottery.app.databinding.ActivityOrderListBinding;
import com.lottery.app.databinding.ActivitySettingBinding;
import com.lottery.app.entity.PageResponse;
import com.lottery.app.entity.TbOrder;
import com.lottery.app.entity.UserInfoResponse;
import com.lottery.app.utils.DateUtils;
import com.lottery.app.utils.HUD;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;

public class OrderListActivity extends BaseActivity {

    private int pageNum = 1;

    private String beginTime = "";

    private String endTime = "";

    private OrderListAdapter adapter;

    private List<TbOrder> orderList;

    private ActivityOrderListBinding binding;
    @Override
    protected ViewBinding setViewBinding(Bundle savedInstanceState) {
        binding = ActivityOrderListBinding.inflate(getLayoutInflater());
        return binding;
    }

    @Override
    protected void setup() {
        super.setup();
        setTitle("订单");
        buildDate();
        //加载数据
        loadData();
        //设置监听
        setListener();
        orderList = new ArrayList<>();

        adapter = new OrderListAdapter(this,orderList);
        binding.recyclerView.setAdapter(adapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        binding.recyclerView.setLayoutManager(layoutManager);
    }

    private void buildDate() {
        Date date = new Date();
        //TODO 构建数据
        endTime = DateUtils.dateToString(date, DateUtils.DATE_TO_STRING_SHORT_PATTERN);
        beginTime = endTime;
        //starttime 为前30天
        //beginTime = DateUtils.dateToString(new Date(date.getTime() - (30l*24*60*60*1000)), DateUtils.DATE_TO_STRING_SHORT_PATTERN);
        binding.startTimeTv.setText(beginTime);
        binding.endTimeTv.setText(endTime);
    }

    private void setListener() {
        binding.chooseStartTimeLayout.setOnClickListener(v -> {
            //TODO 开始时间
            //时间选择器
            DatePickerDialog datePickerDialog = new DatePickerDialog(this, (view, year, month, dayOfMonth) -> {
                beginTime = String.format("%d-%02d-%02d",year , (month + 1) , dayOfMonth);
                binding.startTimeTv.setText(beginTime);
                //开始时间不能大于结束时间
                Date startDate = DateUtils.stringToDate(beginTime, DateUtils.DATE_TO_STRING_SHORT_PATTERN);
                Date endDate = DateUtils.stringToDate(endTime, DateUtils.DATE_TO_STRING_SHORT_PATTERN);
                if (startDate.getTime() > endDate.getTime()) {
                    HUD.Toast("开始时间不能大于结束时间");
                }else{
                    pageNum = 1;
                    loadData();
                }
            }, DateUtils.getYear(beginTime), DateUtils.getMonth(beginTime) - 1, DateUtils.getDay(beginTime));
            datePickerDialog.show();
        });
        binding.chooseEndTimeLayout.setOnClickListener(v -> {
            //TODO 结束时间
            //时间选择器
            DatePickerDialog datePickerDialog = new DatePickerDialog(this, (view, year, month, dayOfMonth) -> {
                endTime = String.format("%d-%02d-%02d",year , (month + 1) , dayOfMonth);
                binding.endTimeTv.setText(endTime);
                //开始时间不能大于结束时间
                Date startDate = DateUtils.stringToDate(beginTime, DateUtils.DATE_TO_STRING_SHORT_PATTERN);
                Date endDate = DateUtils.stringToDate(endTime, DateUtils.DATE_TO_STRING_SHORT_PATTERN);
                if (startDate.getTime() > endDate.getTime()) {
                    HUD.Toast("开始时间不能大于结束时间");
                }else{
                    pageNum = 1;
                    loadData();
                }
            }, DateUtils.getYear(endTime), DateUtils.getMonth(endTime) - 1, DateUtils.getDay(endTime));
            datePickerDialog.show();
        });

        binding.refreshLayout.setOnRefreshListener(refreshLayout -> {
            //TODO 下拉刷新
            pageNum = 1;
            loadData();
        });
        binding.refreshLayout.setOnLoadMoreListener(refreshLayout -> {
            //TODO 上拉加载
            pageNum++;
            loadData();
        });

        //快速回到顶部按钮
        binding.backTopIv.setOnClickListener(v -> {
            binding.recyclerView.smoothScrollToPosition(0);
        });

        binding.backTopIv.setVisibility(View.GONE);
        //监听滑动事件，判断是否显示回到顶部按钮
        binding.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            private int totalDy = 0;
            @Override
            public void onScrolled(@NonNull RecyclerView aRecyclerView, int dx, int dy) {
                totalDy += dy;
                if (totalDy > 1000) {
                    binding.backTopIv.setVisibility(View.VISIBLE);
                } else {
                    binding.backTopIv.setVisibility(View.GONE);
                }
            }
        });
    }

    private void loadData() {
//        Long deptId = 0l;
//        UserInfoResponse userInfoResponse = UserCore.sharedCore().getUserInfo();
//        if (userInfoResponse != null) {
//            deptId = userInfoResponse.getUser().getDeptId();
//        }
        String t1 = beginTime + " 00:00:00";
        String t2 = endTime + " 23:59:59";
        //TODO 加载数据
        HttpAction.getOrderList(pageNum, 10, null,t1, t2,2, new Observer<PageResponse<TbOrder>>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull PageResponse<TbOrder> pageResponse) {
                binding.refreshLayout.finishRefresh();
                binding.refreshLayout.finishLoadMore();
                if (pageResponse.getCode()==200&&pageResponse.getRows().size()>0){
                    if (pageNum==1){
                        orderList.clear();
                    }
                    orderList.addAll(pageResponse.getRows());
                    //判断数据是否加载完，关闭上拉加载
                    if (orderList.size()>=pageResponse.getTotal()) {
                        binding.refreshLayout.setEnableLoadMore(false);
                        //显示已全部获取
                        binding.refreshLayout.setNoMoreData(true);
                    }else {
                        binding.refreshLayout.setEnableLoadMore(true);
                        binding.refreshLayout.setNoMoreData(false);
                    }
                    adapter.setTbOrders(orderList);
                }
            }

            @Override
            public void onError(@NonNull Throwable e) {
                binding.refreshLayout.finishRefresh();
                binding.refreshLayout.finishLoadMore();
            }

            @Override
            public void onComplete() {

            }
        });
    }



}