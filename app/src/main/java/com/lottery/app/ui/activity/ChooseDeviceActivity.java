package com.lottery.app.ui.activity;

import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.viewbinding.ViewBinding;

import com.lottery.app.R;
import com.lottery.app.adapter.ChooseDeviceAdapter;
import com.lottery.app.api.HttpAction;
import com.lottery.app.core.UserCore;
import com.lottery.app.databinding.ActivityChooseDeviceBinding;
import com.lottery.app.entity.PageResponse;
import com.lottery.app.entity.TbEquipmentPreset;
import com.lottery.app.utils.EventBusUtils;
import com.lottery.app.utils.HUD;
import com.lottery.app.utils.ViewUtils;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnLoadMoreListener;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;

public class ChooseDeviceActivity extends BaseActivity {

    private ActivityChooseDeviceBinding binding;

    private Integer pageNum = 1;
    private TbEquipmentPreset selectTbEquipmentPreset;
    private ChooseDeviceAdapter adapter;

    private List<TbEquipmentPreset> tbEquipmentPresets;

    @Override
    protected ViewBinding setViewBinding(Bundle savedInstanceState) {
        binding = ActivityChooseDeviceBinding.inflate(getLayoutInflater());
        return binding;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void setup() {
        setTitle("选择设备");
        buildTopView();
        pageNum = 1;
        loadData();

        tbEquipmentPresets = new ArrayList<>();
        adapter = new ChooseDeviceAdapter(this, tbEquipmentPresets);
        adapter.setOnItemClickListener(new ChooseDeviceAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(TbEquipmentPreset tbEquipmentPreset) {
                selectTbEquipmentPreset = tbEquipmentPreset;
            }
        });
        binding.recyclerView.setAdapter(adapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        binding.recyclerView.setLayoutManager(layoutManager);
        binding.refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@androidx.annotation.NonNull RefreshLayout refreshLayout) {
                pageNum = 1;
                loadData();
            }
        });

        binding.refreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@androidx.annotation.NonNull RefreshLayout refreshLayout) {
                pageNum++;
                loadData();
            }
        });


    }

    private void buildTopView() {
        TextView demoTV = new TextView(this);
        demoTV.setText("确定");
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewUtils.dp2px(this,60), ViewGroup.LayoutParams.MATCH_PARENT);
        demoTV.setLayoutParams(params);
        demoTV.setTextSize(16);
        demoTV.setTextColor(getResources().getColor(R.color.white));
        demoTV.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        demoTV.setGravity(Gravity.CENTER);
        addTopRightView(demoTV);
        demoTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (selectTbEquipmentPreset==null){
                    HUD.Toast("请选择设备");
                    return;
                }
                UserCore.sharedCore().setChooseDevice(selectTbEquipmentPreset);
                EventBusUtils.postChooseDevice();
                finish();
            }
        });
    }
    private void loadData(){
        HttpAction.getEquipmentPresetList(pageNum, 20, new Observer<PageResponse<TbEquipmentPreset>>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull PageResponse<TbEquipmentPreset> pageResponse) {
                binding.refreshLayout.finishRefresh();
                binding.refreshLayout.finishLoadMore();
                if (pageResponse.getCode()==200&&pageResponse.getRows().size()>0){
                    if (pageNum==1){
                        tbEquipmentPresets.clear();
                    }
                    tbEquipmentPresets.addAll(pageResponse.getRows());
                    //判断数据是否加载完，关闭上拉加载
                    if (tbEquipmentPresets.size()>=pageResponse.getTotal()) {
                        binding.refreshLayout.setEnableLoadMore(false);
                        //显示已全部获取
                        binding.refreshLayout.setNoMoreData(true);
                    }else {
                        binding.refreshLayout.setEnableLoadMore(true);
                        binding.refreshLayout.setNoMoreData(false);
                    }
                    adapter.setTbEquipmentPresetCargoLanes(tbEquipmentPresets);
                }
            }

            @Override
            public void onError(@NonNull Throwable e) {
                binding.refreshLayout.finishRefresh();
                binding.refreshLayout.finishLoadMore();
            }

            @Override
            public void onComplete() {
            }
        });
    }
}