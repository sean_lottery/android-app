package com.lottery.app.ui.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.lottery.app.R;


/**
 *  确认订单对话框
 *  选择支付方式
 *  输入手机号
 *  点击确认按钮
 *  传递支付方式和手机号给监听器
 */
public class ConfirmOrderDialogFragment extends DialogFragment {

    private Button confirmButton;

    private RadioGroup radioGroup;


    public interface ConfirmOrderDialogListener {
        void onConfirmOrderClick(DialogFragment dialog,int payType,String phone);
    }

    private int payType = 1;
    private ConfirmOrderDialogListener listener;

    public void setListener(ConfirmOrderDialogListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_confirm_order, null);

        radioGroup = view.findViewById(R.id.payment_radio_group);
        confirmButton = view.findViewById(R.id.confirm_button);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) { // 选择支付方式
                switch (i) {
                    case R.id.wechat_radio_button:
                        payType = 1;
                        break;
                    case R.id.alipay_radio_button:
                        payType = 3;
                        break;
                    default:
                        payType = 0;
                        break;
                }
            }
        });
        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO: Handle order confirmation
                String phone = "";
                if (listener != null) {
                    listener.onConfirmOrderClick(ConfirmOrderDialogFragment.this,payType,phone);
                }
            }
        });

        builder.setView(view);
        return builder.create();
    }
}