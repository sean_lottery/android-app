package com.lottery.app.ui.fragment.home;

import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewbinding.ViewBinding;

import com.lottery.app.R;
import com.lottery.app.adapter.HomeDeviceAdapter;
import com.lottery.app.api.HttpAction;
import com.lottery.app.core.UserCore;
import com.lottery.app.databinding.FragmentHomeBinding;
import com.lottery.app.entity.AddOrderBody;
import com.lottery.app.entity.AddOrderRes;
import com.lottery.app.entity.OrderGoods;
import com.lottery.app.entity.ResponseBody;
import com.lottery.app.entity.TbEquipmentPreset;
import com.lottery.app.entity.TbEquipmentPresetCargoLane;
import com.lottery.app.ui.activity.ChooseDeviceActivity;
import com.lottery.app.ui.dialog.ConfirmOrderDialogFragment;
import com.lottery.app.ui.dialog.CommitCodeDialogFragment;
import com.lottery.app.ui.fragment.base.BaseFragment;
import com.lottery.app.utils.DeviceUtils;
import com.lottery.app.utils.HUD;
import com.lottery.app.utils.ViewUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;

public class HomeFragment extends BaseFragment {

    private static final String TAG = "HomeFragment";

    private FragmentHomeBinding binding;

    private HomeViewModel homeViewModel;

    private HomeDeviceAdapter homeAdapter;

    private TbEquipmentPreset tbEquipmentPreset;
    private List<HomeDeviceAdapter.Data> tbEquipmentPresetCargoLanes;

    @Override
    protected ViewBinding setViewBinding(@NonNull LayoutInflater inflater, @Nullable ViewGroup container) {
        homeViewModel =
                new ViewModelProvider(this).get(HomeViewModel.class);
        binding = FragmentHomeBinding.inflate(inflater, container, false);
        return binding;
    }

    @Override
    public void initUI() {
        setTitle("首页");

        loadData();

        tbEquipmentPresetCargoLanes = new ArrayList<>();
        homeAdapter = new HomeDeviceAdapter(getActivity(),tbEquipmentPresetCargoLanes);
        homeAdapter.setOnItemNumberChangeListener(new HomeDeviceAdapter.OnItemNumberChangeListener() {
            @Override
            public void onItemNumberChange(int position, int number) {
                updateNumber();
            }
        });
        binding.recyclerView.setAdapter(homeAdapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        binding.recyclerView.setLayoutManager(layoutManager);
        binding.refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadData();
            }
        });
        binding.buyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addOrder();
            }
        });
        addTopItem();
    }

    @Override
    public void reload() {
        super.reload();
        loadData();
    }

    /**
     *
     */
    private void addTopItem(){
        ImageView lineImg = new ImageView(getActivity());
        lineImg.setImageDrawable(getContext().getDrawable(R.mipmap.icon_choose_line));
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewUtils.dp2px(getActivity(),40), ViewUtils.dp2px(getActivity(),40));
        lineImg.setLayoutParams(params);
        int padding = ViewUtils.dp2px(getActivity(),5);
        lineImg.setPadding(padding, padding, padding, padding);
        addRightItem(lineImg);
        lineImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), ChooseDeviceActivity.class));
            }
        });
    }



    private void loadData() {
        binding.emptyLayout.setVisibility(View.VISIBLE);
        binding.refreshLayout.setVisibility(View.GONE);
        TbEquipmentPreset equipmentPreset = UserCore.sharedCore().getChooseDevice();
        if (equipmentPreset==null){
            return;
        }
        binding.emptyLayout.setVisibility(View.GONE);
        binding.refreshLayout.setVisibility(View.VISIBLE);
        Long deviceId = equipmentPreset.getePId();
        HttpAction.getEquipmentPresetInfo(deviceId, new Observer<ResponseBody<TbEquipmentPreset>>() {
            @Override
            public void onSubscribe(@io.reactivex.rxjava3.annotations.NonNull Disposable d) {
                Log.d(TAG, "onSubscribe: ");
            }

            @Override
            public void onNext(@io.reactivex.rxjava3.annotations.NonNull ResponseBody<TbEquipmentPreset> responseBody) {
                Log.d(TAG, "onNext: ");
                if (responseBody.getData()!=null){
                    binding.deviceNumTv.setText(responseBody.getData().getePNum());
                    tbEquipmentPreset = responseBody.getData();
                    binding.statusTv.setBackground(DeviceUtils.getDeviceStatusDrawable(tbEquipmentPreset.getePState()));
                }
            }

            @Override
            public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                Log.d(TAG, "onError: ");
            }

            @Override
            public void onComplete() {
                Log.d(TAG, "onComplete: ");
            }
        });

        HttpAction.getEquipmentPresetCargoLaneList(deviceId, 1, new Observer<ResponseBody<List<TbEquipmentPresetCargoLane>>>() {
            @Override
            public void onSubscribe(@io.reactivex.rxjava3.annotations.NonNull Disposable d) {

            }

            @Override
            public void onNext(@io.reactivex.rxjava3.annotations.NonNull ResponseBody<List<TbEquipmentPresetCargoLane>> listResponseBody) {
                binding.refreshLayout.setRefreshing(false);
                if (listResponseBody.getData()!=null){
                    List<HomeDeviceAdapter.Data> list = new ArrayList<>();
                    for (TbEquipmentPresetCargoLane tbEquipmentPresetCargoLane : listResponseBody.getData()) {
                        HomeDeviceAdapter.Data data = new HomeDeviceAdapter.Data();
                        data.setTbEquipmentPresetCargoLane(tbEquipmentPresetCargoLane);
                        list.add(data);
                    }
                    tbEquipmentPresetCargoLanes = list;
                    homeAdapter.setTbEquipmentPresetCargoLanes(tbEquipmentPresetCargoLanes);
                }
            }

            @Override
            public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                binding.refreshLayout.setRefreshing(false);
            }

            @Override
            public void onComplete() {

            }
        });
    }

    private void addOrder(){
        ConfirmOrderDialogFragment dialog = new ConfirmOrderDialogFragment();
        dialog.setListener(new ConfirmOrderDialogFragment.ConfirmOrderDialogListener() {
            @Override
            public void onConfirmOrderClick(DialogFragment dialog, int payType, String phone) {
                dialog.dismiss();
                commitOrder(payType,phone);
            }
        });
        dialog.show(getActivity().getSupportFragmentManager(), "ConfirmOrderDialog");

    }

    private void commitOrder(int payType,String phone){
        //获取选中的货道
        List<HomeDeviceAdapter.Data> selectData = new ArrayList<>();
        for (HomeDeviceAdapter.Data data : tbEquipmentPresetCargoLanes) {
            if (data.getNumber()>0){
                selectData.add(data);
            }
        }
        //获取订单商品
        List<OrderGoods> orderGoods = new ArrayList<>();
        for (HomeDeviceAdapter.Data data : selectData) {
            OrderGoods orderGood = new OrderGoods();
            orderGood.setCargoId(data.getTbEquipmentPresetCargoLane().getId());
            orderGood.setCargoNo(data.getTbEquipmentPresetCargoLane().getePCNum());
            orderGood.setGoodId(data.getTbEquipmentPresetCargoLane().getGoodId());
            orderGood.setGoodImg(data.getTbEquipmentPresetCargoLane().getGoodImg());
            orderGood.setGoodName(data.getTbEquipmentPresetCargoLane().getGoodName());
            orderGood.setGoodNum(data.getNumber());
            orderGood.setGoodPrice(data.getTbEquipmentPresetCargoLane().getGoodPrice());
            orderGoods.add(orderGood);
        }
        //获取订单状态
        String orderStatus = "0";
        //获取支付状态
        String payStatus = "0";
        //获取设备编号
        String equipmentNum = tbEquipmentPreset.getePNum();

        //获取请求参数
        AddOrderBody addOrderBody = new AddOrderBody();
        addOrderBody.setEquipmentNum(equipmentNum);
        addOrderBody.setOrderGoods(orderGoods);
        addOrderBody.setOrderStatus(orderStatus);
        addOrderBody.setPayStatus(payStatus);
        addOrderBody.setPhone(phone);
        addOrderBody.setPayType(""+payType);

        HUD.show(getActivity(),"加载中...");
        HttpAction.addOrder(addOrderBody, new Observer<ResponseBody<AddOrderRes>>() {
            @Override
            public void onSubscribe(@io.reactivex.rxjava3.annotations.NonNull Disposable d) {

            }

            @Override
            public void onNext(@io.reactivex.rxjava3.annotations.NonNull ResponseBody<AddOrderRes> addOrderResResponseBody) {
                HUD.dismiss();
                if (addOrderResResponseBody.getData()!=null){
                    AddOrderRes addOrderRes = addOrderResResponseBody.getData();
                    //dialog展示url生成的二维码
                    showQRCode(addOrderBody,addOrderRes);
                }
            }

            @Override
            public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                HUD.dismiss();
            }

            @Override
            public void onComplete() {
                Log.e(TAG, "onComplete: ");
            }
        });
    }

    private void updateNumber(){
        BigDecimal number = BigDecimal.valueOf(0.0);
        for (HomeDeviceAdapter.Data data : tbEquipmentPresetCargoLanes) {
            BigDecimal money = data.getTbEquipmentPresetCargoLane().getGoodPrice().multiply(BigDecimal.valueOf(data.getNumber()));
            number = number.add(money);
        }
        binding.allMoneyTv.setText(number+"");
    }

    private void showQRCode(AddOrderBody addOrderBody,AddOrderRes addOrderRes){
        //dialog展示url生成的二维码
        //自定义dialog
        CommitCodeDialogFragment dialog = new CommitCodeDialogFragment(addOrderBody,addOrderRes);
        dialog.show(getActivity().getSupportFragmentManager(), "QRCodeDialog");

    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}