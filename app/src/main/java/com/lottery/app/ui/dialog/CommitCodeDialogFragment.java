package com.lottery.app.ui.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.lottery.app.R;
import com.lottery.app.entity.AddOrderBody;
import com.lottery.app.entity.AddOrderRes;
import com.lottery.app.entity.OrderGoods;
import com.lottery.app.utils.ScanUtils;

import java.util.List;
import java.util.Map;

/**
 * 提交订单对话框
 */
public class CommitCodeDialogFragment extends DialogFragment {
    public static interface OnTimeUpdateListener {
        void onTimeUpdate(int time);
    }
    private Bitmap qrCodeBitmap;
    private AddOrderBody addOrderBody;
    private AddOrderRes addOrderRes;

    private Button commitButton;
    private ImageView qrCodeImageView;

    private OnTimeUpdateListener onTimeUpdateListener;

    private Bitmap generateQRCode(String text) {
        return ScanUtils.createQR(text);
    }


    public void setOnTimeUpdateListener(OnTimeUpdateListener onTimeUpdateListener) {
        this.onTimeUpdateListener = onTimeUpdateListener;
    }

    public CommitCodeDialogFragment(AddOrderBody addOrderBody, AddOrderRes addOrderRes) {
        this.addOrderBody = addOrderBody;
        this.addOrderRes = addOrderRes;
        Map wxOrderData = (Map) addOrderRes.getWxOrderData();
        String url = wxOrderData.get("codeUrl").toString();
        this.qrCodeBitmap = generateQRCode(url);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_commit_order, null);
        qrCodeImageView = view.findViewById(R.id.qr_code_image_view);
        qrCodeImageView.setImageBitmap(qrCodeBitmap);
        commitButton = view.findViewById(R.id.commit_btn);

        TextView numbersTV = view.findViewById(R.id.numbers_tv);
        List<OrderGoods> orderGoods = addOrderBody.getOrderGoods();
        StringBuffer stringBuffer = new StringBuffer();
        String sp = orderGoods.size()>7?"  ":"\n";
        for (OrderGoods orderGood : orderGoods) {
            stringBuffer.append(orderGood.getGoodName() + "        x" + orderGood.getGoodNum() + sp);
        }
        numbersTV.setText(stringBuffer.toString());

        TextView totalPriceTV = view.findViewById(R.id.total_price_tv);
        totalPriceTV.setText("￥" + addOrderRes.getOrder().getTotalMoney());

        commitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        startCountDown();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view);
        AlertDialog dialog = builder.create();
        //禁止点击空白关闭dialog
        dialog.setCanceledOnTouchOutside(false);
        return dialog;
    }

    /**
     * 120s 倒计时
     */
    public void startCountDown() {

        commitButton.setText("取消订单(120s)");
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    int count = 120;
                    while (count > 0) {
                        Thread.sleep(1000);
                        count--;
                        int finalCount = count;
                        if (onTimeUpdateListener != null) {
                            onTimeUpdateListener.onTimeUpdate(finalCount);
                        }
                        commitButton.post(new Runnable() {
                            @Override
                            public void run() {
                                commitButton.setText("取消订单("+finalCount + "s)");
                            }
                        });
                    }
                    commitButton.post(new Runnable() {
                        @Override
                        public void run() {
                            commitButton.setVisibility(View.GONE);
                            dismiss();
                        }
                    });
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }


}