package com.lottery.app.ui.fragment.prize;

import android.Manifest;
import android.content.pm.PackageManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.viewbinding.ViewBinding;

import com.lottery.app.R;
import com.lottery.app.adapter.CashPrizeAdapter;
import com.lottery.app.api.HttpAction;
import com.lottery.app.databinding.FragmentCashPrizeBinding;
import com.lottery.app.entity.PageResponse;
import com.lottery.app.entity.TbWxUserRedeem;
import com.lottery.app.ui.fragment.base.BaseFragment;
import com.lottery.app.utils.ScanUtils;
import com.lottery.app.utils.ViewUtils;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;

/**
 * 兑奖
 */
public class CashPrizeFragment extends BaseFragment {

    private FragmentCashPrizeBinding binding;

    private CashPrizeViewModel cashPrizeViewModel;

    private CashPrizeAdapter adapter;

    private List<TbWxUserRedeem> tbWxUserRedeems;

    private int page = 1;

    @Override
    protected ViewBinding setViewBinding(@NonNull LayoutInflater inflater, @Nullable ViewGroup container) {
        cashPrizeViewModel =
                new ViewModelProvider(this).get(CashPrizeViewModel.class);
        binding = FragmentCashPrizeBinding.inflate(inflater, container, false);
        return binding;
    }

    @Override
    public void initUI() {
        super.initUI();
        setTitle("兑奖");
        addTopItem();
        tbWxUserRedeems = new ArrayList<>();
        adapter = new CashPrizeAdapter(getActivity(),tbWxUserRedeems);
        binding.recyclerView.setAdapter(adapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        binding.recyclerView.setLayoutManager(layoutManager);
        page=1;
        loadData();
        binding.refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                page=1;
                loadData();
            }
        });

        binding.refreshLayout.setOnLoadMoreListener(refreshLayout -> {
            page++;
            loadData();
        });
    }
    private void addTopItem(){
        ImageView lineImg = new ImageView(getActivity());
        lineImg.setImageDrawable(getContext().getDrawable(R.mipmap.ic_scan));
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewUtils.dp2px(getActivity(),40), ViewUtils.dp2px(getActivity(),40));
        lineImg.setLayoutParams(params);
        int padding = ViewUtils.dp2px(getActivity(),6);
        lineImg.setPadding(padding, padding, padding, padding);
        addRightItem(lineImg);
        lineImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pushScan();
            }
        });
    }
    private void loadData(){
        //TODO 加载数据
        HttpAction.getRedeemList(page, 10, 3, new Observer<PageResponse<TbWxUserRedeem>>() {
            @Override
            public void onSubscribe(@io.reactivex.rxjava3.annotations.NonNull Disposable d) {

            }

            @Override
            public void onNext(@io.reactivex.rxjava3.annotations.NonNull PageResponse<TbWxUserRedeem> pageResponse) {
                binding.refreshLayout.finishRefresh();
                binding.refreshLayout.finishLoadMore();
                if (pageResponse.getCode()==200&&pageResponse.getRows().size()>0){
                    if (page==1){
                        tbWxUserRedeems.clear();
                    }
                    tbWxUserRedeems.addAll(pageResponse.getRows());
                    //判断数据是否加载完，关闭上拉加载
                    if (tbWxUserRedeems.size()>=pageResponse.getTotal()) {
                        binding.refreshLayout.setEnableLoadMore(false);
                        //显示已全部获取
                        binding.refreshLayout.setNoMoreData(true);
                    }else {
                        binding.refreshLayout.setEnableLoadMore(true);
                        binding.refreshLayout.setNoMoreData(false);
                    }
                    adapter.setTbWxUserRedeems(tbWxUserRedeems);
                }
            }

            @Override
            public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                binding.refreshLayout.finishRefresh();
                binding.refreshLayout.finishLoadMore();
            }

            @Override
            public void onComplete() {

            }
        });
    }

    public static final int CHOOSE_CAMERA = 1998;
    private void pushScan(){
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.CAMERA) ) {
                ActivityCompat.requestPermissions(getActivity(),new String[]{Manifest.permission.CAMERA},
                        CHOOSE_CAMERA);
            }else{
                ActivityCompat.requestPermissions(getActivity(),new String[]{Manifest.permission.CAMERA},
                        CHOOSE_CAMERA);
            }
        }else{
            ScanUtils.pushScan(getActivity());
        }
    }
}
