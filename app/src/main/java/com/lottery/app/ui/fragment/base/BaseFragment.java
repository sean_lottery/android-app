package com.lottery.app.ui.fragment.base;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewbinding.ViewBinding;

import com.lottery.app.databinding.FragmentBaseBinding;

public abstract class BaseFragment extends Fragment {

    private FragmentBaseBinding baseBinding;


    protected abstract ViewBinding setViewBinding(@NonNull LayoutInflater inflater, @Nullable ViewGroup container);
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        baseBinding = FragmentBaseBinding.inflate(inflater, container, false);
        View root = baseBinding.getRoot();
        ViewBinding viewBinding = setViewBinding(inflater, container);
        if (viewBinding!=null){
            View subView = viewBinding.getRoot();
            ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            subView.setLayoutParams(params);
            baseBinding.centerLayout.addView(subView);
        }
        initUI();
        return root;
    }

    public void initUI(){

    }

    public void setTitle(String title){
        baseBinding.topTitleTv.setText(title);
    }

    public void addLeftItem(View view){
        baseBinding.topLeftLayout.addView(view);
    }

    public void addRightItem(View view){
        baseBinding.topRightLayout.addView(view);
    }

    public void reload(){

    }

}


