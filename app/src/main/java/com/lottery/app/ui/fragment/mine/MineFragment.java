package com.lottery.app.ui.fragment.mine;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.viewbinding.ViewBinding;

import com.lottery.app.R;
import com.lottery.app.core.UserCore;
import com.lottery.app.databinding.FragmentHomeBinding;
import com.lottery.app.databinding.FragmentMineBinding;
import com.lottery.app.entity.UserInfoResponse;
import com.lottery.app.ui.activity.LoginActivity;
import com.lottery.app.ui.activity.SettingActivity;
import com.lottery.app.ui.fragment.base.BaseFragment;
import com.lottery.app.ui.fragment.home.HomeViewModel;
import com.lottery.app.utils.ImageUtils;

public class MineFragment extends BaseFragment implements View.OnClickListener{

    private final String TAG = "MineFragment";

    private MineViewModel mineViewModel;

    private FragmentMineBinding binding;

    @Override
    protected ViewBinding setViewBinding(@NonNull LayoutInflater inflater, @Nullable ViewGroup container) {
        mineViewModel =
                new ViewModelProvider(this).get(MineViewModel.class);

        binding = FragmentMineBinding.inflate(inflater, container, false);
        return binding;
    }


    @Override
    public void initUI() {
        setTitle("我的");

        UserInfoResponse userInfoResponse = UserCore.sharedCore().getUserInfo();
        if (userInfoResponse!=null) {
            binding.userNameTv.setText(userInfoResponse.getUser().getNickName());
            ImageUtils.load(getContext(), userInfoResponse.getUser().getAvatar(), binding.userHeaderImg);
        }

        binding.logoutLayout.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.logout_layout:
                logout();
                break;
        }
    }

    private void logout() {
        //提示是否退出登录
        AlertDialog alertDialog = new AlertDialog.Builder(getContext())
                .setTitle("提示")
                .setMessage("是否要退出登录")
                .setPositiveButton("确定", (dialogInterface, i) -> {
                    UserCore.sharedCore().logout();
                    startActivity(new Intent(getActivity(), LoginActivity.class));
                    getActivity().finish();
                })
                .setNegativeButton("取消", (dialogInterface, i) -> {

                })
                .create();
        alertDialog.show();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}