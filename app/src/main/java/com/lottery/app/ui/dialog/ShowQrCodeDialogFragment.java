package com.lottery.app.ui.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.lottery.app.R;
import com.lottery.app.utils.ScanUtils;

import java.util.HashMap;

public class ShowQrCodeDialogFragment extends DialogFragment {

    private Bitmap qrCodeBitmap;


    private Bitmap generateQRCode(String text) {
        return ScanUtils.createQR(text);
    }


    public ShowQrCodeDialogFragment(String text) {
        this.qrCodeBitmap = generateQRCode(text);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_qr_code, null);
        ImageView qrCodeImageView = view.findViewById(R.id.qr_code_image_view);
        qrCodeImageView.setImageBitmap(qrCodeBitmap);


        view.findViewById(R.id.commit_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view);
        AlertDialog dialog = builder.create();
        //禁止点击空白关闭dialog
        dialog.setCanceledOnTouchOutside(false);
        return dialog;
    }
}