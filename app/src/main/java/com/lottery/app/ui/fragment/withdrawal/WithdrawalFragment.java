package com.lottery.app.ui.fragment.withdrawal;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.viewbinding.ViewBinding;

import com.lottery.app.R;
import com.lottery.app.adapter.CashPrizeAdapter;
import com.lottery.app.api.Api;
import com.lottery.app.api.HttpAction;
import com.lottery.app.core.App;
import com.lottery.app.core.UserCore;
import com.lottery.app.databinding.FragmentCashPrizeBinding;
import com.lottery.app.databinding.FragmentWithdrawalBinding;
import com.lottery.app.entity.PageResponse;
import com.lottery.app.entity.TbWxUserRedeem;
import com.lottery.app.ui.activity.ChooseDeviceActivity;
import com.lottery.app.ui.dialog.ShowQrCodeDialogFragment;
import com.lottery.app.ui.fragment.base.BaseFragment;
import com.lottery.app.ui.fragment.prize.CashPrizeViewModel;
import com.lottery.app.utils.ViewUtils;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;

public class WithdrawalFragment extends BaseFragment {

    private FragmentWithdrawalBinding binding;

    private WithdrawalViewModel withdrawalViewModel;

    private CashPrizeAdapter adapter;

    private List<TbWxUserRedeem> tbWxUserRedeems;

    private int page = 1;

    @Override
    protected ViewBinding setViewBinding(@NonNull LayoutInflater inflater, @Nullable ViewGroup container) {
        withdrawalViewModel =
                new ViewModelProvider(this).get(WithdrawalViewModel.class);
        binding = FragmentWithdrawalBinding.inflate(inflater, container, false);
        return binding;
    }

    @Override
    public void initUI() {
        super.initUI();
        setTitle("提现");

        //addTopItem();

        tbWxUserRedeems = new ArrayList<>();
        adapter = new CashPrizeAdapter(getActivity(),tbWxUserRedeems);
        binding.recyclerView.setAdapter(adapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        binding.recyclerView.setLayoutManager(layoutManager);
        page=1;
        loadData();
        binding.refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                page=1;
                loadData();
            }
        });

        binding.refreshLayout.setOnLoadMoreListener(refreshLayout -> {
            page++;
            loadData();
        });
    }

    /**
     *
     */
    private void addTopItem(){
        ImageView lineImg = new ImageView(getActivity());
        lineImg.setImageDrawable(getContext().getDrawable(R.mipmap.ic_tx));
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewUtils.dp2px(getActivity(),40), ViewUtils.dp2px(getActivity(),40));
        lineImg.setLayoutParams(params);
        int padding = ViewUtils.dp2px(getActivity(),5);
        lineImg.setPadding(padding, padding, padding, padding);
        addRightItem(lineImg);
        lineImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //redirect配的是回调地址，这个地址是你自己的服务器地址，微信会把code传给这个地址，然后你自己的服务器再去请求微信的接口，获取用户的openid等信息;
                String redirect = Api.WX_REDIRECT_URL;
                String STATE = UserCore.sharedCore().getDeviceSn();
                //微信授权页面
                String url = "https://open.weixin.qq.com/connect/oauth2/authorize?appid="+Api.WX_APP_ID+"&redirect_uri="+redirect+"&response_type=code&scope=snsapi_userinfo&state="+STATE+"#wechat_redirect";
                //String url = "https://open.weixin.qg.com/connect/oauth2/authorize?appid="+Api.WX_APP_ID+"&redirect_uri="+redirect;

                ShowQrCodeDialogFragment dialogFragment = new ShowQrCodeDialogFragment(url);
                dialogFragment.show(getChildFragmentManager(),"ShowQrCodeDialogFragment");
            }
        });
    }

    private void loadData(){
        //TODO 加载数据
        HttpAction.getRedeemList(page, 10, 2, new Observer<PageResponse<TbWxUserRedeem>>() {
            @Override
            public void onSubscribe(@io.reactivex.rxjava3.annotations.NonNull Disposable d) {

            }

            @Override
            public void onNext(@io.reactivex.rxjava3.annotations.NonNull PageResponse<TbWxUserRedeem> pageResponse) {
                binding.refreshLayout.finishRefresh();
                binding.refreshLayout.finishLoadMore();
                if (pageResponse.getCode()==200&&pageResponse.getRows().size()>0){
                    if (page==1){
                        tbWxUserRedeems.clear();
                    }
                    tbWxUserRedeems.addAll(pageResponse.getRows());
                    //判断数据是否加载完，关闭上拉加载
                    if (tbWxUserRedeems.size()>=pageResponse.getTotal()) {
                        binding.refreshLayout.setEnableLoadMore(false);
                        //显示已全部获取
                        binding.refreshLayout.setNoMoreData(true);
                    }else {
                        binding.refreshLayout.setEnableLoadMore(true);
                        binding.refreshLayout.setNoMoreData(false);
                    }
                    adapter.setTbWxUserRedeems(tbWxUserRedeems);
                }
            }

            @Override
            public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                binding.refreshLayout.finishRefresh();
                binding.refreshLayout.finishLoadMore();
            }

            @Override
            public void onComplete() {

            }
        });
    }

}
