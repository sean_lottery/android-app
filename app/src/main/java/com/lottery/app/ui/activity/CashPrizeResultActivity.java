package com.lottery.app.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.viewbinding.ViewBinding;

import com.alibaba.fastjson.JSON;
import com.lottery.app.R;
import com.lottery.app.adapter.CashPrizeAdapter;
import com.lottery.app.adapter.CashPrizeResultAdapter;
import com.lottery.app.api.Api;
import com.lottery.app.api.HttpAction;
import com.lottery.app.core.DataCore;
import com.lottery.app.core.UserCore;
import com.lottery.app.databinding.ActivityCashPrizeResultBinding;
import com.lottery.app.entity.EndPayParms;
import com.lottery.app.entity.EventMessage;
import com.lottery.app.entity.RedeemBody;
import com.lottery.app.entity.ResponseBody;
import com.lottery.app.entity.TbWxUser;
import com.lottery.app.entity.TbWxUserRedeem;
import com.lottery.app.ui.dialog.ShowQrCodeDialogFragment;
import com.lottery.app.utils.EventBusUtils;
import com.lottery.app.utils.EventLogUtils;
import com.lottery.app.utils.HUD;
import com.lottery.app.utils.ScanUtils;
import com.lottery.app.utils.ViewUtils;
import com.yxing.ScanCodeConfig;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;


/**
 * 兑奖结果
 */
public class CashPrizeResultActivity extends BaseActivity implements View.OnClickListener{

    private static final String TAG = "CashPrizeResultActivity";

    private ActivityCashPrizeResultBinding binding;

    private List<TbWxUserRedeem> tbWxUserRedeems;

    private CashPrizeResultAdapter adapter;

    private ShowQrCodeDialogFragment showQrCodeDialogFragment;
    @Override
    protected ViewBinding setViewBinding(Bundle savedInstanceState) {
        binding = ActivityCashPrizeResultBinding.inflate(getLayoutInflater());
        return binding;
    }
    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart: ");
        if (!EventBus.getDefault().isRegistered(this)) {//加上判断
            EventBus.getDefault().register(this);
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        handlerRedeem();
        reload();
    }

    /**
     * 页面暂停
     */
    @Override
    protected void onPause() {
        super.onPause();
    }



    @Override
    protected void onDestroy() {
        if (EventBus.getDefault().isRegistered(this)) {//加上判断
            EventBus.getDefault().unregister(this);
        }
        super.onDestroy();
    }

    @Override
    protected void setup() {
        super.setup();
        setTitle("兑奖结果");
        addTopItem();

        EventBus.getDefault().register(this);

        tbWxUserRedeems = new ArrayList<>();

        adapter = new CashPrizeResultAdapter(this,tbWxUserRedeems);
        binding.recyclerView.setAdapter(adapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        binding.recyclerView.setLayoutManager(layoutManager);
        calculateTotalMoney();

        binding.exchangeBtn.setOnClickListener(this);
        binding.withdrawBtn.setOnClickListener(this);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && data != null) {
            switch (requestCode) {
                case ScanCodeConfig.QUESTCODE:
                    //接收扫码结果
                    Bundle extras = data.getExtras();
                    if (extras != null) {
                        int codeType = extras.getInt(ScanCodeConfig.CODE_TYPE);
                        String code = extras.getString(ScanCodeConfig.CODE_KEY);
                        Log.e(TAG, "onActivityResult: "+String.format(
                                "扫码结果：\n" +
                                        "码类型: %s  \n" +
                                        "码值  : %s", codeType == 0 ? "一维码" : "二维码", code));
                        if (code==null){
                            HUD.Toast("二维码为空");
                            return;
                        }
                        if (!ScanUtils.checkQRCode(code)){
                            HUD.Toast("二维码不正确");
                            return;
                        }
                        DataCore.sharedCore().setScanCode(code);
                    }
                    break;
                default:
                    break;
            }
        }
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMessage(EventMessage message) {
        Log.w(TAG, "onEventMessage: "+JSON.toJSONString(message));
        switch (message.getEvent()){
            case EventMessage.EVENT_ADD_DEVICE_WX_USER_REDEEMS:
                handleUserRedeems((TbWxUserRedeem)message.getData());
                break;
            case EventMessage.EVENT_MQTT_STATUS:
                updateMQTTUI((Integer)message.getData());
                break;
            case EventMessage.EVENT_GET_WX_USER:
                withdrawEvent(message);
                break;
            case EventMessage.EVENT_RELOAD_REDEEMS:
                List<TbWxUserRedeem> redeems = (List<TbWxUserRedeem>) message.getData();
                tbWxUserRedeems = redeems;
                break;
        }
    }

    private void handleUserRedeems(TbWxUserRedeem tbWxUserRedeem){
        addRedeems(tbWxUserRedeem);
        reload();
    }

    private void reload(){
        adapter.setTbWxUserRedeems(tbWxUserRedeems);
        calculateTotalMoney();
    }
    private void addTopItem(){

        ImageView lineImg = new ImageView(this);
        lineImg.setImageDrawable(getDrawable(R.mipmap.ic_scan));
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewUtils.dp2px(this,40), ViewUtils.dp2px(this,40));
        lineImg.setLayoutParams(params);
        int padding = ViewUtils.dp2px(this,6);
        lineImg.setPadding(padding, padding, padding, padding);
        addTopRightView(lineImg);

        lineImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ScanUtils.pushScan(CashPrizeResultActivity.this);
            }
        });
    }

    private void addRedeems(TbWxUserRedeem tbWxUserRedeem){
        if (tbWxUserRedeems == null){
            tbWxUserRedeems = new ArrayList<>();
        }
        if (tbWxUserRedeem == null){
            HUD.Toast("数据不能为空");
            return;
        }
        //判断重复
        for (TbWxUserRedeem item:tbWxUserRedeems){
            if (item.getId().longValue() == tbWxUserRedeem.getId().longValue()){
                HUD.Toast("重复添加");
                return;
            }
        }
        tbWxUserRedeems.add(tbWxUserRedeem);
    }
    /**
     * 处理兑换
     */
    private void handlerRedeem(){
        String code = DataCore.sharedCore().getScanCode();
        if (code==null || code.isEmpty()){
            return;
        }
        RedeemBody redeemBody = new RedeemBody();
        redeemBody.setDeviceSn(UserCore.sharedCore().getDeviceSn());
        redeemBody.setSafeAreaCode(code);

        HUD.show(this);
        HttpAction.doRedeem2(redeemBody, new Observer<ResponseBody>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull ResponseBody responseBody) {
                HUD.dismiss();
                DataCore.sharedCore().setScanCode(null);
                if (responseBody.getCode()==200){
                    if (responseBody.getData() != null && !(responseBody.getData() instanceof String)){
                        try {
                            TbWxUserRedeem tbWxUserRedeem = JSON.parseObject(JSON.toJSONString(responseBody.getData()),TbWxUserRedeem.class);
                            if (tbWxUserRedeem!=null){
                                addRedeems(tbWxUserRedeem);
                                reload();
                            }else{
                                HUD.Toast("兑奖中,请稍后...");
                            }
                        }catch (Exception e) {
                            Log.e(TAG, "onNext: "+e.getMessage());
                            Map<String,Object> map = new HashMap<>();
                            map.put("error",e.getMessage());
                            map.put("res",responseBody);
                            EventLogUtils.addEventLog("redeem_event_json_error",map);
                            HUD.Toast("兑奖中,请稍后...");
                        }
                    }else{
                        //HUD.Toast("彩票兑奖中,请稍后...");
                        HUD.show(CashPrizeResultActivity.this,"彩票兑奖中,请稍后...");
                        //倒计时3秒关闭
                        binding.getRoot().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                HUD.dismiss();
                            }
                        },3000);
                    }
                }else{
                    HUD.Toast(responseBody.getMsg());
                    EventLogUtils.addEventLog("redeem_event_error",JSON.toJSONString(responseBody));
                }
            }

            @Override
            public void onError(@NonNull Throwable e) {
                HUD.dismiss();
                DataCore.sharedCore().setScanCode(null);
                EventLogUtils.addEventLog("redeem_event_error","Throwable:"+e.getMessage());
                HUD.Toast("兑奖提交失败！");
            }

            @Override
            public void onComplete() {

            }
        });
    }

    /**
     * 计算总金额
     */
    private BigDecimal calculateTotalMoney() {
        BigDecimal totalMoney = BigDecimal.ZERO;
        for (TbWxUserRedeem tbWxUserRedeem : tbWxUserRedeems) {
            if (tbWxUserRedeem.getType().equals("3")){
                BigDecimal win = BigDecimal.valueOf(Double.valueOf(tbWxUserRedeem.getWinprize()));
                BigDecimal rate = BigDecimal.valueOf(tbWxUserRedeem.getMoney()==null?0.0:tbWxUserRedeem.getMoney());
                totalMoney = totalMoney.add(win.subtract(rate));
            }
        }
        binding.allMoneyTv.setText("总奖金：￥" + totalMoney);
        return totalMoney;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.exchange_btn:
                exchange();
                break;
            case R.id.withdraw_btn:
                withdraw();
                break;
        }
    }

    private void exchange(){
        BigDecimal totalMoney = calculateTotalMoney();
        if (totalMoney.doubleValue()<=0){
            HUD.Toast("暂无可兑换金额");
            return;
        }
        DataCore.sharedCore().setDeviceWxUserRedeems(tbWxUserRedeems);
        DataCore.sharedCore().setTotalAmount(totalMoney);
        startActivity(new Intent(this,ExchangeActivity.class));
    }

    private void withdraw(){
        BigDecimal totalMoney = calculateTotalMoney();
        if (totalMoney.doubleValue()<=0){
            HUD.Toast("暂无可提现金额");
            return;
        }
        //redirect配的是回调地址，这个地址是你自己的服务器地址，微信会把code传给这个地址，然后你自己的服务器再去请求微信的接口，获取用户的openid等信息;
        String redirect = Api.WX_REDIRECT_URL;
        String STATE = UserCore.sharedCore().getDeviceSn();
        //微信授权页面
        String url = "https://open.weixin.qq.com/connect/oauth2/authorize?appid="+Api.WX_APP_ID+"&redirect_uri="+redirect+"&response_type=code&scope=snsapi_userinfo&state="+STATE+"#wechat_redirect";
        //String url = "https://open.weixin.qg.com/connect/oauth2/authorize?appid="+Api.WX_APP_ID+"&redirect_uri="+redirect;

        showQrCodeDialogFragment = new ShowQrCodeDialogFragment(url);
        showQrCodeDialogFragment.show(getSupportFragmentManager(),"ShowQrCodeDialogFragment");
    }

    private int totalCount = 0;
    private int addCount = 0;
    //是否提现中[防止重复提交提现]
    private boolean isWithdrawing = false;
    /**
     * 提现确认
     * @param eventMessage
     */
    private void withdrawEvent(EventMessage eventMessage){
        try {
            if (isWithdrawing){
                HUD.Toast("提现中,请稍后...");
                return;
            }
            TbWxUser tbWxUser = (TbWxUser) eventMessage.getData();
            if (tbWxUser==null){
                HUD.Toast("获取用户信息失败");
                return;
            }

            totalCount = tbWxUserRedeems.size();
            addCount = 0;
            List<TbWxUserRedeem> newRedeems = new ArrayList<>();

            Observer<ResponseBody<TbWxUserRedeem>> observer = new Observer<ResponseBody<TbWxUserRedeem>>() {
                @Override
                public void onSubscribe(@NonNull Disposable d) {

                }

                @Override
                public void onNext(@NonNull ResponseBody<TbWxUserRedeem> res) {
                    addCount++;
                    if (res.getCode()==200 && res.getData()!=null){
                        newRedeems.add(res.getData());
                    }else{
                        Map<String,Object> map = new HashMap<>();
                        map.put("res",res);
                        map.put("addCount",addCount);
                        EventLogUtils.addEventLog("getRedeem_err",map);
                    }
                    if (addCount==totalCount){
                        HUD.dismiss();
                        tbWxUserRedeems = newRedeems;
                        reload();
                        withdrawCommit(tbWxUser);
                    }
                }

                @Override
                public void onError(@NonNull Throwable e) {
                    addCount++;
                    Map<String,Object> map = new HashMap<>();
                    map.put("err",e.getMessage());
                    map.put("addCount",addCount);
                    EventLogUtils.addEventLog("getRedeem_err",map);
                    if (addCount==totalCount){
                        isWithdrawing = false;
                        HUD.dismiss();
                        HUD.Toast("加载兑奖彩票信息失败");
                    }
                }

                @Override
                public void onComplete() {

                }
            };
            HUD.show(this);

            isWithdrawing = true;
            for (TbWxUserRedeem tbWxUserRedeem : tbWxUserRedeems) {
                HttpAction.getRedeemInfo(tbWxUserRedeem.getId(), observer);
            }


        }catch (Exception e){
            HUD.Toast(e.getMessage());
            EventLogUtils.addEventLog("withdraw_exc_error",e.getMessage());
        }
    }

    private void withdrawCommit(TbWxUser tbWxUser){
        String openId = tbWxUser.getOpenid();
        List<Long> ids = new ArrayList<>();

        BigDecimal totalMoney = BigDecimal.ZERO;
        for (TbWxUserRedeem tbWxUserRedeem : tbWxUserRedeems) {
            if (tbWxUserRedeem.getType().equals("3")){
                BigDecimal win = BigDecimal.valueOf(Double.valueOf(tbWxUserRedeem.getWinprize()));
                BigDecimal rate = BigDecimal.valueOf(tbWxUserRedeem.getMoney()==null?0.0:tbWxUserRedeem.getMoney());
                totalMoney = totalMoney.add(win.subtract(rate));
                ids.add(tbWxUserRedeem.getId());
            }
        }

        EndPayParms endPayParms = new EndPayParms();
        endPayParms.setOpenid(openId);
        endPayParms.setAmount(totalMoney.intValue());
        endPayParms.setIds(ids);

        HUD.show(this);
        isWithdrawing = true;
        HttpAction.entPay3(endPayParms, new Observer<ResponseBody>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull ResponseBody responseBody) {
                HUD.dismiss();
                if (showQrCodeDialogFragment!=null){
                    showQrCodeDialogFragment.dismiss();
                    showQrCodeDialogFragment = null;
                }
                if (responseBody.getCode()==200) {
                    HUD.Toast("提现成功");
                    tbWxUserRedeems.clear();
                    reload();
                }else{
                    HUD.Toast(responseBody.getMsg());
                    Map<String,Object> map = new HashMap<>();
                    map.put("req",endPayParms);
                    map.put("res",responseBody);
                    EventLogUtils.addEventLog("withdraw_error",map);
                }
                isWithdrawing = false;
            }

            @Override
            public void onError(@NonNull Throwable e) {
                HUD.dismiss();
                if (showQrCodeDialogFragment!=null){
                    showQrCodeDialogFragment.dismiss();
                    showQrCodeDialogFragment = null;
                }
                HUD.Toast("提现失败："+e.getMessage());
                EventLogUtils.addEventLog("withdraw_error",e.getMessage());
                isWithdrawing = false;
            }

            @Override
            public void onComplete() {

            }
        });
    }
}