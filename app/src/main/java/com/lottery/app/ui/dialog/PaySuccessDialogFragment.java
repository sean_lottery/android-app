package com.lottery.app.ui.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.lottery.app.R;
import com.lottery.app.mqtt.GoodVo;
import com.lottery.app.mqtt.PaySuccessVo;

public class PaySuccessDialogFragment extends DialogFragment {

    private PaySuccessVo paySuccessVo;

    private Button confirmButton;

    private Thread thread;

    public PaySuccessDialogFragment(PaySuccessVo paySuccessVo) {
        this.paySuccessVo = paySuccessVo;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_pay_success, null);

        TextView ticketInfoTV = view.findViewById(R.id.ticket_info_tv);
        ticketInfoTV.setText("");
        if (paySuccessVo != null) {
            if (paySuccessVo.goods!=null) {
                StringBuffer sb = new StringBuffer();
                for (int i = 0; i < paySuccessVo.goods.size(); i++) {
                    GoodVo goodVo = paySuccessVo.goods.get(i);
                    sb.append("* ");
                    sb.append(goodVo.getGoodName());
                    sb.append("   x");
                    sb.append(goodVo.getGoodNum());
                    sb.append("\n");
                }
                ticketInfoTV.setText(sb.toString());
            }
        }

        confirmButton = view.findViewById(R.id.commit_btn);
        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (thread != null) {
                    thread.interrupt();
                }
                dismiss();
            }
        });

        startCountDown();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view);
        AlertDialog dialog = builder.create();
        //禁止点击空白关闭dialog
        dialog.setCanceledOnTouchOutside(false);

        return dialog;
    }

    private Integer countDown = 10;

    /**
     * 倒计时60秒关闭dialog
     */
    private void startCountDown() {

        confirmButton.setText("关闭(10s)");
        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    int count = countDown;
                    while (count > 0) {
                        Thread.sleep(1000);
                        count--;
                        int finalCount = count;
                        confirmButton.post(new Runnable() {
                            @Override
                            public void run() {
                                confirmButton.setText( "关闭(" + finalCount + "s)");
                            }
                        });
                    }
                    dismiss();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }

}
