package com.lottery.app.ui.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewbinding.ViewBinding;

import com.lottery.app.R;
import com.lottery.app.adapter.HomeDeviceAdapter;
import com.lottery.app.api.HttpAction;
import com.lottery.app.core.DataCore;
import com.lottery.app.core.UserCore;
import com.lottery.app.databinding.ActivityHomeBinding;
import com.lottery.app.databinding.ActivityMainTabBinding;
import com.lottery.app.entity.AddOrderBody;
import com.lottery.app.entity.AddOrderRes;
import com.lottery.app.entity.EventMessage;
import com.lottery.app.entity.OrderGoods;
import com.lottery.app.entity.ResponseBody;
import com.lottery.app.entity.TbEquipmentPreset;
import com.lottery.app.entity.TbEquipmentPresetCargoLane;
import com.lottery.app.entity.TbOrder;
import com.lottery.app.entity.UserInfoResponse;
import com.lottery.app.mqtt.MQTTClient;
import com.lottery.app.mqtt.MqttEvent;
import com.lottery.app.mqtt.MqttService;
import com.lottery.app.mqtt.PaySuccessVo;
import com.lottery.app.ui.dialog.CommitCodeDialogFragment;
import com.lottery.app.ui.dialog.ConfirmOrderDialogFragment;
import com.lottery.app.ui.dialog.PaySuccessDialogFragment;
import com.lottery.app.utils.DeviceUtils;
import com.lottery.app.utils.EventLogUtils;
import com.lottery.app.utils.HUD;
import com.lottery.app.utils.ImageUtils;
import com.lottery.app.utils.ScanUtils;
import com.yxing.ScanCodeConfig;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;

public class HomeActivity extends AppCompatActivity {
    private static final String TAG = "HomeActivity";

    private ActivityHomeBinding binding;
    private HomeDeviceAdapter homeAdapter;
    private TbEquipmentPreset tbEquipmentPreset;
    private List<HomeDeviceAdapter.Data> tbEquipmentPresetCargoLanes;

    private CommitCodeDialogFragment commitCodeDialogFragment;
    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart: ");
        if (!EventBus.getDefault().isRegistered(this)) {//加上判断
            EventBus.getDefault().register(this);
        }
        updateMQTTStatus(MQTTClient.getInstance().isOnline()?1:0);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityHomeBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        setup();
    }

    private void setup() {
        loadUserInfo();
        loadData();
        tbEquipmentPresetCargoLanes = new ArrayList<>();
        homeAdapter = new HomeDeviceAdapter(this,tbEquipmentPresetCargoLanes);
        homeAdapter.setOnItemNumberChangeListener(new HomeDeviceAdapter.OnItemNumberChangeListener() {
            @Override
            public void onItemNumberChange(int position, int number) {
                Log.d(TAG, "onItemNumberChange: "+position+" "+number);
                updateNumber();
            }
        });
        binding.recyclerView.setAdapter(homeAdapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        binding.recyclerView.setLayoutManager(layoutManager);
        binding.refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadData();
            }
        });
        binding.buyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addOrder();
            }
        });
        addTopItem();
        binding.orderTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, OrderListActivity.class));
            }
        });
        binding.scanBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pushScan();
            }
        });

        Intent serviceIntent = new Intent(this, MqttService.class);
        startService(serviceIntent);
        //判断是否忽略电池优化
        if (!isIgnoringBatteryOptimizations()){
            //请求忽略电池优化
            requestIgnoreBatteryOptimizations();
        }
        EventLogUtils.addEventLog("POWER_SERVICE",isIgnoringBatteryOptimizations()?"启动":"未启动");
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private boolean isIgnoringBatteryOptimizations() {
        boolean isIgnoring = false;
        PowerManager powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
        if (powerManager != null) {
            isIgnoring = powerManager.isIgnoringBatteryOptimizations(getPackageName());
        }
        return isIgnoring;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void requestIgnoreBatteryOptimizations() {
        try {
            Intent intent = new Intent(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
            intent.setData(Uri.parse("package:" + getPackageName()));
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     *
     */
    private void addTopItem(){

        binding.chooseDevice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, ChooseDeviceActivity.class));
            }
        });

        binding.logoutTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logout();
            }
        });


    }

    private void reload(){
        loadData();
    }

    private void updateMQTTStatus(int status){
        binding.userHeaderLayout.setBackground(getResources().getDrawable(status==1?R.drawable.device_status_online:R.drawable.device_status_offline));
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void eventMessage(EventMessage message) {
        switch (message.getEvent()){
            case EventMessage.EVENT_MQTT_STATUS:
                int status = (Integer) message.getData();
                if (status==1 && !MQTTClient.getInstance().isOnline()) {
                    HUD.Toast("MQTT连接成功");
                }
                if (status==0 && MQTTClient.getInstance().isOnline()){
                    HUD.Toast("MQTT连接失败");
                }
                updateMQTTStatus(status);
                break;
            case EventMessage.EVENT_CHOOSE_DEVICE:
                reload();
                break;
            case EventMessage.EVENT_LOGOUT:
                UserCore.sharedCore().logout();
                startActivity(new Intent(this, LoginActivity.class));
                finish();
                break;
            case EventMessage.EVENT_PAY_SUCCESS:
                if (commitCodeDialogFragment!=null){
                    commitCodeDialogFragment.dismiss();
                }
                showPaySuccessDialog((PaySuccessVo) message.getData());
                break;
        }
    }

    private void showPaySuccessDialog(PaySuccessVo paySuccessVo){
        PaySuccessDialogFragment dialogFragment = new PaySuccessDialogFragment(paySuccessVo);
        dialogFragment.show(getSupportFragmentManager(),"pay_success");
    }

    private void logout() {
        //提示是否退出登录
        AlertDialog alertDialog = new AlertDialog.Builder(this)
                .setTitle("提示")
                .setMessage("是否要退出登录")
                .setPositiveButton("确定", (dialogInterface, i) -> {
                    UserCore.sharedCore().logout();
                    startActivity(new Intent(this, LoginActivity.class));
                    finish();
                })
                .setNegativeButton("取消", (dialogInterface, i) -> {

                })
                .create();
        alertDialog.show();
    }

    /**
     * 加载用户信息
     */
    private void loadUserInfo(){
        HttpAction.getInfo(new Observer<UserInfoResponse>() {
            @Override
            public void onSubscribe(@io.reactivex.rxjava3.annotations.NonNull Disposable d) {

            }

            @Override
            public void onNext(@io.reactivex.rxjava3.annotations.NonNull UserInfoResponse userInfoResponse) {
                if (userInfoResponse.getCode() == 200) {
                    UserCore.sharedCore().setUserInfo(userInfoResponse);

                    if (userInfoResponse!=null) {

                        ImageUtils.load(HomeActivity.this, userInfoResponse.getUser().getAvatar(), binding.userHeaderImg);
                    }

                }else {
                    UserCore.sharedCore().setUserInfo(null);
                    startActivity(new Intent(HomeActivity.this, LoginActivity.class));
                    finish();
                }
            }

            @Override
            public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                Log.e(TAG, "onError: ", e);
            }

            @Override
            public void onComplete() {
                Log.w(TAG, "onComplete: ");
            }
        });
    }

    private void loadData() {
        binding.emptyLayout.setVisibility(View.VISIBLE);
        binding.refreshLayout.setVisibility(View.GONE);
        TbEquipmentPreset equipmentPreset = UserCore.sharedCore().getChooseDevice();
        if (equipmentPreset==null){
            return;
        }
        binding.emptyLayout.setVisibility(View.GONE);
        binding.refreshLayout.setVisibility(View.VISIBLE);

        binding.recyclerView.setItemViewCacheSize(0);
        binding.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                // 如果滑动了足够的距离，关闭软键盘
                if (shouldCloseKeyboard(recyclerView)) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (imm != null) {
                        //判断键盘是否弹出
                        if (getCurrentFocus()!=null){
                            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                        }

                    }
                }
            }
            private boolean shouldCloseKeyboard(RecyclerView recyclerView) {
                // 根据需要判断是否滑动了足够的距离
                int firstVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
                if (firstVisibleItemPosition != RecyclerView.NO_POSITION) {
                    // 这里假设滑动超过一个Item的高度就关闭键盘
                    View firstVisibleItem = recyclerView.getLayoutManager().findViewByPosition(firstVisibleItemPosition);
                    return firstVisibleItem.getTop() - recyclerView.getPaddingTop() < firstVisibleItem.getHeight();
                }
                return false;
            }
        });

        Long deviceId = equipmentPreset.getePId();
        HttpAction.getEquipmentPresetInfo(deviceId, new Observer<ResponseBody<TbEquipmentPreset>>() {
            @Override
            public void onSubscribe(@io.reactivex.rxjava3.annotations.NonNull Disposable d) {
                Log.d(TAG, "onSubscribe: ");
            }

            @Override
            public void onNext(@io.reactivex.rxjava3.annotations.NonNull ResponseBody<TbEquipmentPreset> responseBody) {
                Log.d(TAG, "onNext: ");
                if (responseBody.getData()!=null){
                    binding.deviceNumTv.setText(responseBody.getData().getePNum());
                    tbEquipmentPreset = responseBody.getData();
                    binding.statusTv.setBackground(DeviceUtils.getDeviceStatusDrawable(tbEquipmentPreset.getePState()));
                }
            }

            @Override
            public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                Log.d(TAG, "onError: ");
            }

            @Override
            public void onComplete() {
                Log.d(TAG, "onComplete: ");
            }
        });

        HttpAction.getEquipmentPresetCargoLaneList(deviceId, 1, new Observer<ResponseBody<List<TbEquipmentPresetCargoLane>>>() {
            @Override
            public void onSubscribe(@io.reactivex.rxjava3.annotations.NonNull Disposable d) {

            }

            @Override
            public void onNext(@io.reactivex.rxjava3.annotations.NonNull ResponseBody<List<TbEquipmentPresetCargoLane>> listResponseBody) {
                binding.refreshLayout.setRefreshing(false);
                if (listResponseBody.getData()!=null){
                    //更新数据
                    List<HomeDeviceAdapter.Data> list = new ArrayList<>();
                    for (TbEquipmentPresetCargoLane tbEquipmentPresetCargoLane : listResponseBody.getData()) {
                        HomeDeviceAdapter.Data data = new HomeDeviceAdapter.Data();
                        data.setTbEquipmentPresetCargoLane(tbEquipmentPresetCargoLane);
                        list.add(data);
                    }
                    tbEquipmentPresetCargoLanes = list;
                    homeAdapter.setTbEquipmentPresetCargoLanes(tbEquipmentPresetCargoLanes);
                    //更新数量
                    updateNumber();
                }
            }

            @Override
            public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                binding.refreshLayout.setRefreshing(false);
            }

            @Override
            public void onComplete() {

            }
        });
    }

    private void addOrder(){
        List<HomeDeviceAdapter.Data> selectData = new ArrayList<>();
        for (HomeDeviceAdapter.Data data : tbEquipmentPresetCargoLanes) {
            if (data.getNumber()>0){
                selectData.add(data);
            }
        }
        if (selectData.size()<=0){
            HUD.Toast("请选择商品");
            return;
        }

        ConfirmOrderDialogFragment dialog = new ConfirmOrderDialogFragment();
        dialog.setListener(new ConfirmOrderDialogFragment.ConfirmOrderDialogListener() {
            @Override
            public void onConfirmOrderClick(DialogFragment dialog, int payType, String phone) {
                dialog.dismiss();
                commitOrder(payType,phone);
            }
        });
        dialog.show(this.getSupportFragmentManager(), "ConfirmOrderDialog");

    }

    private void commitOrder(int payType,String phone){

        //获取选中的货道
        List<HomeDeviceAdapter.Data> selectData = new ArrayList<>();
        for (HomeDeviceAdapter.Data data : tbEquipmentPresetCargoLanes) {
            if (data.getNumber()>0){
                selectData.add(data);
            }
        }
        if (selectData.size()<=0){
            HUD.Toast("请选择商品");
            return;
        }
        //获取订单商品
        List<OrderGoods> orderGoods = new ArrayList<>();
        for (HomeDeviceAdapter.Data data : selectData) {
            OrderGoods orderGood = new OrderGoods();
            orderGood.setCargoId(data.getTbEquipmentPresetCargoLane().getId());
            orderGood.setCargoNo(data.getTbEquipmentPresetCargoLane().getePCNum());
            orderGood.setGoodId(data.getTbEquipmentPresetCargoLane().getGoodId());
            orderGood.setGoodImg(data.getTbEquipmentPresetCargoLane().getGoodImg());
            orderGood.setGoodName(data.getTbEquipmentPresetCargoLane().getGoodName());
            orderGood.setGoodNum(data.getNumber());
            orderGood.setGoodPrice(data.getTbEquipmentPresetCargoLane().getGoodPrice());
            orderGoods.add(orderGood);
        }
        //获取订单状态
        String orderStatus = "0";
        //获取支付状态
        String payStatus = "0";
        //获取设备编号
        String equipmentNum = tbEquipmentPreset.getePNum();

        //获取请求参数
        AddOrderBody addOrderBody = new AddOrderBody();
        addOrderBody.setEquipmentNum(equipmentNum);
        addOrderBody.setOrderGoods(orderGoods);
        addOrderBody.setOrderStatus(orderStatus);
        addOrderBody.setPayStatus(payStatus);
        addOrderBody.setPhone(phone);
        addOrderBody.setPayType(""+payType);
        addOrderBody.setAppDeviceSn(UserCore.sharedCore().getDeviceSn());

        HUD.show(this,"加载中...");
        HttpAction.addOrder(addOrderBody, new Observer<ResponseBody<AddOrderRes>>() {
            @Override
            public void onSubscribe(@io.reactivex.rxjava3.annotations.NonNull Disposable d) {

            }

            @Override
            public void onNext(@io.reactivex.rxjava3.annotations.NonNull ResponseBody<AddOrderRes> addOrderResResponseBody) {
                HUD.dismiss();
                reload();
                if (addOrderResResponseBody.getData()!=null){
                    AddOrderRes addOrderRes = addOrderResResponseBody.getData();
                    //dialog展示url生成的二维码
                    showQRCode(addOrderBody,addOrderRes);
                }
            }

            @Override
            public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                HUD.dismiss();
            }

            @Override
            public void onComplete() {
                Log.e(TAG, "onComplete: ");
            }
        });
    }

    private void updateNumber(){
        BigDecimal number = BigDecimal.valueOf(0.0);
        for (HomeDeviceAdapter.Data data : tbEquipmentPresetCargoLanes) {
            BigDecimal money = data.getTbEquipmentPresetCargoLane().getGoodPrice().multiply(BigDecimal.valueOf(data.getNumber()));
            number = number.add(money);
        }
        binding.allMoneyTv.setText(number+"");
    }

    private void showQRCode(AddOrderBody addOrderBody,AddOrderRes addOrderRes){
        //dialog展示url生成的二维码
        //自定义dialog
        commitCodeDialogFragment = new CommitCodeDialogFragment(addOrderBody,addOrderRes);
        commitCodeDialogFragment.show(this.getSupportFragmentManager(), "QRCodeDialog");
        //延时5秒查询订单状态
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(5000);
                    queryOrder(addOrderRes.getOrder().getOrderNo());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
    private void queryOrder(String orderNo) {
        HttpAction.getOrder(orderNo, new Observer<ResponseBody<PaySuccessVo>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(ResponseBody<PaySuccessVo> paySuccessVoResponseBody) {
                if (paySuccessVoResponseBody.getData()!=null){
                    PaySuccessVo paySuccessVo = paySuccessVoResponseBody.getData();
                    if (paySuccessVo.getPayState()==2){
                        //支付成功
                        if (commitCodeDialogFragment!=null){
                            commitCodeDialogFragment.dismiss();
                        }
                        showPaySuccessDialog(paySuccessVo);
                        return;
                    }
                }
                //3秒后继续查询
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(3000);
                            queryOrder(orderNo);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
            }

            @Override
            public void onError(Throwable e) {
                Log.e(TAG, "onError: ",e );
                //3秒后继续查询
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(3000);
                            queryOrder(orderNo);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
            }

            @Override
            public void onComplete() {

            }
        });
    }


    public static final int CHOOSE_CAMERA = 1998;
    private void pushScan(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.CAMERA) ) {
                ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.CAMERA},
                        CHOOSE_CAMERA);
            }else{
                ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.CAMERA},
                        CHOOSE_CAMERA);
            }
        }else{
            ScanUtils.pushScan(this);
//            QrScanActivity.push(this,0,1);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && data != null) {
            switch (requestCode) {
                case ScanCodeConfig.QUESTCODE:
                    //接收扫码结果
                    Bundle extras = data.getExtras();
                    if (extras != null) {
                        int codeType = extras.getInt(ScanCodeConfig.CODE_TYPE);
                        String code = extras.getString(ScanCodeConfig.CODE_KEY);
                        Log.e(TAG, "onActivityResult: "+String.format(
                                "扫码结果：\n" +
                                        "码类型: %s  \n" +
                                        "码值  : %s", codeType == 0 ? "一维码" : "二维码", code));
                        if (code==null){
                            HUD.Toast("二维码为空");
                            return;
                        }
                        if (!ScanUtils.checkQRCode(code)){
                            HUD.Toast("二维码不正确");
                            return;
                        }
                        DataCore.sharedCore().setScanCode(code);
                        startActivity(new Intent(HomeActivity.this, CashPrizeResultActivity.class));
                    }
                    break;
                default:
                    break;
            }
        }
    }

}