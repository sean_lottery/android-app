package com.lottery.app.ui.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.lottery.app.R;
import com.lottery.app.core.UserCore;

/**
 * 启动页
 * 1.延时2秒跳转到mainActivity
 */
public class LaunchActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);
        //延时2秒跳转到mainActivity
        new Thread(() -> {
            try {
                Thread.sleep(2000);
                toMainActivity();
                finish();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
    }

    /**
     * 跳转到mainActivity
     */
    private void toMainActivity() {
        //startActivity(new Intent(this, MainTabActivity.class));
        if (UserCore.isLogin()) {
            startActivity(new Intent(this, HomeActivity.class));
        } else {
            startActivity(new Intent(this, LoginActivity.class));
        }
    }
}