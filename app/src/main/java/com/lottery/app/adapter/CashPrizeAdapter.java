package com.lottery.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.lottery.app.R;
import com.lottery.app.entity.TbWxUserRedeem;
import com.lottery.app.utils.ImageUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * 现金奖励 Adapter
 */
public class CashPrizeAdapter extends RecyclerView.Adapter<CashPrizeAdapter.ItemViewHolder> {


    public class ItemViewHolder extends RecyclerView.ViewHolder{

        private TbWxUserRedeem tbWxUserRedeem;
        @BindView(R.id.user_header_img)
        ImageView userHeaderImg;

        @BindView(R.id.name_tv)
        TextView nameTV;

        @BindView(R.id.money_tv)
        TextView moneyTV;

        //订单号
        @BindView(R.id.order_num_tv)
        TextView orderNumTV;

        @BindView(R.id.time_tv)
        TextView timeTV;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

        public void setTbWxUserRedeem(TbWxUserRedeem tbWxUserRedeem) {
            this.tbWxUserRedeem = tbWxUserRedeem;
            updateUI();
        }

        private void updateUI(){
            if (tbWxUserRedeem != null){
                ImageUtils.load(itemView.getContext(), tbWxUserRedeem.getHeadImgUrl(),userHeaderImg);
                nameTV.setText("昵称："+tbWxUserRedeem.getWxNickName());
                moneyTV.setText("￥"+tbWxUserRedeem.getMoney());
                orderNumTV.setText("订单号："+tbWxUserRedeem.getOrderId());
                timeTV.setText("时间："+tbWxUserRedeem.getCreateTime());
            }
        }
    }



    private Context context;
    private List<TbWxUserRedeem> tbWxUserRedeems;



    public CashPrizeAdapter(Context context, List<TbWxUserRedeem> tbWxUserRedeems){
        this.context = context;
        this.tbWxUserRedeems = tbWxUserRedeems;
    }

    public void setTbWxUserRedeems(List<TbWxUserRedeem> tbWxUserRedeems) {
        this.tbWxUserRedeems = tbWxUserRedeems;
        //更新列表
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cash_prize, parent, false);
        ItemViewHolder viewHolder = new ItemViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {

        TbWxUserRedeem tbWxUserRedeem = tbWxUserRedeems.get(position);
        holder.setTbWxUserRedeem(tbWxUserRedeem);

    }

    @Override
    public int getItemCount() {
        if (tbWxUserRedeems == null) {
            return 0;
        }
        return tbWxUserRedeems.size();
    }
}
