package com.lottery.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.lottery.app.R;
import com.lottery.app.core.UserCore;
import com.lottery.app.entity.TbEquipmentPreset;
import com.lottery.app.entity.TbEquipmentPresetCargoLane;
import com.lottery.app.utils.DeviceUtils;
import com.lottery.app.utils.ImageUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * 选择设备
 */
public class ChooseDeviceAdapter extends RecyclerView.Adapter<ChooseDeviceAdapter.ChooseDeviceViewHolder> {

    public interface OnItemClickListener {
        void onItemClick(TbEquipmentPreset tbEquipmentPreset);
    }

    public class ChooseDeviceViewHolder extends RecyclerView.ViewHolder{


        @BindView(R.id.name_tv)
        TextView nameTV;

        @BindView(R.id.device_num_tv)
        TextView deviceNumTV;

        @BindView(R.id.type_tv)
        TextView typeTV;

        @BindView(R.id.dept_tv)
        TextView deptTV;

        @BindView(R.id.check_iv)
        ImageView checkIV;

        @BindView(R.id.status_tv)
        TextView statusTV;

        public ChooseDeviceViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

        private void setCheck(boolean isCheck){
            if (isCheck){
                checkIV.setImageResource(R.mipmap.ic_check);
            }else {
                checkIV.setImageResource(R.mipmap.ic_un_check);
            }
        }

    }



    private Context context;
    private List<TbEquipmentPreset> tbEquipmentPresets;

    private TbEquipmentPreset selectTbEquipmentPreset;

    private OnItemClickListener onItemClickListener;

    private ChooseDeviceAdapter.ChooseDeviceViewHolder chooseDeviceViewHolder;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public ChooseDeviceAdapter(Context context, List<TbEquipmentPreset> tbEquipmentPresets){
        this.context = context;
        this.tbEquipmentPresets = tbEquipmentPresets;

        selectTbEquipmentPreset = UserCore.sharedCore().getChooseDevice();
    }

    public void setTbEquipmentPresetCargoLanes(List<TbEquipmentPreset> tbEquipmentPresets) {
        this.tbEquipmentPresets = tbEquipmentPresets;
        //更新列表
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public ChooseDeviceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_choose_device, parent, false);
        ChooseDeviceViewHolder ChooseDeviceViewHolder = new ChooseDeviceViewHolder(view);
        return ChooseDeviceViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ChooseDeviceViewHolder holder, int position) {

        TbEquipmentPreset tbEquipmentPreset = tbEquipmentPresets.get(position);
        holder.nameTV.setText(tbEquipmentPreset.getePName());
        holder.deviceNumTV.setText("设备号："+tbEquipmentPreset.getePNum());
        holder.typeTV.setText("类型:"+tbEquipmentPreset.getTypeName());
        holder.deptTV.setText("部门："+tbEquipmentPreset.getDeptName());
        holder.statusTV.setBackground(DeviceUtils.getDeviceStatusDrawable(tbEquipmentPreset.getePState()));
        holder.setCheck(false);
        if (selectTbEquipmentPreset!=null&&selectTbEquipmentPreset.getePId().equals(tbEquipmentPreset.getePId())){
            chooseDeviceViewHolder = holder;
            holder.setCheck(true);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectTbEquipmentPreset = tbEquipmentPreset;
                if (chooseDeviceViewHolder!=null){
                    chooseDeviceViewHolder.setCheck(false);
                }
                chooseDeviceViewHolder = holder;
                holder.setCheck(true);
                if (onItemClickListener!=null){
                    onItemClickListener.onItemClick(selectTbEquipmentPreset);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        if (tbEquipmentPresets == null) {
            return 0;
        }
        return tbEquipmentPresets.size();
    }
}
