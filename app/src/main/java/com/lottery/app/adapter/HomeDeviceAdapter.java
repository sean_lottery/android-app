package com.lottery.app.adapter;

import android.app.Activity;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.lottery.app.R;
import com.lottery.app.entity.TbEquipmentPresetCargoLane;
import com.lottery.app.ui.view.NumberEditView;
import com.lottery.app.utils.HUD;
import com.lottery.app.utils.ImageUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * 首页设备
 */
public class HomeDeviceAdapter extends RecyclerView.Adapter<HomeDeviceAdapter.HomeDeviceViewHolder>  {

    private static final String TAG = "HomeDeviceAdapter";
    public interface OnItemNumberChangeListener{
        void onItemNumberChange(int position,int number);
    }

    public class HomeDeviceViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private OnItemNumberChangeListener onItemNumberChangeListener;

        public void setOnItemNumberChangeListener(OnItemNumberChangeListener onItemNumberChangeListener) {
            this.onItemNumberChangeListener = onItemNumberChangeListener;
        }

        @BindView(R.id.device_image_view)
        ImageView deviceImageView;

        @BindView(R.id.name_tv)
        TextView nameTextView;

        @BindView(R.id.price_tv)
        TextView priceTextView;

        @BindView(R.id.num_tv)
        NumberEditView numberTextView;

        @BindView(R.id.reduce_tv)
        TextView reduceTextView;

        @BindView(R.id.plus_tv)
        TextView plusTextView;

        @BindView(R.id.stock_tv)
        TextView stockTV;

        private int position;
        private Data data;

        private TextWatcher textWatcher;

        public HomeDeviceViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

        public void setPosition(int position) {
            this.position = position;
        }
        public void setData(Data data) {
            this.data = data;
            reduceTextView.setOnClickListener(this);
            plusTextView.setOnClickListener(this);

            //先移除监听
            if (numberTextView.getTag() instanceof TextWatcher) {
                numberTextView.removeTextChangedListener((TextWatcher) numberTextView.getTag());
                numberTextView.clearFocus();
            }

            final HomeDeviceViewHolder holder = this;
            textWatcher = new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    //numberTextView.setSelection(numberTextView.getText().length());
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    Log.d(TAG, "onTextChanged: "+s+" >>"+position+" >>"+holder);
                }

                @Override
                public void afterTextChanged(Editable s) {
                    Log.d(TAG, "afterTextChanged: "+s+" >>"+position+" >>"+holder);
                    String text = s.toString();
                    if(text.length() > 1 && text.substring(0,1).equals("0")) {
                        numberTextView.setText(text.substring(1));
                    }
                    if (s.length() == 0){
                        data.setNumber(0);
                        numberTextView.setText("0");
                        if (onItemNumberChangeListener != null){
                            onItemNumberChangeListener.onItemNumberChange(position,data.getNumber());
                        }
                        return;
                    }
                    Integer num = Integer.valueOf(s.toString());
                    if (num > data.getTbEquipmentPresetCargoLane().getInStock()){
                        //HUD.Toast("库存不足");
                        data.setNumber(data.getTbEquipmentPresetCargoLane().getInStock());
                        numberTextView.setText(data.getNumber()+"");
                        if (onItemNumberChangeListener != null){
                            onItemNumberChangeListener.onItemNumberChange(position,data.getNumber());
                        }
                        return;
                    }
                    data.setNumber(num);
                    numberTextView.setSelection(numberTextView.getText().length());
                    if (onItemNumberChangeListener != null){
                        onItemNumberChangeListener.onItemNumberChange(position,data.getNumber());
                    }

                }
            };
            numberTextView.addTextChangedListener(textWatcher);
            numberTextView.setTag(textWatcher);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()){
                case R.id.reduce_tv:
                    if (data.getNumber() > 0){
                        data.setNumber(data.getNumber() - 1);
                        numberTextView.setText(data.getNumber()+"");
                        if (onItemNumberChangeListener != null){
                            onItemNumberChangeListener.onItemNumberChange(getAdapterPosition(),data.getNumber());
                        }
                    }
                    break;
                case R.id.plus_tv:
                    if (data.getTbEquipmentPresetCargoLane().getInStock()<data.getNumber()+1){
                        HUD.Toast("库存不足");
                        return;
                    }
                    data.setNumber(data.getNumber() + 1);
                    numberTextView.setText(data.getNumber()+"");
                    if (onItemNumberChangeListener != null){
                        onItemNumberChangeListener.onItemNumberChange(getAdapterPosition(),data.getNumber());
                    }
                    break;
            }
        }
    }

    public static class Data {

        private int number = 0;

        private TbEquipmentPresetCargoLane tbEquipmentPresetCargoLane;

        public Data(){
        }

        public int getNumber() {
            return number;
        }

        public void setNumber(int number) {
            this.number = number;
        }

        public TbEquipmentPresetCargoLane getTbEquipmentPresetCargoLane() {
            return tbEquipmentPresetCargoLane;
        }

        public void setTbEquipmentPresetCargoLane(TbEquipmentPresetCargoLane tbEquipmentPresetCargoLane) {
            this.tbEquipmentPresetCargoLane = tbEquipmentPresetCargoLane;
        }
    }
    private Activity context;
    private List<Data> tbEquipmentPresetCargoLanes;
    private OnItemNumberChangeListener onItemNumberChangeListener;

    public HomeDeviceAdapter(Activity context,List<Data> tbEquipmentPresetCargoLanes){
        this.context = context;
        this.tbEquipmentPresetCargoLanes = tbEquipmentPresetCargoLanes;
    }

    public void setOnItemNumberChangeListener(OnItemNumberChangeListener onItemNumberChangeListener) {
        this.onItemNumberChangeListener = onItemNumberChangeListener;
    }



    public void setTbEquipmentPresetCargoLanes(List<Data> tbEquipmentPresetCargoLanes) {
        this.tbEquipmentPresetCargoLanes = tbEquipmentPresetCargoLanes;
        //更新列表
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public HomeDeviceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_device, parent, false);
        HomeDeviceViewHolder homeDeviceViewHolder = new HomeDeviceViewHolder(view);
        return homeDeviceViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull HomeDeviceViewHolder holder, int position) {
        Log.d(TAG, "onBindViewHolder: "+position+" >>:"+holder);
        Data data = tbEquipmentPresetCargoLanes.get(position);
        TbEquipmentPresetCargoLane tbEquipmentPresetCargoLane = data.getTbEquipmentPresetCargoLane();
        if (tbEquipmentPresetCargoLane != null){
            ImageUtils.load(context,tbEquipmentPresetCargoLane.getGoodImg(),holder.deviceImageView);
            holder.nameTextView.setText(tbEquipmentPresetCargoLane.getGoodName());
            holder.priceTextView.setText("￥"+tbEquipmentPresetCargoLane.getGoodPrice());
            holder.numberTextView.setText(data.getNumber()+"");
            holder.stockTV.setText(tbEquipmentPresetCargoLane.getInStock()+"");
            holder.setPosition(position);
            holder.setData(data);
        }
        holder.setIsRecyclable(false);//防止editText数据错乱

        holder.setOnItemNumberChangeListener(new OnItemNumberChangeListener() {
            @Override
            public void onItemNumberChange(int position, int number) {
                if (onItemNumberChangeListener != null){
                    onItemNumberChangeListener.onItemNumberChange(position,number);
                }
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    if (context.getCurrentFocus()!=null) {
                        imm.hideSoftInputFromWindow(context.getCurrentFocus().getWindowToken(), 0);
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        if (tbEquipmentPresetCargoLanes == null) {
            return 0;
        }
        return tbEquipmentPresetCargoLanes.size();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }
}
