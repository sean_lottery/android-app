package com.lottery.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.lottery.app.R;
import com.lottery.app.entity.TbWxUserRedeem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CashPrizeResultAdapter extends RecyclerView.Adapter<CashPrizeResultAdapter.ItemViewHolder> {


    public class ItemViewHolder extends RecyclerView.ViewHolder{

        private TbWxUserRedeem tbWxUserRedeem;


        @BindView(R.id.name_tv)
        TextView nameTV;

        @BindView(R.id.winprize_tv)
        TextView winprizeTV;

        @BindView(R.id.money_tv)
        TextView moneyTV;

        //订单号
        @BindView(R.id.result_tv)
        TextView resultTV;

        @BindView(R.id.time_tv)
        TextView timeTV;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

        public void setTbWxUserRedeem(TbWxUserRedeem tbWxUserRedeem) {
            this.tbWxUserRedeem = tbWxUserRedeem;
            updateUI();
        }

        private void updateUI(){
            if (tbWxUserRedeem != null){
                nameTV.setText("兑奖码：" + tbWxUserRedeem.getSn());
                timeTV.setText("兑奖时间：" + tbWxUserRedeem.getCreateTime());
                resultTV.setText("兑奖结果：" + getTypeValue(tbWxUserRedeem.getType()));

                winprizeTV.setText("中奖:￥"+tbWxUserRedeem.getWinprize());
                moneyTV.setText("提现:￥"+(tbWxUserRedeem.getMoney()==null?"0":tbWxUserRedeem.getMoney()));
            }
        }

        private String getTypeValue(String type){
            String value = "";
            switch (type){
                case "1":
                    value = "未中奖";
                    break;
                case "2":
                    value = "提现";
                    break;
                case "3":
                    value = "中奖";
                    break;
                default:
                    value = "未知";
                    break;
            }
            return value;
        }
    }



    private Context context;
    private List<TbWxUserRedeem> tbWxUserRedeems;



    public CashPrizeResultAdapter(Context context, List<TbWxUserRedeem> tbWxUserRedeems){
        this.context = context;
        this.tbWxUserRedeems = tbWxUserRedeems;
    }

    public void setTbWxUserRedeems(List<TbWxUserRedeem> tbWxUserRedeems) {
        this.tbWxUserRedeems = tbWxUserRedeems;
        //更新列表
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cash_prize_result, parent, false);
        ItemViewHolder viewHolder = new ItemViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {

        TbWxUserRedeem tbWxUserRedeem = tbWxUserRedeems.get(position);
        holder.setTbWxUserRedeem(tbWxUserRedeem);

    }

    @Override
    public int getItemCount() {
        if (tbWxUserRedeems == null) {
            return 0;
        }
        return tbWxUserRedeems.size();
    }
}
