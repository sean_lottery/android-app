package com.lottery.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.lottery.app.R;
import com.lottery.app.entity.TbOrder;
import com.lottery.app.entity.TbOrderGood;
import com.lottery.app.entity.TbWxUserRedeem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderListAdapter extends RecyclerView.Adapter<OrderListAdapter.ItemViewHolder> {

    public class ItemViewHolder extends RecyclerView.ViewHolder{
        private TbOrder tbOrder;

        @BindView(R.id.name_tv)
        TextView nameTV;
        @BindView(R.id.money_tv)
        TextView moneyTV;
        @BindView(R.id.status_tv)
        TextView statusTV;
        //订单号
        @BindView(R.id.result_tv)
        TextView resultTV;
        @BindView(R.id.time_tv)
        TextView timeTV;
        @BindView(R.id.goods_tv)
        TextView goodsTV;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

        public void setTbOrder(TbOrder tbOrder) {
            this.tbOrder = tbOrder;
            updateUI();
        }

        private void updateUI(){
            if (tbOrder != null){

                nameTV.setText("订单号："+tbOrder.getOrderNo());
                moneyTV.setText("￥"+tbOrder.getTotalMoney());
                timeTV.setText("创建时间："+tbOrder.getCreateTime());
                resultTV.setText("支付方式："+getPayType(tbOrder.getPayType()));
                statusTV.setText(getPayStatus(tbOrder.getPayStatus()));
                goodsTV.setText(tbOrder.getRemark());
            }
        }

        private String getOrderStatus(String status){
            if (status.equals("0")){
                return "交易中";
            }else if (status.equals("1")){
                return "交易完成";
            }else {
                return "未知";
            }
        }
        private String getPayType(String payType){
            if (payType.equals("1")){
                return "微信支付";
            }else if (payType.equals("2")){
                return "余额支付";
            }else if (payType.equals("3")){
                return "支付宝支付";
            }else {
                return "未知";
            }
        }

        private String getPayStatus(String payStatus){
            if (payStatus.equals("0")){
                return "待支付";
            }else if (payStatus.equals("2")){
                return "支付完成";
            }else if (payStatus.equals("3")){
                return "未支付";
            }else {
                return "未知";
            }
        }
    }



    private Context context;
    private List<TbOrder> tbOrders;



    public OrderListAdapter(Context context, List<TbOrder> tbOrders){
        this.context = context;
        this.tbOrders = tbOrders;
    }

    public void setTbOrders(List<TbOrder> tbOrders) {
        this.tbOrders = tbOrders;
        //更新列表
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_order_list, parent, false);
        ItemViewHolder viewHolder = new ItemViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {

        TbOrder tbOrder = tbOrders.get(position);
        holder.setTbOrder(tbOrder);

    }

    @Override
    public int getItemCount() {
        if (tbOrders == null) {
            return 0;
        }
        return tbOrders.size();
    }
}
