package com.lottery.app.core;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;

import com.umeng.analytics.MobclickAgent;
import com.umeng.commonsdk.UMConfigure;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

/**
 * app 入口
 */
public class App extends Application {
    public static int screenWidth;

    public static int screenHeight;

    private static App application;

    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context=getApplicationContext();
        application = this;
        initScreenWidth();
        registerActivityLifecycleCallbacks(new AppLifecycleCallbacks());

        //友盟统计
        initUM();

    }
    public static Activity getCurrentActivity () {
        try {
            Class activityThreadClass = Class.forName("android.app.ActivityThread");
            Object activityThread = activityThreadClass.getMethod("currentActivityThread").invoke(
                    null);
            Field activitiesField = activityThreadClass.getDeclaredField("mActivities");
            activitiesField.setAccessible(true);
            Map activities = (Map) activitiesField.get(activityThread);
            for (Object activityRecord : activities.values()) {
                Class activityRecordClass = activityRecord.getClass();
                Field pausedField = activityRecordClass.getDeclaredField("paused");
                pausedField.setAccessible(true);
                if (!pausedField.getBoolean(activityRecord)) {
                    Field activityField = activityRecordClass.getDeclaredField("activity");
                    activityField.setAccessible(true);
                    Activity activity = (Activity) activityField.get(activityRecord);
                    return activity;
                }
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     * 获取屏幕的参数，宽度和高度
     */
    private void initScreenWidth() {
        DisplayMetrics metrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager)
                this.getSystemService(Context.WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(metrics);
        screenHeight = metrics.heightPixels;
        screenWidth = metrics.widthPixels;
    }
    public static Application sharedApplication() {
        return application;
    }

    public static Context getContext() {
        return context;
    }

    /**
     *  友盟appkey
     */
    private static final String umAppKey = "65919b34a7208a5af1935bbb";

    /**
     * 初始化友盟统计
     */
    private void initUM(){
        //获取渠道号
        String channel = "DEV";

        try {
            UMConfigure.submitPolicyGrantResult(context,true);

            UMConfigure.setLogEnabled(true);

            UMConfigure.preInit(this,umAppKey, channel);
            // 选用AUTO页面采集模式
            MobclickAgent.setPageCollectionMode(MobclickAgent.PageMode.AUTO);
            // 支持在子进程中统计自定义事件
            UMConfigure.setProcessEvent(true);
            // 初始化SDK
            UMConfigure.init(context, umAppKey, channel, UMConfigure.DEVICE_TYPE_PHONE, null);

            // 选用AUTO页面采集模式
            MobclickAgent.setPageCollectionMode(MobclickAgent.PageMode.AUTO);
            // 支持在子进程中统计自定义事件
            UMConfigure.setProcessEvent(true);

        }catch (Exception e){
            Log.e("App", "InitUMSDK: "+e.getMessage());
        }
    }
}
