package com.lottery.app.core;

import com.alibaba.fastjson.JSON;
import com.lottery.app.entity.TbEquipmentPreset;
import com.lottery.app.entity.UserInfoResponse;
import com.lottery.app.utils.MD5;
import com.lottery.app.utils.SPHelper;

import java.util.UUID;

public class UserCore {


    /**
     * 单列模式
     */
    private static volatile UserCore instance = null;

    private String deviceSn;

    public static UserCore sharedCore() {
        if (instance == null) {
            synchronized (UserCore.class) {
                if (instance == null) {
                    instance = new UserCore();
                }
            }
        }
        return instance;
    }

    private UserCore() {

    }
    public void logout() {
        setToken(null);
        setUserInfo(null);
        setChooseDevice(null);

    }
    public static boolean isLogin() {
        return UserCore.sharedCore().getToken() != null && !UserCore.sharedCore().getToken().isEmpty();
    }
    public void setToken(String token) {
        if (token==null){
            SPHelper.getInstance().remove("token");
            return;
        }
        SPHelper.getInstance().put("token", token);
    }

    public String getToken() {
        return SPHelper.getInstance().getString("token");
    }

    public void setUserInfo(UserInfoResponse userInfo) {
        if (userInfo==null) {
            SPHelper.getInstance().remove("userInfo");
            return;
        }
        SPHelper.getInstance().put("userInfo", JSON.toJSONString(userInfo));
    }

    public UserInfoResponse getUserInfo() {
        String jsonStr = SPHelper.getInstance().getString("userInfo");
        if (jsonStr==null|| jsonStr.isEmpty()) {
            return null;
        }
        return JSON.parseObject(jsonStr, UserInfoResponse.class);
    }

    /**
     * 保存用户名
     */
    public void setUserName(String userName) {
        SPHelper.getInstance().put("userName", userName);
    }
    /**
     * 获取用户名
     */
    public String getUserName() {
        return SPHelper.getInstance().getString("userName");
    }

    /**
     * 保存密码
     * @param password
     */
    public void setPassword(String password) {
        SPHelper.getInstance().put("password", password);
    }

    /**
     * 获取密码
     * @return
     */
    public String getPassword() {
        return SPHelper.getInstance().getString("password");
    }

    /**
     * 保存选中的设备
     */
    public void setChooseDevice(TbEquipmentPreset tbEquipmentPreset) {
        if (tbEquipmentPreset==null) {
            SPHelper.getInstance().remove("chooseDevice");
            return;
        }
        SPHelper.getInstance().put("chooseDevice", JSON.toJSONString(tbEquipmentPreset));
    }

    /**
     * 获取选中的设备
     */
    public TbEquipmentPreset getChooseDevice() {
        String jsonStr = SPHelper.getInstance().getString("chooseDevice");
        if (jsonStr == null || jsonStr.isEmpty()) {
            return null;
        }
        return JSON.parseObject(jsonStr, TbEquipmentPreset.class);
    }



    public String getDeviceSn() {
        if (deviceSn == null || deviceSn.isEmpty()) {
            deviceSn = SPHelper.getInstance().getString("deviceSn");
            if (deviceSn == null || deviceSn.isEmpty()){
                deviceSn = "client_app_"+ MD5.encode(UUID.randomUUID().toString());
                SPHelper.getInstance().put("deviceSn", deviceSn);
            }
        }
        return deviceSn;
    }

    public void setDeviceSn(String deviceSn) {
        this.deviceSn = deviceSn;
        if (deviceSn==null){
            SPHelper.getInstance().remove("deviceSn");
            return;
        }
        SPHelper.getInstance().put("deviceSn", deviceSn);
    }
}
