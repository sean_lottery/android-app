package com.lottery.app.core;

import com.lottery.app.entity.TbWxUserRedeem;
import com.lottery.app.utils.SPHelper;

import java.math.BigDecimal;
import java.util.List;

public class DataCore {


    /**
     * 单列模式
     */
    private static volatile DataCore instance = null;

    /**
     * 获取单列模式
     * @return
     */
    public static DataCore sharedCore() {
        if (instance == null) {
            synchronized (DataCore.class) {
                if (instance == null) {
                    instance = new DataCore();
                }
            }
        }
        return instance;
    }


    /**
     * 总金额
     */
    private BigDecimal totalAmount;


    /**
     *  扫描二维码Code
     */
    private String scanCode;

    private List<TbWxUserRedeem> deviceWxUserRedeems;

    private DataCore() {

    }

    public List<TbWxUserRedeem> getDeviceWxUserRedeems() {
        return deviceWxUserRedeems;
    }

    public void setDeviceWxUserRedeems(List<TbWxUserRedeem> deviceWxUserRedeems) {
        this.deviceWxUserRedeems = deviceWxUserRedeems;
    }

    /**
     * 获取总金额
     */
    public BigDecimal getTotalAmount() {
        if (totalAmount == null) {
            String v = SPHelper.getInstance().getString("totalAmount");
            if (v == null || v.isEmpty()) {
                return null;
            }
            totalAmount = new BigDecimal(v);
        }
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
        if (totalAmount == null) {
            SPHelper.getInstance().remove("totalAmount");
            return;
        }
        SPHelper.getInstance().put("totalAmount", totalAmount.toString());
    }


    public String getScanCode() {
        return scanCode;
    }

    public void setScanCode(String scanCode) {
        this.scanCode = scanCode;
    }
}
