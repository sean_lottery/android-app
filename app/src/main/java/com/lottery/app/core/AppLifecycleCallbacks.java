package com.lottery.app.core;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.lottery.app.mqtt.MQTTClient;
import com.lottery.app.ui.activity.LaunchActivity;
import com.lottery.app.ui.activity.LoginActivity;
import com.lottery.app.utils.AppUtils;

/**
 * App生命周期
 */
public class AppLifecycleCallbacks implements Application.ActivityLifecycleCallbacks {

    private static final String TAG = "AppLifecycleCallbacks";

    private boolean isAppInForeground = false;
    @Override
    public void onActivityCreated(@NonNull Activity activity, @Nullable Bundle savedInstanceState) {

    }

    @Override
    public void onActivityStarted(@NonNull Activity activity) {

    }

    @Override
    public void onActivityResumed(@NonNull Activity activity) {
        Log.e(TAG, "onActivityResumed: " + activity.getClass().getSimpleName() );
        if (isAppInForeground == false) {
            isAppInForeground = true;
            // 应用切换到前台
            Log.d(TAG, "onActivityResumed: 应用切换到前台");
        }
        if (!((activity instanceof LoginActivity) || (activity instanceof LaunchActivity))) {
            if (MQTTClient.getInstance().isBuild() && !MQTTClient.getInstance().isOnline()){
                Log.w(TAG, "onActivityResumed: MQTTClient is offline");
                MQTTClient.getInstance().connect();
            }else{
                Log.w(TAG, "onActivityResumed: MQTTClient is online");
            }
        }
    }

    @Override
    public void onActivityPaused(@NonNull Activity activity) {
        Log.e(TAG, "onActivityPaused: " + activity.getClass().getSimpleName() );
        if (AppUtils.isAppInForeground(activity) == false) {
            // 应用切换到后台
            isAppInForeground = false;
            Log.d(TAG, "onActivityPaused: 应用切换到后台");
        }
    }

    @Override
    public void onActivityStopped(@NonNull Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(@NonNull Activity activity, @NonNull Bundle outState) {

    }

    @Override
    public void onActivityDestroyed(@NonNull Activity activity) {

    }
}

