package com.lottery.app.api;

import com.lottery.app.entity.AddOrderBody;
import com.lottery.app.entity.AddOrderRes;
import com.lottery.app.entity.CaptchaImage;
import com.lottery.app.entity.EndPayParms;
import com.lottery.app.entity.ExchangeOrderBody;
import com.lottery.app.entity.LoginBody;
import com.lottery.app.entity.LoginResponse;
import com.lottery.app.entity.PageResponse;
import com.lottery.app.entity.RedeemBody;
import com.lottery.app.entity.ResponseBody;
import com.lottery.app.entity.TbEquipmentPreset;
import com.lottery.app.entity.TbEquipmentPresetCargoLane;
import com.lottery.app.entity.TbEventLog;
import com.lottery.app.entity.TbOrder;
import com.lottery.app.entity.TbWxUserRedeem;
import com.lottery.app.entity.UserInfoResponse;
import com.lottery.app.mqtt.PaySuccessVo;

import java.util.List;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.schedulers.Schedulers;
import retrofit2.http.GET;
import retrofit2.http.Query;


public class HttpAction {


    private static CommonService commonService = RetrofitServiceManager.getInstance().create(CommonService.class);

    /**
     * 获取验证码
     */
    public static void getCaptchaImage(Observer<CaptchaImage> observer) {
        setSubscribe(commonService.getCaptchaImage(), observer);
    }

    /**
     * 登陆
     * @param loginBody 登陆信息
     * @param observer 回调
     */
    public static void login(LoginBody loginBody, Observer<LoginResponse> observer) {
        setSubscribe(commonService.login(loginBody), observer);
    }

    /**
     * 获取用户信息
     * @param observer 回调
     */
    public static void getInfo(Observer<UserInfoResponse> observer) {
        setSubscribe(commonService.getInfo(), observer);
    }


    /**
     * 获取设备列表
     * @param pageNum 页码
     * @param pageSize 每页数量
     * @param observer 回调
     */
    public static void getEquipmentPresetList(int pageNum, int pageSize, Observer<PageResponse<TbEquipmentPreset>> observer) {
        setSubscribe(commonService.getEquipmentPresetList(pageNum, pageSize), observer);
    }

    /**
     * 获取设备信息
     * @param ePId 设备ID
     * @param observer 回调
     */
    public static void getEquipmentPresetInfo(Long ePId, Observer<ResponseBody<TbEquipmentPreset>> observer) {
        setSubscribe(commonService.getEquipmentPresetInfo(ePId), observer);
    }

    /**
     * 获取设备货道信息
     * @param equipmentId 设备ID
     * @param isYn 是否启用
     * @param observer 回调
     */
    public static void  getEquipmentPresetCargoLaneList( Long equipmentId, int isYn,Observer<ResponseBody<List<TbEquipmentPresetCargoLane>>> observer){
        setSubscribe(commonService.getEquipmentPresetCargoLaneList(equipmentId,isYn),observer);
    }


    /**
     * 添加订单
     * @param addOrderBody  订单信息
     * @param observer
     */
    public static void addOrder(AddOrderBody addOrderBody, Observer<ResponseBody<AddOrderRes>> observer) {
        setSubscribe(commonService.addOrder(addOrderBody), observer);
    }

    /**
     * 获取订单[支付状态]
     * @param orderNo
     * @param observer
     */
    public static void getOrder(String orderNo, Observer<ResponseBody<PaySuccessVo>> observer) {
        setSubscribe(commonService.getOrder(orderNo), observer);
    }

    /**
     * 获取订单详情
     * @param orderNo
     * @param observer
     */
    public static void getOrderInfo(String orderNo, Observer<ResponseBody<TbOrder>> observer) {
        setSubscribe(commonService.getOrderInfo(orderNo), observer);
    }

    /**
     * 订单列表
     * @param pageNum 页码
     * @param pageSize 每页数量
     * @param deptId 部门ID
     * @param beginTime 开始时间
     * @param endTime 结束时间
     * @param observer 回调
     */
    public static void getOrderList(int pageNum, int pageSize, Long deptId,String beginTime,String endTime,Integer payStatus, Observer<PageResponse<TbOrder>> observer) {
        setSubscribe(commonService.getOrderList(pageNum, pageSize,deptId,beginTime,endTime,payStatus), observer);
    }

    /**
     * 微信兑奖记录
     * @param pageNum 页码
     * @param pageSize 每页数量
     * @param type 类型 1：未兑奖 2：提现 3：已中奖
     * @param observer 回调
     */
    public static void getRedeemList(int pageNum, int pageSize,Integer type, Observer<PageResponse<TbWxUserRedeem>> observer) {
        setSubscribe(commonService.getRedeemList(pageNum, pageSize,type), observer);
    }

    /**
     * 获取兑奖/提现详情
     * @param id 记录ID
     * @return
     */
    public static void getRedeemInfo(Long id, Observer<ResponseBody<TbWxUserRedeem>> observer) {
        setSubscribe(commonService.getRedeemInfo(id), observer);
    }


    /**
     * 兑奖
     * @param redeemBody
     * @param observer
     */
    public static void doRedeem2(RedeemBody redeemBody, Observer<ResponseBody> observer) {
        setSubscribe(commonService.doRedeem2(redeemBody), observer);
    }

    /**
     * 换购
     */
    public static void addOrderBalanceticai(ExchangeOrderBody exchangeOrderBody, Observer<ResponseBody> observer) {
        setSubscribe(commonService.addOrderBalanceticai(exchangeOrderBody), observer);
    }

    /**
     * 提现
     * @param endPayParms
     * @param observer
     */
    public static void entPay3(EndPayParms endPayParms,Observer<ResponseBody> observer){
        setSubscribe(commonService.entPay3(endPayParms),observer);
    }

    public static void addEventLog(TbEventLog eventLog, Observer<ResponseBody> observer){
        setSubscribe(commonService.addEventLog(eventLog),observer);
    }

    private static <T> void setSubscribe(Observable<T> observable, Observer<T> observer) {
        observable.subscribeOn(Schedulers.io())
                .subscribeOn(Schedulers.newThread())//子线程访问网络
                .observeOn(AndroidSchedulers.mainThread())//回调到主线程
                .subscribe(observer);
    }


}
