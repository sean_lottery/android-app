package com.lottery.app.api;

import com.lottery.app.core.UserCore;
import com.lottery.app.utils.EventBusUtils;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * 在请求头里添加token的拦截器处理
 */
public class TokenHeaderInterceptor implements Interceptor {

    @Override
    public Response intercept(Chain chain) throws IOException {

        Response response = null;

        String token = UserCore.sharedCore().getToken();
        if (token.isEmpty()) {
            Request originalRequest = chain.request();
            response = chain.proceed(originalRequest);
        }else {
            Request originalRequest = chain.request();
            //key的话以后台给的为准，我这边是叫token
            Request updateRequest = originalRequest.newBuilder().header("Authorization", "Bearer "+token).build();
            response = chain.proceed(updateRequest);
        }

        // 对返回code统一拦截
        if (response != null) {
            if (response.code() == 401) {
                //退出登录
                UserCore.sharedCore().logout();
                //关闭所以页面跳转到登录页面
                EventBusUtils.postLogout();

            }
        }
        return response;
    }
}
