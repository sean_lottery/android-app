package com.lottery.app.api;


/**
 * 接口
 */
public interface Api {

    /**
     * 服务器地址
     */
    public String SERVER_URL  = "https://www.junhuachuancai.cn/prod-api/";
    public String IMAGE_URL  = "https://www.junhuachuancai.cn/prod-api";
    public String MQTT_HOST  = "47.107.70.239";
    public int MQTT_PORT  = 1885;

//    public String SERVER_URL  = "http://192.168.0.218:8080/";
//    public String IMAGE_URL  = "http://192.168.0.218:8080/";
//    public String MQTT_HOST  = "192.168.0.218";
//    public int MQTT_PORT  = 18850;

    /** 微信AppId */
    public String WX_APP_ID = "wx237f01678689b8e4";
    /** 微信回调页面地址 */
    public String WX_REDIRECT_URL = "https://www.junhuachuancai.cn/h5/appindex.html";



}
