package com.lottery.app.api;

import com.lottery.app.entity.AddOrderBody;
import com.lottery.app.entity.AddOrderRes;
import com.lottery.app.entity.CaptchaImage;
import com.lottery.app.entity.EndPayParms;
import com.lottery.app.entity.ExchangeOrderBody;
import com.lottery.app.entity.LoginBody;
import com.lottery.app.entity.LoginResponse;
import com.lottery.app.entity.PageResponse;
import com.lottery.app.entity.RedeemBody;
import com.lottery.app.entity.ResponseBody;
import com.lottery.app.entity.TbEquipmentPreset;
import com.lottery.app.entity.TbEquipmentPresetCargoLane;
import com.lottery.app.entity.TbEventLog;
import com.lottery.app.entity.TbOrder;
import com.lottery.app.entity.TbWxUserRedeem;
import com.lottery.app.entity.UserInfoResponse;
import com.lottery.app.mqtt.PaySuccessVo;

import java.util.List;

import io.reactivex.rxjava3.core.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface CommonService {

    /**
     * 获取验证码
     * url:captchaImage
     */
    @GET("captchaImage")
    Observable<CaptchaImage> getCaptchaImage();

    /**
     * 登陆
     */
    @POST("login")
    Observable<LoginResponse> login(@Body LoginBody loginBody);

    /**
     * 获取用户信息
     * @return
     */
    @GET("getInfo")
    Observable<UserInfoResponse> getInfo();

    /**
     * 获取设备列表
     * @param pageNum 页码
     * @param pageSize 每页数量
     * @return
     */
    @GET("project/equipmentPreset/list")
    Observable<PageResponse<TbEquipmentPreset>> getEquipmentPresetList(@Query("pageNum") int pageNum, @Query("pageSize") int pageSize);

    /**
     * 获取设备信息
     * @param ePId 设备ID
     * @return 设备信息
     */
    @GET("phone/equipmentPreset/info")
    Observable<ResponseBody<TbEquipmentPreset>> getEquipmentPresetInfo(@Query("ePId") Long ePId);

    /**
     * 获取设备货道信息
     * @param equipmentId
     * @param isYn
     * @return
     */
    @GET("phone/equipmentPresetCargoLane/list")
    Observable<ResponseBody<List<TbEquipmentPresetCargoLane>>> getEquipmentPresetCargoLaneList(@Query("equipmentId") Long equipmentId,@Query("isYn") int isYn);


    /**
     * 添加订单
     * @param addOrderBody
     */
    @POST("phone/addOrder")
    Observable<ResponseBody<AddOrderRes>> addOrder(@Body AddOrderBody addOrderBody);

    @GET("phone/order")
    Observable<ResponseBody<PaySuccessVo>> getOrder(@Query("orderNo") String orderNo);
    /**
     * 获取订单详情
     * @param orderNo 订单号
     * @return 订单详情
     */
    @GET("phone/order/info")
    Observable<ResponseBody<TbOrder>> getOrderInfo(@Query("orderNo") String orderNo);


    /**
     * 订单列表
     */
    @GET("project/order/list")
    Observable<PageResponse<TbOrder>> getOrderList(@Query("pageNum") int pageNum, @Query("pageSize") int pageSize, @Query("deptId") Long deptId,@Query("params[beginTime]") String beginTime , @Query("params[endTime]") String endTime,@Query("payStatus") Integer payStatus);

    /**
     * 微信 兑奖/提现 记录
     */
    @GET("project/redeem/list")
    Observable<PageResponse<TbWxUserRedeem>> getRedeemList(@Query("pageNum") int pageNum, @Query("pageSize") int pageSize, @Query("type") Integer type);

    /**
     * 获取兑奖/提现详情
     * @param id 记录ID
     * @return
     */
    @GET("phone/redeem/{id}")
    Observable<ResponseBody<TbWxUserRedeem>> getRedeemInfo(@Path("id") Long id);


    @POST("wx/getRedeem2")
    Observable<ResponseBody> doRedeem2(@Body RedeemBody redeemBody);


    /**
     * 换购
     * url: wx/addOrderBalanceticai
     */
    @POST("phone/addOrderBalanceticai")
    Observable<ResponseBody> addOrderBalanceticai(@Body ExchangeOrderBody exchangeOrderBody);

    /**
     * 提现
     * @param redeemBody
     * @return
     */
    @POST("wx/entPay3")
    Observable<ResponseBody> entPay3(@Body EndPayParms redeemBody);

    @POST("phone/eventLog/add")
    Observable<ResponseBody> addEventLog(@Body TbEventLog tbEventLog);

}
